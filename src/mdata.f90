SUBROUTINE mdata(dim)

!  Read data for a molecule or atom.

USE parameters
USE connect, ONLY : indexb
USE consts
USE global
USE indexes
USE labels
USE switches
USE types
USE variables
USE input
USE rotations, ONLY : wigner
USE sites
USE sizes
USE molecules, mlcl => mol
USE monolayer
IMPLICIT NONE

INTEGER, INTENT(IN) :: dim  ! 0 for an atom, 3 otherwise

DOUBLE PRECISION :: r(3,3), s(3,3), d(sq,sq), x

LOGICAL :: eof, first, copied, moving, turning, rotate, position_specified
INTEGER :: flag, frag=0, i,j, k, m, m0, mol, n, nc, p, last

LOGICAL, SAVE :: firstmol=.true.

CHARACTER(LEN=20) :: word, ww
CHARACTER(LEN=80) :: commnt
CHARACTER(LEN=20) :: w

INTERFACE
  SUBROUTINE axis(m,k0,r,e,verbose)
  INTEGER, INTENT(IN) :: k0, m
  DOUBLE PRECISION :: r(3,3), e(3,3)
  LOGICAL, INTENT(IN), OPTIONAL :: verbose
  END SUBROUTINE axis
END INTERFACE

!  Read data for a molecule or atom. In the following, square brackets denote
!  optional items, and curly brackets denote alternatives. Generally,
!  the items associated with a main keyword should appear on the same
!  line, but +++ at the end of a line in the data indicates that the
!  next line is to be concatenated with the present one. Keywords are
!  here shown in upper case, but may appear in the data in either case.
!  Items shown here in lower case are values, and should be given in the
!  data either as numerical values or as the names of variables or
!  constants, optionally preceded by a minus sign.

!  All molecules are read into layer(0), a linked list of molecules.
!  If a real layer is later defined, molecules are transferred from
!  layer(0) to layer(layerindex). nextm() and prevm() are the next
!  and previous molecules in the list respectively. The number of
!  molecules in each layer is contained in nmolls(layerindex).
!  Following a definition of a layer, any molecules remaining in layer(0)
!  are treated as an isolated cluster on the surface.

!  The position and some properties of the molecule are specified by the
!  MOLECULE command:

!  MOLECULE name position [LINEAR|ATOM] [options]
!  If LINEAR is specified, the molecule is linear: the sites are all on
!  the local z axis, and their positions in the local coordinate frame
!  are specified by giving the z coordinate only. Only the m=0 multipole
!  components should be specified. However the position of the molecular
!  origin is specified in the global coordinate frame in 3-dimensional
!  cartesian or polar coordinates, whether the molecule is linear or
!  not.

!  An interaction site may be associated with the molecular origin, in
!  which case the options and data for it follow as for other sites.
!  If ATOM is specified, this is the only site, and any attempt to define
!  further sites will be faulted.

!  Data are required for each site of the molecule in turn.
!  The positions of the sites are normally given in the local molecular
!  coordinate system, but they can be given relative to another
!  'parent' site in the same molecule.

!  name {POL r theta phi} [options]
!       {x y z          }
!  'name' is the name of the site (enclosed in quotes if it contains
!  spaces).
!  (x,y,z) or (r,theta,phi) is the position of the site. Distances are
!  in the current length unit, initially bohr; angles are in degrees.

!  The options available are as follows:

!  A rotation can be specified for the entire molecule or for an
!  individual site:
!    ROTATED alpha beta gamma
!    ROTATED BY psi ABOUT x y z
!  If neither of these items is given, the axes of the new site are
!  parallel to those of the parent site.
!  If a rotation is specified for a molecule, the entire molecule is
!  to be rotated, and the rotation is relative to the global axis
!  system. If for a site, the site is to be rotated relative to its
!  parent site.

!  The following option may be specified for the molecule or for an
!  individual site:
!    COPY name
!  If specified for a molecule, the entire molecule description is to
!  be copied from molecule 'name', and no further information is
!  required. No END is needed.
!  If specified for an individual site, the moments and site type are
!  to be copied from the specified site.

!  The following options are normally specified for individual sites,
!  but can also be specified for the molecule, and then apply to
!  a site at the molecular origin:

!    LIMIT [=] L
!    RANK [=] L
!  L is the maximum rank of multipole to be specified. The default is
!  set by the DEFAULT RANK command, or is -1 (no multipoles) in the
!  absence of a DEFAULT RANK command.

!  The remaining options apply to individual sites only:

!    RELATIVE TO parent-name
!  'parent-name' specifies the name of a site to which this one is
!  attached. The default orientation of this site is the same as that of
!  the parent, and the position is specified in the local axis system of
!  the parent site. If no RELATIVE TO option is present, or if
!  'parent-name' is the molecule name, the parent site is the molecular
!  origin.

!    LINK TO parent-name
!  This has a similar effect to RELATIVE TO, but the coordinates
!  given are referred to the molecular origin, not to the axis system
!  of the parent site. The program converts the coordinates given into
!  coordinates relative to the specified parent site.

!    TWIST [=] omega
!  This specifies a rotation through an angle omega about the bond
!  between the new site and its parent. A positive rotation is a
!  clockwise rotation of the new site when viewed from the parent.
!  Strictly speaking, this rotation is redundant, but the intention
!  is that ROTATED should be used to define a reference orientation
!  for the site (and will usually involve constant angles) and that
!  TWIST can define a variable dihedral angle. The default is zero.

!    BOND[S] [TO] name [AND name]
!  The sites specified, which should be atoms, are directly bonded to
!  this one. They should have been defined previously. If more than one
!  site of the same name has been defined, the one most recently defined
!  is used. The special name 'LAST' (case irrelevant) can be used to
!  refer to the site defined immediately before this one.

!  The multipole moments follow, starting on a new line and given in
!  the order Q00, Q10, Q11c, Q11s, Q20, ..., QLLc, QLLs. Alternatively,
!  the non-zero values may be given in the format Qlmc [=] value ...,
!  with the list terminated by a blank line.

!  At any point in the data for the molecule the following may occur:

!    AXES { x | y | z } FROM site TO site +++
!           { x | y | z } FROM site TO site +++
!           [ ROTATE | DEFINE ] FOR site [ site  ...]
!
!  Redefine the axes for the sites listed after FOR. The
!  first axis given is in the direction indicated; the second axis is
!  taken initially in the direction specified but is then orthogonalised
!  to the first; and the third axis forms a right-handed set and is not
!  specified explicitly. All the sites mentioned must have been defined
!  previously. In cases of ambiguity, the most recently defined site with
!  the name given is used. It is possible to use the syntax
!  'FROM s BETWEEN s1 AND s2', in which case the axis bisects the s1-s-s2
!  angle.

!  The multipole moments for the sites in question are treated as follows.
!  If DEFINE is specified, the numerical values of the moments are unchanged;
!  it is assumed that the values originally given relate to the new axes.
!  If ROTATE is specified, the moments were given in the axes originally 
!  defined for the site, and are now to be transformed to the new axis
!  system.

!  'END' on a line by itself terminates the list for this molecule.


!  Is there a deleted molecule to re-use?
mol=0
do i=1,mols
  if (head(i) .eq. 0) mol=i
end do
if (mol .eq. 0) then
!  New molecule
  mols=mols+1
  if (mols .gt. nmols) call die("Too many molecules",.true.)
  mol=mols
endif
first=.true.
last=0
! mdim(mol)=dim
mlcl(mol)%mdim=dim
linear(mol)=.false.
fixed(mol)=.true.
calc(mol)=.false.
cm(mol)=0
!  Read the molecule name from the current line
call reada(word)

do
  if (.not. first) then
    do
      !  Looking for another site, but other things may intervene
      call read_line(eof)
      if (eof) call die('Unexpected end of input file.',.false.)
      if (nitems == 0) cycle
      call reada(word)
      select case(upcase(word))
      case("NOTE","!","")
        cycle
      case("END")
        !  Definition complete.
        call axes(mol,.false.)
        return
      case("COMMENT")
        call reada(commnt)
        print "(/a)", commnt
        cycle
      case("FRAGMENT")
        call readi(frag)
        cycle
      case("AXES")
        !  Redefine axes for specified sites, and rotate multipoles into the
        !  new axis system if ROTATE is specified
        call axes(mol,.false.)
        !  Unset 'axes calculated' flag (there may be sites not yet read)
        calc(mol)=.false.
        s=se(:,:,0)  !  Unit matrix
        call axis(mol,m,r,s)
        !  S now contains the site orientation relative to global axes
        call readu(ww)
        select case(ww)
        case("ROTATE","FOR")
          rotate=.true.
        case("DEFINE")
          rotate=.false.
        case default
          rotate=.true.
          !  Assume it's a site name
          call reread(-1)
        end select
        call readu(ww)
        if (ww .ne. 'FOR') call reread(-1)
        do while (item < nitems)
          call read_site(k,mol)
          p=link(k)    !  Parent site
          print '(/a,a)', 'Site ', trim(name(k))
          se(:,:,k)=s
          !  Construct axis-angle specification of the orientation of
          !  this site relative to its parent, and store in the molecule
          !  description. Also return the relative orientation matrix in R.
          call relative(p,k,r)
          if (rotate) then
            !  Rotate multipoles into new axis system
            call wigner(r,d,4)
            print '(a/f12.6,3(/3f12.6),5(/5f12.6))',                   &
                'Wigner matrix:', d(1,1), ((d(i,j), j=2,4), i=2,4),    &
                ((d(i,j), j=5,9), i=5,9)
            n=(l(k)+1)**2
            q(1:n,k)=matmul(q(1:n,k),d(1:n,1:n))
            print '(a,a)', 'Transformed moments for site ', name(k)
            if (n .ge. 1) print '(a,f12.7)',                           &
                'Charge        ', q(1,k)
            if (n .ge. 4) print '(a,3f12.7)',                          &
                'Dipole        ', (q(i,k), i=2,4)
            if (n .ge. 9) print '(a,5f12.7)',                          &
                'Quadrupole    ', (q(i,k), i=5,9)
            if (n .ge. 16) print '(a,5f12.7/(26x,4f12.7))',            &
                'Octopole      ', (q(i,k), i=10,16)
            if (n .ge. 25) print '(a,5f12.7/(26x,4f12.7))',            &
                'Hexadecapole  ', (q(i,k), i=17,25)
            if (n .ge. 36) print '(a,5f12.7/(26x,4f12.7))',            &
                'Rank 5        ', (q(i,k), i=26,36)
          else
            print "(3a)", "Moments specified for site ", trim(name(k)), &
                " are now taken to refer to this axis system"
          endif
        end do
      case default
        !  Unrecognised -- must be a site name. This is an error if
        !  we have an atom. (If we have got here we have already read
        !  the site details for the atom.)
        if (mlcl(mol)%mdim .eq. 0)                                      &
            call die("An atom may have only one site",.true.)
        exit
      end select
    end do
  end if ! not first

  !  New site. Re-use a site from the free list if there is one.
  if (free .gt. 0) then
    m=free
    free=next(free)
  else
    tops=tops+1
    m=tops
  endif
  if (m .gt. nsites) call die('Too many sites.',.true.)

  name(m)=word
  !  First site for this molecule?
  if (first) then
    head(mol)=m
    mlcl(mol)%name=word
    mlcl(mol)%head=m
    link(m)=0
    l(m)=-1
    !  Layer data
    if (firstmol) then
      nmolls(0)=1
      moll(1)=mol
      firstmol=.false.
      layer(0)=mol
      nmolls(0)=mol
      prevm(mol)=0
      nextm(mol)=0
    else
      nmolls(0)=nmolls(0)+1
      moll(nmolls(0))=mol
      prevm(mol)=lastmoll
      nextm(mol)=0
      nextm(lastmoll)=mol
    endif
    lastmoll=mol
    !  Atom?
    if (mlcl(mol)%mdim==0) then
      linear(mol)=.true.
    end if
    sm(m)=m
    se(:,:,m)=0d0
    do i=1,3
      se(i,i,m)=1d0
    end do
  else
    next(last)=m
    link(m)=head(mol)
    l(m)=ldef
  endif

  !  Set defaults
  next(m)=0        ! Last site so far
  type(m)=0        ! Type undefined
  ps(m)=0          ! Unpolarizable
  q(1:sq,m)=0d0
  sf(m)=0
  sp(:,m)=0
  sr(:,m)=0
  st(m)=0
  if (first) then
    level(m)=1
    moving=.true.
    turning=.true.
  else
    level(m)=2
    moving=.false.
    turning=.false.
  endif
  copied=.false.
  fragment(m) = frag
  nc=nconst
  position_specified=.false.


  !  Read options
  do while (item<nitems)
    call reada(word)
    ! print "(a)", word
    select case(upcase(word))
    case("AT","POSITION")
      !  This (or "POLAR") is optional only for the position of the
      !  molecule. Other sites must have their positions specified
      !  relative to the molecular origin.

      !  Provide for "AT POLAR" etc.
      call readu(ww)
      call reread(-1)
      if (ww .eq. "POL" .or. ww .eq. "POLAR") cycle
      sf(m)=sf(m)+32
      if (first .or. .not. linear(mol)) then
        call readv(sp(1,m),1,moving)
        call readv(sp(2,m),1,moving)
      endif
      call readv(sp(3,m),1,moving)
      position_specified=.true.
    case("POL","POLAR")
      if (linear(mol) .and. .not. first) call die                       &
        ("Polar coordinates not allowed for sites in linear molecules", &
        .true.)
      sf(m)=sf(m)+16
      call readv(sp(1,m),1,moving)
      call readv(sp(2,m),2,moving)
      call readv(sp(3,m),2,moving)
      position_specified=.true.

    case("ATOM")
      mlcl(mol)%mdim=0
      linear(mol)=.true.
      sm(m)=m
      se(:,:,m)=0d0
      do i=1,3
        se(i,i,m)=1d0
      end do
    case("LINEAR")
      if (.not. first)                                                 &
          call die("LINEAR may not be specified for a site",.true.)
      linear(mol)=.true.
      mlcl(mol)%mdim=1
    case("COPY")
      !  COPY name [FRAGMENT n]
      !  
      call reada(ww)
      if (first) then
        !  Copy the entire molecule description
        m0=locate(ww)
        if (item < nitems) then
          call readu(ww)
          if (ww == "FRAGMENT") then
            call readi(i)
          else
            i = 0
          end if
        end if
        if (moving .or. turning) fixed(mol)=.false.
        if (moving) sf(m)=sf(m)+64
        if (turning) sf(m)=sf(m)+8
        call copy(m0, mol, i)
        call show_site(.false.)
        print "(2a)", "Copied from ", trim(name(head(m0)))
        return
      else
        !  Copy site description
        k=head(mol)
        do while (k>0)
          if (k == m) then
            !  All previous sites tested
            call die("Site "//trim(ww)//" not found",.true.)
          else if (ww .eq. name(k)) then
            l(m)=l(k)
            q(:,m)=q(:,k)
            type(m)=type(k)
            copied=.true.
            exit
          else
            k=next(k)
          end if
        end do
      end if
    case("LIMIT","RANK")
      call reada(w)
      if (w .ne. "=") call reread(-1)
      call readi(l(m))
      if (mlcl(mol)%mdim .eq. 0 .and. l(m) .gt. 0) call die                 &
          ("Multipoles other than charge may not be assigned to an atom", &
          .true.)
      if (qsize(l(m)) .gt. sq) call die                                &
          ("Rank declared for this site is too high",.true.)
    case("TYPE")
      call reada(ww)
      if (ww .eq. "=") call reada(ww)
      type(m)=gettype(ww)
    case("RELATIVE","LINK","LINKED")
      if (first) call die                                              &
          ("Syntax error: RELATIVE forbidden for base site",.true.)
      call readu(w)
      if (w .ne. "TO") call reread(-1)
      call readu(ww)
      if (ww .eq. "LAST") then
        link(m)=last
      else
        call reread(-1)
        call reada(ww)
        link(m)=-1
        i=head(mol)
        do while (i>0)
          if (ww .eq. name(i)) link(m)=i
          i=next(i)
        end do
        if (link(m) .eq. -1)                                           &
            call die("Parent site not recognized",.true.)
        if (link(m) .eq. m)                                            &
            call die("Site may not be defined relative to itself",.true.)
      end if
      level(m)=level(link(m))+1
      if (word .eq. "LINK" .or. word .eq. "LINKED") then
        !  Convert global coordinates to local
        do j=1,3
          k=link(m)
          x=vvalue(sp(j,m))
          do while (k .ne. head(mol))
            x=x-vvalue(sp(j,k))
            k=link(k)
          end do
          if (sp(j,m) .ge. nc .or. sp(j,m) .eq. 0 .and. x .ne. 0d0) then
            nconst=nconst-1
            if (nconst .eq. nvar) call die("Too many constants",.true.)
            sp(j,m)=nconst
            value(nconst)=x
            vtype(nconst)=1
          else if (x .eq. 0d0) then
            sp(j,m)=0
          else
            value(sp(j,m))=x
          endif
        end do
      end if

    case("ROTATED")
      if (mlcl(mol)%mdim .eq. 0)                                            &
          call die("An atom may not be rotated",.true.)
      if (mlcl(mol)%mdim .eq. 1 .and. m .ne. head(mol)) then
        sf(m)=1
        sr(1,m)=0d0
        sr(2,m)=pi
        sr(3,m)=0d0
      else
        flag=0
        do while (flag<3)
          call readu(ww)
          select case(ww)
          case("THROUGH","BY")
            call readv(sr(1,m),2,turning)
            flag=flag+1
          case("ABOUT")
            call readv(sr(2,m),1,turning)
            call readv(sr(3,m),1,turning)
            call readv(sr(4,m),1,turning)
            flag=flag+2
          case default
            if (flag==0) then
              call reread(-1)
              call readv(sr(1,m),2,turning)
              call readv(sr(2,m),2,turning)
              call readv(sr(3,m),2,turning)
              flag=4
            else
              call die                                                 &
                  ("Syntax error in rotation specification",.true.)
            end if
          end select
        end do
        !  Set site flag sf(m) to 1 for Euler angle rotation, 2 for
        !  angle/axis rotation (and 0 for no rotation).
        flag=5-flag
        sf(m)=sf(m)+flag
      end if
    case("TWIST")
      call reada(w)
      if (w .ne. "=") call reread(-1)
      call readv(st(m),2,turning)
      sf(m)=sf(m)+4
    case("BOND","BONDS")
      do while (item<nitems)
        call readu(w)
        select case(w)
        case("TO","AND")
          cycle
        case("LAST")
          call insert(indexb,last,m,1,"bond index")
        case default
          call reread(-1)
          call reada(ww)
          j=0
          k=head(mol)
          do while (k>0)
            if (name(k) .eq. ww) j=k
            k=next(k)
          end do
          if (j .eq. 0) call die                                       &
              ("Name "//trim(ww)//" not recognised after BOND",.true.)
          call insert(indexb,j,m,1,"bond index")
        end select
      end do
    case default
      !  Assume we have a position specified without "AT"
      sf(m)=sf(m)+32
      call reread(-1)
      if (first .or. .not. linear(mol)) then
        call readv(sp(1,m),1,moving)
        call readv(sp(2,m),1,moving)
      endif
      call readv(sp(3,m),1,moving)
      position_specified=.true.
    end select
  end do

  if (.not. first .and. .not. position_specified) then
    print "(3a)", "Site ", trim(name(m)), ": position not given"
    call die("Stopping",.true.)
  end if

  if (.not. copied) then
    !  Read multipoles
    q(:,m) = 0.0d0
    if (l(m) .ge. 0) then
      n = (l(m)+1)**2
      call read_line(eof)
      do while (nitems == 0)
        call read_line(eof)
      end do
      !  Get first character of first item
      call readu(w)
      call reread(0)
      if (w(1:1) == "Q") then
        !  Multipole names supplied
        do
          do while (item < nitems)
            call readl(w)
            k = 0
            do i = 1,n
              if (w(2:) == label(i)) k = i
            end do
            if (k == 0)                                                &
                call die("Unrecognised multipole name "//w,.true.)
            call reada(w)
            if (w .ne. "=") call reread(-1)
            call readf(q(k,m))
          end do
          call read_line(eof)
          !  Blank line terminates multipole list
          if (nitems == 0) exit
        end do
      else if (linear(mol)) then
        do i = 0,l(m)
          k = i**2+1
          call getf(q(k,m))
        end do
      else
        do i = 1,n
          call getf(q(i,m))
        end do
      end if
    end if
  end if
  if (moving .or. turning) fixed(mol) = .false.
  moving = moving .or. (iand(sf(link(m)),72) .gt. 0)
  turning = turning .or. (iand(sf(link(m)),8) .gt. 0)
  if (moving) sf(m) = sf(m)+64
  if (turning) sf(m) = sf(m)+8

  if (switch(5)) then
    call show_site
  endif
  last = m
  first = .false.
end do

CONTAINS

SUBROUTINE show_site(show_multipoles)

USE output

LOGICAL, INTENT(IN), OPTIONAL :: show_multipoles
INTEGER i

if (iand(sf(m),16) == 16) then
  ww="(polar)"
else
  ww="(cartesian)"
end if
if (first) then
  print "(/a,i0,a,i0,a,a)", "Molecule ", mol, " (Site ", m, "):  ",    &
      trim(name(m))
  call putstr("Origin position "//ww,0)
else
  print "(/a,i0,4a)", "Site ", m, ":  ", trim(name(m)),                &
      "    Type ", tname(type(m))
  call putstr("Position "//ww,0)
endif
do i=1,3
  call vout(sp(i,m))
end do

if (.not. first .and. link(m) .ne. head(mol))                          &
    call putstr("  relative to "//name(link(m)),0)
call putout(0)
if (flag .eq. 1) then
  call putstr("Euler angles",0)
  do i=1,3
    call vout(sr(i,m))
  end do
  call putsp(5)
  call putout(0)
else if (flag .eq. 2) then
  call putstr("Rotated by",0)
  call vout(sr(1,m))
  call putstr(" about",0)
  do i=2,4
    call vout(sr(i,m))
  end do
  call putsp(5)
  call putout(0)
endif
if (iand(sf(m),4) .eq. 4) then
  call putstr("Rotated by",0)
  call vout(st(m))
  call putstr(" about bond to "//name(link(m)),0)
  call putout(0)
endif

if (present(show_multipoles)) then
  if (.not. show_multipoles) return
end if
if (n .ge. 1) print "(a,f10.5)",                                       &
    "Charge        ", q(1,m)
if (n .ge. 4) print "(a,3f10.5)",                                      &
    "Dipole        ", (q(i,m), i=2,4)
if (n .ge. 9) print "(a,5f10.5)",                                      &
    "Quadrupole    ", (q(i,m), i=5,9)
if (n .ge. 16) print "(a,5f10.5/(24x,4f10.5))",                        &
    "Octopole      ", (q(i,m), i=10,16)
if (n .ge. 25) print "(a,5f10.5/(24x,4f10.5))",                        &
    "Hexadecapole  ", (q(i,m), i=17,25)
if (n .ge. 36) print "(a,5f10.5/(24x,4f10.5))",                        &
    "Rank 5        ", (q(i,m), i=26,36)

END SUBROUTINE show_site

SUBROUTINE copy(m1,m,only)

!  Copy definition of molecule M1 to molecule M.

!  Bonding information in INDEXB is not currently copied. No
!  problem in principle.

USE input
USE switches
USE sites
USE molecules
IMPLICIT NONE
INTEGER, INTENT(IN) :: m, m1
INTEGER, INTENT(IN), OPTIONAL :: only
INTEGER :: flag, k, k1, last, frag=0

linear(m) = linear(m1)
calc(m) = .false.
k1 = head(m1)
k = head(m)
flag = iand(sf(k),64+8)
fixed(m) = (flag == 0)
frag = 0
if (present(only)) then
  if (only > 0) then
    frag = only
  end if
end if

do while (k1>0)
  !  The 8 bit of SF is set if the site orientation is variable, and the 64
  !  bit if the position is variable. These values must be propagated down
  !  the site list, and into the FIXED array.
  !  Remember the mapping between sites k and k1.
  sw(k1)=k
  sw(k)=k1
  type(k)=type(k1)
  l(k)=l(k1)
  q(:,k)=q(:,k1)
  level(k)=level(k1)
  link(k)=sw(link(k1))
  if (iand(sf(k),64+8) .ne. 0) fixed(m)=.false.
  !  Note: Bonding information not currently transferred
  last=k
  next(k)=0
  do while (k1 > 0)
    !  Move to next site
    k1=next(k1)
    if (k1 > 0) then
      if (fragment(k1) == 0 .or. frag == 0 .or. fragment(k1) == frag) then
        !  Copy this site
        !  Get new site k for molecule m
        if (free > 0) then
          k=free
          free=next(free)
        else
          tops=tops+1
          k=tops
        endif
        if (k .ge. nsites) call die('Too many sites.',.true.)

        next(last)=k
        name(k)=name(k1)
        sf(k)=ior(sf(k1),flag)
        sp(:,k)=sp(:,k1)
        sr(:,k)=sr(:,k1)
        st(k)=st(k1)
      end if
      exit ! and finish copying this site in outer loop
    ! else not copied; look at next site
    end if
  end do
end do

END SUBROUTINE copy

END SUBROUTINE mdata
