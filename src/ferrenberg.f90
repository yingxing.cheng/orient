      SUBROUTINE expectation_epot(tmin,tmax,tsamp,ntemp,xhist,hist,     &
                                  nhist,engfact,filename)

      USE consts, ONLY : ehvk, ehvkjm, mgc
      USE thermofns, ONLY : degsfreedom
      IMPLICIT NONE

      INTEGER :: ntemp,itemp,i,nhist,ncomplex,nconstituents
      DOUBLE PRECISION :: tmin,tmax,tsamp,temp,hist(nhist),             &
          xhist(nhist),exp_epot_nom,exp_epot2_nom,exp_epot_denom,       &
          exponent,max_exponent,exp_epot,exp_epot2,engfact,Cv
      CHARACTER(LEN=32) :: filename


      open(99,FILE=filename,STATUS='UNKNOWN')

      do itemp=0,ntemp-1
       temp=dble(itemp)/dble(ntemp-1)*(tmax-tmin)+tmin
       exp_epot_nom=0.0d0
       exp_epot2_nom=0.0d0
       exp_epot_denom=0.0d0
       max_exponent=0d0
       do i=1,nhist
         max_exponent=max(max_exponent,-xhist(i)*(1.0d0/temp-1.0d0/tsamp))
       end do
       do i=1,nhist
         exponent=-xhist(i)*(1.0d0/temp-1.0d0/tsamp)-max_exponent
         exp_epot_denom=exp_epot_denom+hist(i)*exp(exponent)

!  1. moment of the potential energy
         exp_epot_nom=exp_epot_nom+                                     &
             xhist(i)*hist(i)*exp(exponent)

!  2. moment of the potential energy
         exp_epot2_nom=exp_epot2_nom+                                   &
             xhist(i)*xhist(i)*hist(i)*exp(exponent)

       end do
       exp_epot=exp_epot_nom/exp_epot_denom
       exp_epot2=exp_epot2_nom/exp_epot_denom
!  Calculate heat capacity from moments
       call degsfreedom(ncomplex,nconstituents)
       Cv=(exp_epot2-exp_epot*exp_epot)*1d3*EHVKJM/(temp*temp*EHVK)     &
                  +0.5d0*ncomplex*MGC                           !J/mol/K

       write (99,'(1p,e20.12,3(1x,1p,e20.12))') temp*EHVK,                &
                  exp_epot*engfact,                                     &
                  exp_epot2*engfact*engfact,                            &
                  Cv  !J/mol/K
      end do

      close(99)

      END SUBROUTINE expectation_epot

!-----------------------------------------------------------------------

      SUBROUTINE expectation_epot_anl(tmin,tmax,tsamp,ntemp,ghist,      &
                                      engfact,fitorder,filename)
      USE gammafns, ONLY :dgagnc, erf
      USE consts, ONLY : sqrtpi
      IMPLICIT NONE

      DOUBLE PRECISION :: tmin,tmax,tsamp,temp,c2,fac1,fac2,c2bb,exp1
      DOUBLE PRECISION :: exp_epot_nom,exp_epot_denom
      INTEGER :: ntemp,i,j,fitorder
      DOUBLE PRECISION :: ghist(fitorder)
      DOUBLE PRECISION :: engfact
      CHARACTER(LEN=32) :: filename

      open(99,FILE=filename,STATUS='UNKNOWN')

      do i=0,ntemp-1
       exp_epot_nom=0.0d0
       exp_epot_denom=0.0d0
       temp=dble(i)/dble(ntemp-1)*(tmax-tmin)+tmin
       do j=0,fitorder/3-1
        print *,i,j,'A'
        c2=ghist(3*j+3)*ghist(3*j+3)
        fac1=ghist(3*j+1)*c2/sqrt(c2)*(-c2*tsamp+c2*temp-               &
             2.0d0*ghist(3*j+2)*tsamp*temp)/(8.0d0*tsamp*temp)
        c2bb=c2*(2.0d0*ghist(3*j+2)/c2-1.0d0/tsamp+1.0d0/temp)**2
        exp1=-ghist(3*j+2)*ghist(3*j+2)/c2+c2bb/4.0d0
        print *,c2bb,-2.0d0*sqrtpi
!       fac2=2.0d0*sqrtpi+                                              &
!            exp(gammln(-0.5d0))*(1.0d0-gammq(-0.5d0,c2bb/4.0d0))
!  gamma(-1/2)=-2sqrt(pi)!
        fac2=-(2d0/sqrt(c2bb)*exp(-c2bb/4.0d0)*dgagnc(-0.5d0,c2bb/4.0d0))
!       fac2=2.0d0*sqrtpi*gammq(-0.5d0,c2bb/4.0d0)
        print *,'after'
        exp_epot_nom=exp_epot_nom+fac1*exp(exp1)*fac2
        print *,i,j,'B'

        fac1=ghist(3*j+1)*c2/sqrt(c2)*sqrtpi/2.0d0
        c2bb=(2.0d0*ghist(3*j+2)/c2-1.0d0/tsamp+1.0d0/temp)*sqrt(c2)
        fac2=1.0d0-erf(c2bb/2.0d0)
        exp_epot_denom=exp_epot_denom+fac1*exp(exp1)*fac2
       end do
       write (99,'(1p,e20.12,1x,1p,e20.12)') temp,                        &
                 exp_epot_nom/exp_epot_denom*engfact
      end do

      close(99)

      END SUBROUTINE expectation_epot_anl
