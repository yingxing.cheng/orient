      SUBROUTINE fourier

      USE consts
      USE input
      USE labels
      USE variables, ONLY: vtype, freek
      USE sites
      USE sizes
      USE molecules

      IMPLICIT NONE

      CHARACTER(LEN=16) :: word

      INTEGER, PARAMETER :: order=3
      INTEGER, PARAMETER :: n=2*order
      INTEGER :: na ! number of angles being varied (1 or 2)

      INTEGER :: i, j, jj, k, m, mf, kf, kq, nq, nv, n2, last, step
      DOUBLE PRECISION :: f, ff, phi(2), cs(0:N,2),                    &
          factor(0:(N+1)**2)
      LOGICAL :: first, finish, all, add, check
      LOGICAL :: done(0:15,0:15)
      SAVE na, step, nv, n2, ff, mf, done
      CHARACTER(LEN=2) ::                                              &
          tag(0:2*order) = (/'a0', 'a1', 'b1', 'a2', 'b2', 'a3', 'b3'/)

      add=.false.
      check=.false.
      first=.false.
      finish=.false.
      do while (item .lt. nitems)
        call readu(word)
        if (word .eq. 'START') then
          first = .true.
          na=1
          step=30
          nv=12
          n2=0
          done(:,:)=.false.
        else if (word .eq. 'FINISH') then
          finish = .true.
        else if (word .eq. "STEP") then
          call readi(step)
          nv=360/step
        else if (word .eq. "SINGLE") then
          na=1
          n2=0
        else if (word .eq. "DOUBLE") then
          na=2
          n2=N
        else if (word .eq. "CHECK") then
          check=.true.
          call readf(phi(1))
          phi(2)=0d0
          if (na .eq. 2) call readf(phi(2))
        else
          call reread(-1)
          call reada(word)
          m=locate(word)
          if (m .eq. 0) call die("Can't find molecule "//word,.true.)
          call readf(phi(1))
          phi(2)=0d0
          if (na .eq. 2) call readf(phi(2))
          add=.true.
        endif
      end do

      if (first) then
!  Set up a dummy molecule to hold the Fourier coefficients. It will use
!  (N+1)**na sites for each site of the real molecule.
        mf=mols+1
        head(mf)=tops+1
        kq=tops+1
        kf=tops+1
        k=head(m)
        do while (k .gt. 0)
          if (l(k) .ge. 0) then
            name(kf)=name(k)
            l(kf)=l(k)
            kq=kf
            do j=1,(N+1)**na
              do i=1,sq
                q(i,kf)=0d0
              end do
              kf=kf+1
              if (kf .gt. nsites+1) call die                          &
                   ('Not enough sites available',.true.)
            end do
            next(kq)=kf
          end if
          k=next(k)
        end do
        next(kq)=0
        tops=kf-1
        first=.false.

!  Define some constants
        ff=(2d0/nv)**na
        print '(A,I2,A)',                                                &
           'Fourier transformation with', na, ' variable angles'

      endif

      if (add .or. check) then
!  Calculate sines and cosines for the current molecule
        if (add) then
          f=ff
          cs(0,1)=1d0
          cs(0,2)=1d0
        else
          f=1d0
!  The a_0 coefficient has to be divided by 2. Use a fudged value for
!  the corresponding cosine to achieve this.
          cs(0,1)=0.5d0
          if (na .eq. 2) then
            cs(0,2)=0.5d0
          else
            cs(0,2)=1d0
          endif
        endif
        do i=1,na
          do j=1,ORDER
            cs(2*j-1,i)=cos(j*phi(i)/afact)
            cs(2*j,i)=sin(j*phi(i)/afact)
          end do
        end do
        k=0
        do i=0,n2
          do j=0,N
            factor(k+j)=f*cs(j,1)*cs(i,2)
          end do
!         print '(7f8.5)', (factor(k+j), j=0,N)
          k=k+N+1
        end do
        last=k-1
      endif

      if (add) then
!  Loop through sites for this molecule, accumulating Fourier contributions
        k=head(m)
        kf=head(mf)
        do while (k .gt. 0)
          if (l(k) .ge. 0) then
            nq=qsize(l(k))
            do j=0,last
              do i=1,nq
                q(i,kf+j)=q(i,kf+j)+q(i,k)*factor(j)
              end do
            end do
            kf=next(kf)
          endif
          k=next(k)
        end do

!  Mark the angles as done
        do i=1,na
          do while (phi(i) .lt. 0d0)
            phi(i)=phi(i)+360d0
          end do
          do while (phi(i) .ge. 360d0)
            phi(i)=phi(i)-360d0
          end do
        end do
        if (done(int(phi(1)/step+0.5),int(phi(2)/step+0.5))) then
          print '(A,F7.0,A,F7.0)',                                       &
             'Multipoles provided twice for phi(1) =', phi(1),           &
             ', phi(2) =', phi(2)
        else
          done(int(phi(1)/step+0.5),int(phi(2)/step+0.5))=.true.
        endif

!  Delete this molecule so that the space can be re-used for the next one
        k=head(m)
        do while (k .gt. 0)
          do i=1,3
            if (sp(i,k) .gt. 0) then
              vtype(sp(i,k))=freek
              freek=sp(i,k)
              sp(i,k)=0
            endif
          end do
          do i=1,4
            if (sr(i,k) .gt. 0) then
              vtype(sr(i,k))=freek
              freek=sr(i,k)
              sr(i,k)=0
            endif
          end do
          j=k
          k=next(k)
        end do
        next(j)=free
        free=head(m)
        name(head(m))=' '
        head(m)=0
      endif

      if (finish) then

!  Check that we have read in all the molecular distributed multipoles
        all=.true.
        if (na .eq. 1) then
          do j=0,nv-1
            if (.not. done(i,0)) then
              print '(A,I4)',                                            &
                'No multipoles for phi =', i*step
              all=.false.
            end if
          end do
        else
          do j=0,nv-1
            do i=0,nv-1
              if (.not. done(i,j)) then
                print '(A,I4,A,I4)', 'No multipoles for phi(1) =',       &
                     i*step, ' phi(2) =', j*step
                all=.false.
              end if
            end do
          end do
        endif
        if (.not. all) call die("Missing values", .false.)

!  If so we have constructed the Fourier coefficients
        kf=head(mf)
        if (na .eq. 1) then
          do while (kf .gt. 0)
            print '(/A5,7(3x,A2,5x))',                                   &
               name(kf), (tag(j), j=0,last)
            do i=1,qsize(l(kf))
              print '(2A,7f10.5)',                                       &
                   'Q', label(i), (q(i,kf+j), j=0,last)
            end do
            kf=next(kf)
          end do
        else
          do while (kf .gt. 0)
            print "(/A,A)", "Fourier coefficients for ", name(kf)
            do i=1,qsize(l(kf))
              print '(/2A,7(3x,A2,5x))',                                 &
                  'Q', label(i), (tag(j), j=0,N)
              k=0
              do j=0,last,N+1
                print '(A,7f10.5)', tag(k), (q(i,kf+j+jj), jj=0,N)
                k=k+1
              end do
            end do
            kf=next(kf)
          end do
        end if
      end if

      if (check) then
        print '(/A,F7.1,A,F7.1)',                                        &
           'Multipoles from fourier series for phi(1) =', phi(1),        &
           ', phi(2) =', phi(2)
        kf=head(mf)
        k=free
        if (k .eq. 0) k=tops+1
        do while (kf .gt. 0)
          print '(/A5)', name(kf)
          do i=1,qsize(l(kf))
            q(i,k)=0d0
            do j=0,last
              q(i,k)=q(i,k)+q(i,kf+j)*factor(j)
            end do
          end do
          print                                                          &
          '(5X,F10.5:/5X,3F10.5:/5x,5F10.5:/5x,7F10.5:'//                &
          '/5x,7F10.5/15x,2F10.5)',                                      &
                   (q(i,k), i=1,qsize(l(kf)))
          kf=next(kf)
        end do
      endif

      END SUBROUTINE fourier
