MODULE pairderiv

USE consts, ONLY: dp

REAL(dp) :: u0(6), u1(12,6), u2(12,12,6),                              &
    alpha0, alpha1(12), alpha2(12,12),  rho0, rho1(12), rho2(12,12),   &
    es0, es1(12), es2(12,12),                                          &
    er0, er1(12), er2(12,12), prefac0, prefac1(12), prefac2(12,12),    &
    disp0, disp1(12), disp2(12,12),                                    &
    c0(6:12), c1(12,6:12), c2(12,12,6:12)



END MODULE pairderiv
