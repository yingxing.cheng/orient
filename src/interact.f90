MODULE interact

USE switches, ONLY: debug

IMPLICIT NONE

!  This module holds information concerned with a particular pair
!  interaction.
!  PAIRMAX is the maximum number of S functions involved in a single
!  site-site interaction. (There could be up to 301 electrostatic terms.)
!  list() contains the list of S functions needed for this site-site
!  pair. For a particular S function, Q1 and Q2 are the multipole
!  moments and FAC the binomial coefficient that appears
!  in the electrostatic interaction. POWER is (minus) the power of R, the
!  site-site distance.
!  alphax, rhox and Cx(n) are the coefficients of the repulsion and
!  dispersion terms. index(n) is the index ix of the corresponding values
!  in the alpha and rho arrays. isdisp(n) is true if there are any R^-n
!  dispersion terms.

INTEGER :: pairmax=0
!     PARAMETER (PAIRMAX=400)

DOUBLE PRECISION, POINTER :: e1r(:), e2r(:), r
!     SAVE E1R, E2R, XX, R
DOUBLE PRECISION, TARGET :: cosine(16)
!     equivalence                                                        &
!         (cosine(1),e1r(1)), (cosine(4),e2r(1)), (cosine(7),xx(1,1)),   &
!         (cosine(16), R)
DOUBLE PRECISION, POINTER :: rax, ray, raz, rbx, rby, rbz,               &
    cxx, cyx, czx, cxy, cyy, czy, cxz, cyz, czz
!     equivalence (rax, e1r(1)), (ray, e1r(2)), (raz, e1r(3)),           &
!         (rbx, e2r(1)), (rby, e2r(2)), (rbz, e2r(3)),                   &
!         (cxx, xx(1,1)), (cxy, xx(1,2)), (cxz, xx(1,3)),                &
!         (cyx, xx(2,1)), (cyy, xx(2,2)), (cyz, xx(2,3)),                &
!         (czx, xx(3,1)), (czy, xx(3,2)), (czz, xx(3,3))

!  list will contain a list of the S functions that occur in the current
!  pair interaction, identified by their S-function index.
INTEGER, ALLOCATABLE, SAVE :: list(:), power(:), index(:)
INTEGER :: nesmax, nmax, pair
DOUBLE PRECISION erfactor
LOGICAL isdisp(6:12)

DOUBLE PRECISION, SAVE :: D1(16,12), D2(16,12,12)
DOUBLE PRECISION, ALLOCATABLE, SAVE ::                             &
    S0(:), S1(:,:), S2(:,:), D1S(:,:), D2S(:,:,:),                 &
    q1(:), q2(:), fac(:), alphax(:), rhox(:), cx(:,:)
INTEGER, ALLOCATABLE :: ranks(:,:)

LOGICAL :: angle_axis_derivs=.false.

PUBLIC

PRIVATE pairinfo_aa, pairinfo_tq

CONTAINS

SUBROUTINE init_interact

e1r => cosine(1:3)
e2r => cosine(4:6)

rax => cosine(1)
ray => cosine(2)
raz => cosine(3)

rbx => cosine(4)
rby => cosine(5)
rbz => cosine(6)

cxx => cosine(7)
cyx => cosine(8)
czx => cosine(9)

cxy => cosine(10)
cyy => cosine(11)
czy => cosine(12)

cxz => cosine(13)
cyz => cosine(14)
czz => cosine(15)

r => cosine(16)

END SUBROUTINE init_interact

SUBROUTINE alloc_interact(newmax)

USE input, ONLY : die

INTEGER, INTENT(IN) :: newmax
INTEGER :: ok, sfnsize

if (allocated(list)) then
  if (newmax .le. pairmax) return
  deallocate(list, power, index, S0, S1, S2, D1S, D2S, q1, q2, fac,&
      alphax, rhox, cx)
endif
pairmax=newmax
allocate(list(pairmax), power(pairmax), index(pairmax),            &
    S0(pairmax), S1(15,pairmax), S2(120,pairmax), D1S(12,pairmax), &
    D2S(12,12,pairmax),q1(pairmax), q2(pairmax), fac(pairmax),     &
    alphax(pairmax), rhox(pairmax), cx(pairmax,6:12),              &
    ranks(2,pairmax), stat=ok)
if (ok>1) then
  print "(a)", "Can't allocate arrays for S-function list."
  sfnsize=pairmax*(12+8*(1+15+120+12+144+12))
  print "(a,i0,a,i0,a)",                                               &
      "Space needed for ", pairmax, " S functions is ", sfnsize, " bytes."
  call die("Calculation abandoned")
endif

END SUBROUTINE alloc_interact

!----------------------------------------------------------------------

SUBROUTINE pairinfo(mA,ka,mB,kb,order)

INTEGER, INTENT(IN) :: ka, kb, mA, mB, order

if (angle_axis_derivs) then
  call pairinfo_aa(mA,ka,mB,kb,order)
else
  call pairinfo_tq(mA,ka,mB,kb,order)
end if

END SUBROUTINE pairinfo

!----------------------------------------------------------------------

SUBROUTINE aaderiv(mm, force)

!  Calculate the molecular orientation matrix and its derivatives for
!  molecule mm, or for all molecules if mm=0. It's assumed that the
!  angle-axis variables are in mol(m)%p(:).

USE molecules
! USE sites, ONLY: name
! USE consts, ONLY: pi, afact

!  p(:) is the set of angle-axis coordinates for this molecule
!  n=p/psi is the unit vector describing the rotation axis.
!  N0(:,:) is the skew-symmetric matrix corresponding to the unit
!  vector n parallel to p
!  N1(:,:,k) is the derivative of this matrix w.r.t. p(k)
!  M(:,:) is the molecular rotation matrix: (M(i,j) is the direction
!  cosine between global axis i and molecular axis j. Same as se(:,:,cm(m))
!  M1(:,:,k) is the first derivative of M w.r.t. p(k).

INTEGER, INTENT(IN) :: mm
LOGICAL, INTENT(IN), OPTIONAL :: force
REAL(dp) :: p(3), n(3), psi
REAL(dp) :: N0(3,3), N1(3,3,3)
INTEGER :: k, m, m1, m2


if (mm==0) then
  m1=1
  m2=mols
else
  m1=mm
  m2=mm
end if

do m=m1,m2
if (present(force)) then
  if (.not. force .and. mol(m)%aaderivs_done) return
end if
  
  p=mol(m)%p(:)
  ! print "(a,3f12.8)", "aaderiv: p = ", p
  psi=sqrt(p(1)**2+p(2)**2+p(3)**2)
!   !  This code shifts psi into the range 0 <= psi <= pi, changing the
!   !  sign of the n vector if necessary. Not a good idea when optimizing
!   !   because it introduces a discontinuity.
!   if (psi>2d0*pi) then
!     psi0=psi
!     do while (psi>2d0*pi)
!       psi=psi-2d0*pi
!     end do
!     p=p*psi/psi0
!     print "(3a,f12.6,a,f12.6)", "Molecule ", trim(name(head(m))),      &
!         ": psi>2pi. Changed from ", psi0*afact, " to ", psi*afact
!   endif
!   if (psi>pi) then
!     psi0=psi
!     psi=2d0*pi-psi0
!     p=-p*psi/psi0
!     print "(3a,f12.6,a,f12.6)", "Molecule ", trim(name(head(m))),      &
!         ": psi>pi. Changed from ", psi0*afact, " to ", -psi*afact
!   end if
  !  Note: reshape fills the 3x3 array in element order, i.e. by columns.
  if (psi<1d-8) then
    mol(m)%M(:,:)=reshape([1d0,p(3),-p(2),-p(3),1d0,p(1),p(2),-p(1),1d0],[3,3])
    mol(m)%M1(:,:,1)=reshape([0d0,0d0,0d0,0d0,0d0,1d0,0d0,-1d0,0d0],[3,3])
    mol(m)%M1(:,:,2)=reshape([0d0,0d0,-1d0,0d0,0d0,0d0,1d0,0d0,0d0],[3,3])
    mol(m)%M1(:,:,3)=reshape([0d0,1d0,0d0,-1d0,0d0,0d0,0d0,0d0,0d0],[3,3])
  else
    n=p/psi
    N0=reshape([0d0,n(3),-n(2),-n(3),0d0,n(1),n(2),-n(1),0d0],[3,3])
    mol(m)%M(:,:)=reshape([1d0,0d0,0d0,0d0,1d0,0d0,0d0,0d0,1d0],[3,3])   &
        + matmul(N0,N0)*(1d0-cos(psi))+N0*sin(psi)
    N1(:,:,1)=(1d0/psi)*reshape([0d0,-n(1)*n(3),n(1)*n(2),               &
        n(1)*n(3),0d0,1d0-n(1)**2,-n(1)*n(2),n(1)**2-1d0,0d0],[3,3])
    N1(:,:,2)=(1d0/psi)*reshape([0d0,-n(2)*n(3),n(2)**2-1,               &
        n(2)*n(3),0d0,-n(1)*n(2),1d0-n(2)**2,n(1)*n(2),0d0],[3,3])
    N1(:,:,3)=(1d0/psi)*reshape([0d0,1d0-n(3)**2,n(2)*n(3),              &
        n(3)**2-1d0,0d0,-n(1)*n(3),-n(2)*n(3),n(1)*n(3),0d0],[3,3])
    do k=1,3
      mol(m)%M1(:,:,k)=n(k)*sin(psi)*matmul(N0,N0)                       &
          + (1d0-cos(psi))*(matmul(N1(:,:,k),N0)+matmul(N0,N1(:,:,k)))   &
          + n(k)*cos(psi)*N0 + sin(psi)*N1(:,:,k)
    end do
  end if

  !  Currently ignored
  mol(m)%aaderivs_done=.true.
end do

END SUBROUTINE aaderiv

!----------------------------------------------------------------------

SUBROUTINE pairinfo_aa(mA,ka,mB,kb,order)

!  Calculate the scalar products of the local axis vectors and the
!  inter-site vector (magnitude R and unit vector ER) for a pair of sites.
!  If order>0, the derivatives of these quantities with respect to the
!  angle-axis parameters describing the molecular orientations are also
!  calculated.

USE global
USE sites
USE molecules
!  USE interact
IMPLICIT NONE

!  Input:
!     mA, mB: The two molecules being considered.
!     ka, kb: Site pointers.
!     order:  Order of derivatives needed.

!  It is also required that the arrays slink and srotlink contain the
!  positions and orientations of the sites relative to the respective
!  molecular centres of mass and inertial axes. The position of the
!  molecular centre of mass must be in mol(m)%cm.
!  The rotation matrix mol(m)%M(:,:) must have been calculated from the
!  angle-axis coordinates p(k), and likewise its derivatives
!  mol(m)%M1(:,:,k), if needed.

!  The global position and orientation (sx and se) are not required,
!  but the se matrices are updated here.


INTEGER, INTENT(IN) :: ka, kb, mA, mB, order

!  Output:
!  The following arrays are stored in the module "interact".
!    E1R(3)    E1R(i) is the scalar product of the unit vector e_i
!              for site a (index ka) with the unit inter-site vector ER
!              (components of ER in the axis system of site a)
!    E2R(3)    Scalar product of the unit vector e_i for site b (index kb)
!              with the unit inter-site vector -ER (components of -ER in
!              the axis system of site b). NOTE SIGN!
!    xx(3,3)   Scalar products of unit vectors for site a and site b
!    R         distance between sites
!  The above (in that order) comprise the 16 intermediate variables q(i).
!  Note that they involve the unit intermolecular vector in this
!  implementation.
!    D1(16,12) First derivatives of the 16 intermediate variables w.r.t. the
!              molecular position and angle-axis variables:
!              1:3  position A
!              4:6  angle-axis vector p for A
!              7:9  position B
!             10:12 angle-axis vector p for B
!    D2(16,12,12) Second derivatives. Not implemented for angle-axis variables.


DOUBLE PRECISION ::                                                &
    rab(3),      & ! site-site vector R
    er(3),       & ! normalised site-site vector
    a(3), b(3),  & ! site vectors relative to molecular origin (c. of m.)
    ra(3), rb(3)   ! R+a and R-b respectively

INTEGER :: i, pa, pb
!  INTEGER :: j, j1, j2, k, m, p(3), i1(3)=[2,3,1], i2(3)=[3,1,2]
DOUBLE PRECISION :: xx(3,3)

LOGICAL :: debug=.false.

!  Pointers to site orientation matrices.
pa=sm(ka)
pb=sm(kb)

! print "(3(a, 2i3))", "Molecules ", mA, mB, "  Sites ", ka, kb,          &
!     "  Pointers ", pa, pb

!  We assume that the rotation matrix for molecule m is in 
!  mol(m)%M(:,:) and its derivatives w.r.t. the angle-axis coordinates
!  p(k) (if needed) are in mol(m)%M1(:,:,k).

!  Scalar products of local axis vectors. xx(i,j) = wa(i) . wb(j)
xx=matmul(transpose(matmul(mol(mA)%M,srotlink(:,:,pa))),matmul(mol(mB)%M,srotlink(:,:,pb)))
cosine(7:15)=reshape(xx,(/9/))

!  With centres of mass at vector positions A and B and sites at A+a
!  and B+b, the intersite vector is R=B+b-A-a, i.e. in the direction
!  from A to B. All in global axes.

! debug=(ka==2 .and. kb==5 .and. order>0)

a=matmul(mol(mA)%M,slink(:,ka))
b=matmul(mol(mB)%M,slink(:,kb))
sx(:,ka)=mol(mA)%cm+a
sx(:,kb)=mol(mB)%cm+b
rab=sx(:,kb)-sx(:,ka)
r=sqrt(rab(1)**2+rab(2)**2+rab(3)**2)

er=rab/r   ! normalized intersite vector

!  Construct orientation matrices for sites ka and kb if necessary. Not
!  necessary if ka .ne. pa because then ka uses the matrix for pa, which
!  will already have been calculated. 
! if (ka .eq. pa) then
  se(:,:,ka)=matmul(mol(mA)%M,srotlink(:,:,pa))
! end if
! if (kb .eq. pb) then
  se(:,:,kb)=matmul(mol(mB)%M,srotlink(:,:,pb))
! end if

if (debug) then
print "(a, i0, a, 3f10.5/a/(3f10.5))", "Site ", ka, " position", sx(:,ka), &
    "orientation ", se(:,:,ka)
print "(a, i0, a, 3f10.5/a/(3f10.5))", "Site ", kb, " position", sx(:,kb), &
    "orientation ", se(:,:,kb)
end if


!  e1r is the unit intersite vector from a to b in the local axes of
!  site a, and e2r is the unit intersite vector from b to a in the 
!  local axes of site b.
e1r(:)=matmul(er,se(:,:,ka))
e2r(:)=-matmul(er,se(:,:,kb))  ! Note sign


! print "(6f8.4/3(3f8.4)/f10.4)", cosine

if (debug) print "(a/15f6.3,f8.3)", "cosines", cosine

!  If order=0 this is all we need

if (order .lt. 1) return

!  R = rab = B+b-A-a, so ra=B+b-A, which is constant with respect to
!  rotations of A. Similarly rb=B-A-a, constant with respect to
!  rotations of B.
ra = rab + a
rb = rab - b

if (debug) print "(a,3f8.3/a,3f8.3)", "ra = ", ra, "rb = ", rb

!  For the derivatives w.r.t. angle-axis coordinates we need the
!  derivatives of the molecular rotation matrix w.r.t. the angle-axis
!  parameters (px,py,pz).

!  We assume that the rotation matrix for molecule m is in 
!  mol(mA)%M(:,:) and its derivatives w.r.t. the angle-axis coordinates
!  p(k) are in mol(m)%M1(:,:,k).

D1=0d0

!  Now compute first derivatives of |R|, wa.R, -wb.R and wa.wb w.r.t.
!  molecule positions -- coordinates 1:3 for molecule A and 7:9 for
!  molecule B. The scalar products involve the vector R, not the unit
!  vector er.

if (debug) then
  print "(a,i0,a/(3f10.5))", "se(:,:,",ka,"): ", se(:,:,ka)
  print "(a,i0,a/(3f10.5))", "se(:,:,",kb,"): ", se(:,:,kb)
end if

!  R     (16)
D1(16,1:3)=-rab(:)/R
D1(16,7:9)=rab(:)/R
!  R.wa  (1:3)
D1(1:3,1:3)=-transpose(se(:,:,ka))
D1(1:3,7:9)=transpose(se(:,:,ka))
!  -R.wb  (4:6)
D1(4:6,1:3)=transpose(se(:,:,kb))
D1(4:6,7:9)=-transpose(se(:,:,kb))
!  wa.wb (7:15)
D1(7:15,1:3)=0d0
D1(7:15,7:9)=0d0



!  Now first derivatives w.r.t. molecule rotations -- coordinates 4:6
!  for molecule A and 10:12 for molecule B.
!  R     (16)
do i=1,3
if (debug) then
  print "(a,i0,a/(3f10.5))", "M1A(",i,"): ", mol(mA)%M1(:,:,i)
  print "(a,i0,a/(3f10.5))", "M1B(",i,"): ", mol(mB)%M1(:,:,i)
  print "(a,i0,a/(3f10.5))", "srotlink(:,:,",pa,"): ", srotlink(:,:,pa)
  print "(a,i0,a/(3f10.5))", "srotlink(:,:,",pb,"): ", srotlink(:,:,pb)
  print "(a,i0,a,3f10.5)", "slink(:,",ka,"): ", slink(:,ka)
  print "(a,i0,a,3f10.5)", "slink(:,",kb,"): ", slink(:,kb)
end if
  !  R     (16)
  D1(16,3+i)=-dot_product(ra,matmul(mol(mA)%M1(:,:,i),slink(:,ka)))/R
  D1(16,9+i)=dot_product(rb,matmul(mol(mB)%M1(:,:,i),slink(:,kb)))/R
  !  R.wa  (1:3)
  D1(1:3,3+i)=matmul(ra,matmul(mol(mA)%M1(:,:,i),srotlink(:,:,pa)))
  D1(1:3,9+i)=matmul(matmul(mol(mB)%M1(:,:,i),slink(:,kb)),se(:,:,ka))
  !  -R.wb  (4:6)
  D1(4:6,3+i)=matmul(transpose(se(:,:,kb)),matmul(mol(mA)%M1(:,:,i),slink(:,ka)))
  D1(4:6,9+i)=-matmul(rb,matmul(mol(mB)%M1(:,:,i),srotlink(:,:,pb)))
  !  wa.wb  (7:15)
  D1(7:15,3+i)=reshape(matmul(transpose(matmul(mol(mA)%M1(:,:,i),srotlink(:,:,pa))), &
      matmul(mol(mB)%M(:,:),srotlink(:,:,pb))),[9])
  D1(7:15,9+i)=reshape(matmul(transpose(matmul(mol(mA)%M(:,:),srotlink(:,:,pa))), &
      matmul(mol(mB)%M1(:,:,i),srotlink(:,:,pb))),[9])
end do

if (debug) then
  print "(a/(a,2F20.12))", "D1         pxA                 pyA",       &
      "rbx", D1(4,4:5),    &
      "rby", D1(5,4:5)
end if

!  Now we have to manipulate the derivatives of the scalar products
!  with R to get derivatives of the unit-vector scalar products.
do i=1,3
  D1(i,:)=(D1(i,:)-e1r(i)*D1(16,:))/R
  D1(3+i,:)=(D1(3+i,:)-e2r(i)*D1(16,:))/R
end do
if (debug) then
  print "(a/(a,2F20.12))", "D1         pxA                 pyA",       &
      "rax", D1(4,4:5),    &
      "ray", D1(5,4:5)
end if

if (order .lt. 2) return

call die("Second derivatives w.r.t. angle-axis coordinates not implemented")

END SUBROUTINE pairinfo_aa

!----------------------------------------------------------------------

SUBROUTINE pairinfo_tq(m1,k1,m2,k2,order)

USE global
USE sites
USE molecules
! USE interact
IMPLICIT NONE

!  Calculate the scalar products of the local axis vectors and the
!  inter-site vector (magnitude R and unit vector ER) for a pair of sites.
!  If order>0, the vector products are also calculated for use in the
!  derivative formulae.

!  This is the original version, which calculates the torque.

!  Input:
!     m1, m2: The two molecules being considered.
!     k1, k2: Site pointers.
!     order:  Order of derivatives needed.
INTEGER, INTENT(IN) :: k1, k2, m1, m2, order

!  Output:
!  The following arrays are stored in the module "interact".
!    E1R(3)    E1R(i) is the scalar product of the unit vector e_i
!              for site k1 with the unit inter-site vector ER.
!              (components of ER in the axis system of site k1)
!    E2R(3)    Scalar product of the unit vector e_i for site k2
!              with the unit inter-site vector -ER (components of -ER in
!              the axis system of site k2). NOTE SIGN!
!    xx(3,3)   Scalar products of unit vectors for site k1 and site k2.
!    R         Distance between sites.
!  The above comprise the 16 intermediate variables q(i). Note that they
!  involve the unit intermolecular vector in this implementation.
!    D1(16,12) First derivatives of the 16 intermediate variables w.r.t. the
!              molecular position and torque variables
!    D2(16,12,12) Second derivatives.



DOUBLE PRECISION ::                                                &
    rab(3),            & ! site-site vector R
    er(3),             & ! normalised site-site vector
    a(3), b(3),        & ! site vectors relative to molecular origin (c. of m.)
    ra(3), rb(3),      & ! R+a and R-b respectively
    rxa(3), rxb(3),    & ! R x a and R x b
    raxw1(3,3),        & ! (R+a) x w1     Note: w1(i,j) = se(i,j,p1)
    rbxw2(3,3),        & ! (R-b) x w2           w2(i,j) = se(i,j,p2)
    w2xa(3,3),         & ! w2 x a
    w1xb(3,3),         & ! w1 x b
    w1xw2(3,3,3),      & ! w1 x w2
    ab, raa, rbb,      & ! a . b, (R+a) . a, (R-b) . b
    aw1(3), bw1(3),    & ! a . w1, b . w1
    aw2(3), bw2(3),    & ! a . w2, b . w2
    w1ra(3), w2rb(3)   ! w1 . (R+a), w1 . (R-b)
!  DOUBLE PRECISION :: d11j,d12j,d13j,d14j,d15j,d16j,d116j,d116i,d216ij
!  In the vector products, the first index labels the vector component.
!  w1xw2(i,j1,j2) is the cross product of local unit vector w1(j1)
!  for site k1 with local unit vector w2(j2) for site k2.

INTEGER :: i, j, j1, j2, k, m, p1, p2, i1(3), i2(3)
DOUBLE PRECISION :: xx(3,3)

data i1 /2,3,1/, i2 /3,1,2/

!  Pointers to orientation matrices
p1=sm(k1)
p2=sm(k2)

!  Scalar products of local axis vectors. xx(i,j) = w1(i) . w2(j)
!  Not optimised
!     do i=1,3
!       do j=1,3
!         xx(i,j)=0.D0
!         do k=1,3
!           xx(i,j)=xx(i,j)+se(k,i,p1)*se(k,j,p2)
!         end do
!       end do
!     end do
!  Optimised
do j=1,3
  do i=1,3
    xx(i,j)=se(1,i,p1)*se(1,j,p2)                                &
        +se(2,i,p1)*se(2,j,p2)                                &
        +se(3,i,p1)*se(3,j,p2)
  end do
end do
cosine(7:15)=reshape(xx,(/9/))
!  End optimised

! print "(2(a,i0,a,3f8.4))", "Sites ", k1, " at", sx(:,k1),               &
!     " and ", k2, " at", sx(:,k2)
r=0d0
do i=1,3
  rab(i)=sx(i,k2)-sx(i,k1)
  r=r+rab(i)**2
end do
!      if (r .eq. 0d0) then
!        print "(a,i0,a,i0,a)", "sites ", k1, " and ", k2, " coincide"
!        call die ("Sites coincide", .true.)
!      end if
r=sqrt(r)


if (r < 0.001d0) then  ! sites are too close : error report
  write (6,'(/1X,2(A,I3,A,I2),A,1P,E12.5,A)')                          &
      'Sites', K1, ' in molecule', M1, ' and', K2, ' in molecule',     &
      M2, ' are only', r, ' bohr apart.'
  call axes(m1,.true.)
  call axes(m2,.true.)
  call die("TASK ABANDONED.")
end if

do i=1,3
  er(i)=rab(i)/r   ! normalized intersite vector
end do

e1r=0d0
e2r=0d0
do i=1,3
  do j=1,3
    e1r(i)=e1r(i)+er(j)*se(j,i,p1)
    e2r(i)=e2r(i)-er(j)*se(j,i,p2)  ! Note sign
  end do
end do

! print "(6f8.4/3(3f8.4)/f10.4)", cosine
if (debug(5)) then
  print "(a,1x,a,1x,5(3f8.4,2x), f10.4)", trim(name(k1)),trim(name(k2)), cosine
end if

!  If order=0 this is all we need
IF (order < 1) RETURN

do i=1,3
  a(i)=sx(i,k1)-sx(i,cm(m1))     !  site vectors wrt GLOBAL AXES
  b(i)=sx(i,k2)-sx(i,cm(m2))     !  relative to molecular origin
  ra(i) = rab(i) + a(i)
  rb(i) = rab(i) - b(i)
end do

!  Calculate cross products needed in first derivatives
rxa(1) = rab(2)*a(3) - rab(3)*a(2)
rxa(2) = rab(3)*a(1) - rab(1)*a(3)
rxa(3) = rab(1)*a(2) - rab(2)*a(1)

rxb(1) = rab(2)*b(3) - rab(3)*b(2)
rxb(2) = rab(3)*b(1) - rab(1)*b(3)
rxb(3) = rab(1)*b(2) - rab(2)*b(1)

do j1=1,3
  raxw1(1,j1) = ra(2)*se(3,j1,p1) - ra(3)*se(2,j1,p1)
  raxw1(2,j1) = ra(3)*se(1,j1,p1) - ra(1)*se(3,j1,p1)
  raxw1(3,j1) = ra(1)*se(2,j1,p1) - ra(2)*se(1,j1,p1)

  rbxw2(1,j1) = rb(2)*se(3,j1,p2) - rb(3)*se(2,j1,p2)
  rbxw2(2,j1) = rb(3)*se(1,j1,p2) - rb(1)*se(3,j1,p2)
  rbxw2(3,j1) = rb(1)*se(2,j1,p2) - rb(2)*se(1,j1,p2)

  w2xa(1,j1)  = se(2,j1,p2)*a(3) - se(3,j1,p2)*a(2)
  w2xa(2,j1)  = se(3,j1,p2)*a(1) - se(1,j1,p2)*a(3)
  w2xa(3,j1)  = se(1,j1,p2)*a(2) - se(2,j1,p2)*a(1)

  w1xb(1,j1)  = se(2,j1,p1)*b(3) - se(3,j1,p1)*b(2)
  w1xb(2,j1)  = se(3,j1,p1)*b(1) - se(1,j1,p1)*b(3)
  w1xb(3,j1)  = se(1,j1,p1)*b(2) - se(2,j1,p1)*b(1)

  do j2=1,3   !  nine cross products for w1xw2
    w1xw2(1,j1,j2)=se(2,j1,p1)*se(3,j2,p2)-se(3,j1,p1)*se(2,j2,p2)
    w1xw2(2,j1,j2)=se(3,j1,p1)*se(1,j2,p2)-se(1,j1,p1)*se(3,j2,p2)
    w1xw2(3,j1,j2)=se(1,j1,p1)*se(2,j2,p2)-se(2,j1,p1)*se(1,j2,p2)
  end do
end do

!  Now compute first derivatives of |R|, w1.R, w2.R and w1.w2. The scalar
!  products involve the vector R, not the unit vector er.
do i=1,3
  !  R
  D1(16,i)=-rab(i)/R
  D1(16,3+i)=rxa(i)/R
  D1(16,6+i)=rab(i)/R
  D1(16,9+i)=-rxb(i)/R
  do j=1,3
    !  w1.R
    D1(j,i)=-se(i,j,p1)
    D1(j,3+i)=-raxw1(i,j)
    D1(j,6+i)=se(i,j,p1)
    D1(j,9+i)=-w1xb(i,j)
    !  w2.R
    D1(3+j,i)=se(i,j,p2)
    D1(3+j,3+i)=-w2xa(i,j)
    D1(3+j,6+i)=-se(i,j,p2)
    D1(3+j,9+i)=rbxw2(i,j)
    do j1=1,3
      !  w1.w2
      D1(3+j1+3*j,i)=0d0
      D1(3+j1+3*j,3+i)=w1xw2(i,j1,j)
      D1(3+j1+3*j,6+i)=0d0
      D1(3+j1+3*j,9+i)=-w1xw2(i,j1,j)
    end do
  end do
end do

!  Now we have to manipulate these to get derivatives of the unit-vector
!  scalar products.
!  Not optimised
!     do j=1,3
!       do i=1,12
!         D1(j,i)=(D1(j,i)-e1r(j)*D1(16,i))/R
!         D1(3+j,i)=(D1(3+j,i)-e2r(j)*D1(16,i))/R
!       end do
!     end do
!  Optimised
do i=1,12
  D1(1,i)=(D1(1,i)-e1r(1)*D1(16,i))/R
  D1(2,i)=(D1(2,i)-e1r(2)*D1(16,i))/R
  D1(3,i)=(D1(3,i)-e1r(3)*D1(16,i))/R
  D1(4,i)=(D1(4,i)-e2r(1)*D1(16,i))/R
  D1(5,i)=(D1(5,i)-e2r(2)*D1(16,i))/R
  D1(6,i)=(D1(6,i)-e2r(3)*D1(16,i))/R
end do
!  End optimised

if (order .lt. 2) return

!  Scalar products occurring in second derivatives
do i=1,3
  aw1(i)=0.d0
  aw2(i)=0.d0
  bw1(i)=0.d0
  bw2(i)=0.d0
  w1ra(i)=0.d0
  w2rb(i)=0.d0
end do

ab=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
raa=ra(1)*a(1)+ra(2)*a(2)+ra(3)*a(3)
rbb=rb(1)*b(1)+rb(2)*b(2)+rb(3)*b(3)
do i=1,3
  do j=1,3
    aw1(i)=aw1(i)+se(j,i,p1)*a(j)
    aw2(i)=aw2(i)+se(j,i,p2)*a(j)
    bw1(i)=bw1(i)+se(j,i,p1)*b(j)
    bw2(i)=bw2(i)+se(j,i,p2)*b(j)
    w1ra(i)=w1ra(i)+se(j,i,p1)*ra(j)
    w2rb(i)=w2rb(i)+se(j,i,p2)*rb(j)
  end do
end do

!  Second derivatives. Clear matrix.
!  Not optimised
!     do i=1,12
!       do j=1,12
!         do iq=1,16
!           d2(iq,i,j)=0d0
!         end do
!       end do
!     end do
!  Optimised This routine needs 8% CPU Time!
do j=1,12
  do i=1,12
    d2(1,i,j)=0d0
    d2(2,i,j)=0d0
    d2(3,i,j)=0d0
    d2(4,i,j)=0d0
    d2(5,i,j)=0d0
    d2(6,i,j)=0d0
    d2(7,i,j)=0d0
    d2(8,i,j)=0d0
    d2(9,i,j)=0d0
    d2(10,i,j)=0d0
    d2(11,i,j)=0d0
    d2(12,i,j)=0d0
    d2(13,i,j)=0d0
    d2(14,i,j)=0d0
    d2(15,i,j)=0d0
    d2(16,i,j)=0d0
  end do
end do
!  End optimised

!  Second derivatives of R.R (factor of 2 omitted from all of these)
do i=1,3
  D2(16,i,i)=1d0
  D2(16,6+i,i)=-1d0
  D2(16,i,6+i)=-1d0
  D2(16,6+i,6+i)=1d0
  do j=1,3
    D2(16,3+i,3+j)=-ra(i)*a(j)
    D2(16,3+i,9+j)=b(i)*a(j)
    D2(16,9+i,3+j)=a(i)*b(j)
    D2(16,9+i,9+j)=rb(i)*b(j)
  end do
  D2(16,3+i,3+i)=D2(16,3+i,3+i)+raa
  D2(16,3+i,9+i)=D2(16,3+i,9+i)-ab
  D2(16,9+i,3+i)=D2(16,9+i,3+i)-ab
  D2(16,9+i,9+i)=D2(16,9+i,9+i)-rbb
  j=i1(i)
  k=i2(i)
  D2(16,j,3+k)=a(i)
  D2(16,k,3+j)=-a(i)
  D2(16,j,9+k)=-b(i)
  D2(16,k,9+j)=b(i)
  D2(16,3+j,k)=-a(i)
  D2(16,3+k,j)=a(i)
  D2(16,3+j,6+k)=a(i)
  D2(16,3+k,6+j)=-a(i)
  D2(16,6+j,3+k)=-a(i)
  D2(16,6+k,3+j)=a(i)
  D2(16,6+j,9+k)=b(i)
  D2(16,6+k,9+j)=-b(i)
  D2(16,9+j,k)=b(i)
  D2(16,9+k,j)=-b(i)
  D2(16,9+j,6+k)=-b(i)
  D2(16,9+k,6+j)=b(i)
end do
!  Construct second derivatives of |R| from these
do i=1,12
  do j=1,12
    D2(16,i,j)=(D2(16,i,j)-D1(16,i)*D1(16,j))/R
  end do
end do

!  Second derivatives of w1.R and w2.R. Again, the scalar
!  products initially involve the vector R, not the unit vector er.
do k=1,3
  do i=1,3
    j1=i1(i)
    j2=i2(i)
    !  w1(k).R
    do j=1,3
      D2(k,3+i,3+j)=ra(i)*se(j,k,p1)
      D2(k,3+i,9+j)=-b(i)*se(j,k,p1)
      D2(k,9+i,3+j)=-se(i,k,p1)*b(j)
      D2(k,9+i,9+j)=se(i,k,p1)*b(j)
    end do
    D2(k,3+i,3+i)=D2(k,3+i,3+i)-w1ra(k)
    D2(k,3+i,9+i)=D2(k,3+i,9+i)+bw1(k)
    D2(k,9+i,3+i)=D2(k,9+i,3+i)+bw1(k)
    D2(k,9+i,9+i)=D2(k,9+i,9+i)-bw1(k)
    D2(k,j1,3+j2)=-se(i,k,p1)
    D2(k,j2,3+j1)=se(i,k,p1)
    D2(k,3+j1,j2)=se(i,k,p1)
    D2(k,3+j2,j1)=-se(i,k,p1)
    D2(k,3+j1,6+j2)=-se(i,k,p1)
    D2(k,3+j2,6+j1)=se(i,k,p1)
    D2(k,6+j1,3+j2)=se(i,k,p1)
    D2(k,6+j2,3+j1)=-se(i,k,p1)
    !  w2(k).R
    do j=1,3
      D2(3+k,3+i,3+j)=se(i,k,p2)*a(j)
      D2(3+k,3+i,9+j)=-se(i,k,p2)*a(j)
      D2(3+k,9+i,3+j)=-a(i)*se(j,k,p2)
      D2(3+k,9+i,9+j)=-rb(i)*se(j,k,p2)
    end do
    D2(3+k,3+i,3+i)=D2(3+k,3+i,3+i)-aw2(k)
    D2(3+k,3+i,9+i)=D2(3+k,3+i,9+i)+aw2(k)
    D2(3+k,9+i,3+i)=D2(3+k,9+i,3+i)+aw2(k)
    D2(3+k,9+i,9+i)=D2(3+k,9+i,9+i)+w2rb(k)
    D2(3+k,j1,9+j2)=se(i,k,p2)
    D2(3+k,j2,9+j1)=-se(i,k,p2)
    D2(3+k,6+j1,9+j2)=-se(i,k,p2)
    D2(3+k,6+j2,9+j1)=se(i,k,p2)
    D2(3+k,9+j1,j2)=-se(i,k,p2)
    D2(3+k,9+j2,j1)=se(i,k,p2)
    D2(3+k,9+j1,6+j2)=se(i,k,p2)
    D2(3+k,9+j2,6+j1)=-se(i,k,p2)
    !  w1(k).w2(m)
    do m=1,3
      do j=1,3
        D2(3+k+3*m,3+i,3+j)=se(j,k,p1)*se(i,m,p2)
        D2(3+k+3*m,3+i,9+j)=-se(j,k,p1)*se(i,m,p2)
        D2(3+k+3*m,9+i,3+j)=-se(i,k,p1)*se(j,m,p2)
        D2(3+k+3*m,9+i,9+j)=se(i,k,p1)*se(j,m,p2)
      end do
      D2(3+k+3*m,3+i,3+i)=D2(3+k+3*m,3+i,3+i)-xx(k,m)
      D2(3+k+3*m,3+i,9+i)=D2(3+k+3*m,3+i,9+i)+xx(k,m)
      D2(3+k+3*m,9+i,3+i)=D2(3+k+3*m,9+i,3+i)+xx(k,m)
      D2(3+k+3*m,9+i,9+i)=D2(3+k+3*m,9+i,9+i)-xx(k,m)
    end do ! m
  end do ! i
end do ! k

!  Convert derivatives of w1.R and w2.R into derivatives of w1.R/R and w2.R/R
!  Not optimised
!     do k=1,3
!       do i=1,12
!         do j=1,12
!           D2(k,i,j)=(D2(k,i,j)-e1r(k)*D2(16,i,j)
!    &                  -D1(16,i)*D1(k,j)-D1(16,j)*D1(k,i))/R
!           D2(3+k,i,j)=(D2(3+k,i,j)-e2r(k)*D2(16,i,j)
!    &                  -D1(16,i)*D1(3+k,j)-D1(16,j)*D1(3+k,i))/R
!         end do ! j
!       end do ! i
!     end do ! k
!  Optimised
do j=1,12
  D2(1,1,j)=(D2(1,1,j)-e1r(1)*D2(16,1,j)                       &
      -D1(16,1)*D1(1,j)-D1(16,j)*D1(1,1))/R
  D2(2,1,j)=(D2(2,1,j)-e1r(2)*D2(16,1,j)                       &
      -D1(16,1)*D1(2,j)-D1(16,j)*D1(2,1))/R
  D2(3,1,j)=(D2(3,1,j)-e1r(3)*D2(16,1,j)                       &
      -D1(16,1)*D1(3,j)-D1(16,j)*D1(3,1))/R
  D2(4,1,j)=(D2(4,1,j)-e2r(1)*D2(16,1,j)                       &
      -D1(16,1)*D1(4,j)-D1(16,j)*D1(4,1))/R
  D2(5,1,j)=(D2(5,1,j)-e2r(2)*D2(16,1,j)                       &
      -D1(16,1)*D1(5,j)-D1(16,j)*D1(5,1))/R
  D2(6,1,j)=(D2(6,1,j)-e2r(3)*D2(16,1,j)                       &
      -D1(16,1)*D1(6,j)-D1(16,j)*D1(6,1))/R

  D2(1,2,j)=(D2(1,2,j)-e1r(1)*D2(16,2,j)                       &
      -D1(16,2)*D1(1,j)-D1(16,j)*D1(1,2))/R
  D2(2,2,j)=(D2(2,2,j)-e1r(2)*D2(16,2,j)                       &
      -D1(16,2)*D1(2,j)-D1(16,j)*D1(2,2))/R
  D2(3,2,j)=(D2(3,2,j)-e1r(3)*D2(16,2,j)                       &
      -D1(16,2)*D1(3,j)-D1(16,j)*D1(3,2))/R
  D2(4,2,j)=(D2(4,2,j)-e2r(1)*D2(16,2,j)                       &
      -D1(16,2)*D1(4,j)-D1(16,j)*D1(4,2))/R
  D2(5,2,j)=(D2(5,2,j)-e2r(2)*D2(16,2,j)                       &
      -D1(16,2)*D1(5,j)-D1(16,j)*D1(5,2))/R
  D2(6,2,j)=(D2(6,2,j)-e2r(3)*D2(16,2,j)                       &
      -D1(16,2)*D1(6,j)-D1(16,j)*D1(6,2))/R

  D2(1,3,j)=(D2(1,3,j)-e1r(1)*D2(16,3,j)                       &
      -D1(16,3)*D1(1,j)-D1(16,j)*D1(1,3))/R
  D2(2,3,j)=(D2(2,3,j)-e1r(2)*D2(16,3,j)                       &
      -D1(16,3)*D1(2,j)-D1(16,j)*D1(2,3))/R
  D2(3,3,j)=(D2(3,3,j)-e1r(3)*D2(16,3,j)                       &
      -D1(16,3)*D1(3,j)-D1(16,j)*D1(3,3))/R
  D2(4,3,j)=(D2(4,3,j)-e2r(1)*D2(16,3,j)                       &
      -D1(16,3)*D1(4,j)-D1(16,j)*D1(4,3))/R
  D2(5,3,j)=(D2(5,3,j)-e2r(2)*D2(16,3,j)                       &
      -D1(16,3)*D1(5,j)-D1(16,j)*D1(5,3))/R
  D2(6,3,j)=(D2(6,3,j)-e2r(3)*D2(16,3,j)                       &
      -D1(16,3)*D1(6,j)-D1(16,j)*D1(6,3))/R

  D2(1,4,j)=(D2(1,4,j)-e1r(1)*D2(16,4,j)                       &
      -D1(16,4)*D1(1,j)-D1(16,j)*D1(1,4))/R
  D2(2,4,j)=(D2(2,4,j)-e1r(2)*D2(16,4,j)                       &
      -D1(16,4)*D1(2,j)-D1(16,j)*D1(2,4))/R
  D2(3,4,j)=(D2(3,4,j)-e1r(3)*D2(16,4,j)                       &
      -D1(16,4)*D1(3,j)-D1(16,j)*D1(3,4))/R
  D2(4,4,j)=(D2(4,4,j)-e2r(1)*D2(16,4,j)                       &
      -D1(16,4)*D1(4,j)-D1(16,j)*D1(4,4))/R
  D2(5,4,j)=(D2(5,4,j)-e2r(2)*D2(16,4,j)                       &
      -D1(16,4)*D1(5,j)-D1(16,j)*D1(5,4))/R
  D2(6,4,j)=(D2(6,4,j)-e2r(3)*D2(16,4,j)                       &
      -D1(16,4)*D1(6,j)-D1(16,j)*D1(6,4))/R

  D2(1,5,j)=(D2(1,5,j)-e1r(1)*D2(16,5,j)                       &
      -D1(16,5)*D1(1,j)-D1(16,j)*D1(1,5))/R
  D2(2,5,j)=(D2(2,5,j)-e1r(2)*D2(16,5,j)                       &
      -D1(16,5)*D1(2,j)-D1(16,j)*D1(2,5))/R
  D2(3,5,j)=(D2(3,5,j)-e1r(3)*D2(16,5,j)                       &
      -D1(16,5)*D1(3,j)-D1(16,j)*D1(3,5))/R
  D2(4,5,j)=(D2(4,5,j)-e2r(1)*D2(16,5,j)                       &
      -D1(16,5)*D1(4,j)-D1(16,j)*D1(4,5))/R
  D2(5,5,j)=(D2(5,5,j)-e2r(2)*D2(16,5,j)                       &
      -D1(16,5)*D1(5,j)-D1(16,j)*D1(5,5))/R
  D2(6,5,j)=(D2(6,5,j)-e2r(3)*D2(16,5,j)                       &
      -D1(16,5)*D1(6,j)-D1(16,j)*D1(6,5))/R

  D2(1,6,j)=(D2(1,6,j)-e1r(1)*D2(16,6,j)                       &
      -D1(16,6)*D1(1,j)-D1(16,j)*D1(1,6))/R
  D2(2,6,j)=(D2(2,6,j)-e1r(2)*D2(16,6,j)                       &
      -D1(16,6)*D1(2,j)-D1(16,j)*D1(2,6))/R
  D2(3,6,j)=(D2(3,6,j)-e1r(3)*D2(16,6,j)                       &
      -D1(16,6)*D1(3,j)-D1(16,j)*D1(3,6))/R
  D2(4,6,j)=(D2(4,6,j)-e2r(1)*D2(16,6,j)                       &
      -D1(16,6)*D1(4,j)-D1(16,j)*D1(4,6))/R
  D2(5,6,j)=(D2(5,6,j)-e2r(2)*D2(16,6,j)                       &
      -D1(16,6)*D1(5,j)-D1(16,j)*D1(5,6))/R
  D2(6,6,j)=(D2(6,6,j)-e2r(3)*D2(16,6,j)                       &
      -D1(16,6)*D1(6,j)-D1(16,j)*D1(6,6))/R

  D2(1,7,j)=(D2(1,7,j)-e1r(1)*D2(16,7,j)                       &
      -D1(16,7)*D1(1,j)-D1(16,j)*D1(1,7))/R
  D2(2,7,j)=(D2(2,7,j)-e1r(2)*D2(16,7,j)                       &
      -D1(16,7)*D1(2,j)-D1(16,j)*D1(2,7))/R
  D2(3,7,j)=(D2(3,7,j)-e1r(3)*D2(16,7,j)                       &
      -D1(16,7)*D1(3,j)-D1(16,j)*D1(3,7))/R
  D2(4,7,j)=(D2(4,7,j)-e2r(1)*D2(16,7,j)                       &
      -D1(16,7)*D1(4,j)-D1(16,j)*D1(4,7))/R
  D2(5,7,j)=(D2(5,7,j)-e2r(2)*D2(16,7,j)                       &
      -D1(16,7)*D1(5,j)-D1(16,j)*D1(5,7))/R
  D2(6,7,j)=(D2(6,7,j)-e2r(3)*D2(16,7,j)                       &
      -D1(16,7)*D1(6,j)-D1(16,j)*D1(6,7))/R

  D2(1,8,j)=(D2(1,8,j)-e1r(1)*D2(16,8,j)                       &
      -D1(16,8)*D1(1,j)-D1(16,j)*D1(1,8))/R
  D2(2,8,j)=(D2(2,8,j)-e1r(2)*D2(16,8,j)                       &
      -D1(16,8)*D1(2,j)-D1(16,j)*D1(2,8))/R
  D2(3,8,j)=(D2(3,8,j)-e1r(3)*D2(16,8,j)                       &
      -D1(16,8)*D1(3,j)-D1(16,j)*D1(3,8))/R
  D2(4,8,j)=(D2(4,8,j)-e2r(1)*D2(16,8,j)                       &
      -D1(16,8)*D1(4,j)-D1(16,j)*D1(4,8))/R
  D2(5,8,j)=(D2(5,8,j)-e2r(2)*D2(16,8,j)                       &
      -D1(16,8)*D1(5,j)-D1(16,j)*D1(5,8))/R
  D2(6,8,j)=(D2(6,8,j)-e2r(3)*D2(16,8,j)                       &
      -D1(16,8)*D1(6,j)-D1(16,j)*D1(6,8))/R

  D2(1,9,j)=(D2(1,9,j)-e1r(1)*D2(16,9,j)                       &
      -D1(16,9)*D1(1,j)-D1(16,j)*D1(1,9))/R
  D2(2,9,j)=(D2(2,9,j)-e1r(2)*D2(16,9,j)                       &
      -D1(16,9)*D1(2,j)-D1(16,j)*D1(2,9))/R
  D2(3,9,j)=(D2(3,9,j)-e1r(3)*D2(16,9,j)                       &
      -D1(16,9)*D1(3,j)-D1(16,j)*D1(3,9))/R
  D2(4,9,j)=(D2(4,9,j)-e2r(1)*D2(16,9,j)                       &
      -D1(16,9)*D1(4,j)-D1(16,j)*D1(4,9))/R
  D2(5,9,j)=(D2(5,9,j)-e2r(2)*D2(16,9,j)                       &
      -D1(16,9)*D1(5,j)-D1(16,j)*D1(5,9))/R
  D2(6,9,j)=(D2(6,9,j)-e2r(3)*D2(16,9,j)                       &
      -D1(16,9)*D1(6,j)-D1(16,j)*D1(6,9))/R

  D2(1,10,j)=(D2(1,10,j)-e1r(1)*D2(16,10,j)                    &
      -D1(16,10)*D1(1,j)-D1(16,j)*D1(1,10))/R
  D2(2,10,j)=(D2(2,10,j)-e1r(2)*D2(16,10,j)                    &
      -D1(16,10)*D1(2,j)-D1(16,j)*D1(2,10))/R
  D2(3,10,j)=(D2(3,10,j)-e1r(3)*D2(16,10,j)                    &
      -D1(16,10)*D1(3,j)-D1(16,j)*D1(3,10))/R
  D2(4,10,j)=(D2(4,10,j)-e2r(1)*D2(16,10,j)                    &
      -D1(16,10)*D1(4,j)-D1(16,j)*D1(4,10))/R
  D2(5,10,j)=(D2(5,10,j)-e2r(2)*D2(16,10,j)                    &
      -D1(16,10)*D1(5,j)-D1(16,j)*D1(5,10))/R
  D2(6,10,j)=(D2(6,10,j)-e2r(3)*D2(16,10,j)                    &
      -D1(16,10)*D1(6,j)-D1(16,j)*D1(6,10))/R

  D2(1,11,j)=(D2(1,11,j)-e1r(1)*D2(16,11,j)                    &
      -D1(16,11)*D1(1,j)-D1(16,j)*D1(1,11))/R
  D2(2,11,j)=(D2(2,11,j)-e1r(2)*D2(16,11,j)                    &
      -D1(16,11)*D1(2,j)-D1(16,j)*D1(2,11))/R
  D2(3,11,j)=(D2(3,11,j)-e1r(3)*D2(16,11,j)                    &
      -D1(16,11)*D1(3,j)-D1(16,j)*D1(3,11))/R
  D2(4,11,j)=(D2(4,11,j)-e2r(1)*D2(16,11,j)                    &
      -D1(16,11)*D1(4,j)-D1(16,j)*D1(4,11))/R
  D2(5,11,j)=(D2(5,11,j)-e2r(2)*D2(16,11,j)                    &
      -D1(16,11)*D1(5,j)-D1(16,j)*D1(5,11))/R
  D2(6,11,j)=(D2(6,11,j)-e2r(3)*D2(16,11,j)                    &
      -D1(16,11)*D1(6,j)-D1(16,j)*D1(6,11))/R

  D2(1,12,j)=(D2(1,12,j)-e1r(1)*D2(16,12,j)                    &
      -D1(16,12)*D1(1,j)-D1(16,j)*D1(1,12))/R
  D2(2,12,j)=(D2(2,12,j)-e1r(2)*D2(16,12,j)                    &
      -D1(16,12)*D1(2,j)-D1(16,j)*D1(2,12))/R
  D2(3,12,j)=(D2(3,12,j)-e1r(3)*D2(16,12,j)                    &
      -D1(16,12)*D1(3,j)-D1(16,j)*D1(3,12))/R
  D2(4,12,j)=(D2(4,12,j)-e2r(1)*D2(16,12,j)                    &
      -D1(16,12)*D1(4,j)-D1(16,j)*D1(4,12))/R
  D2(5,12,j)=(D2(5,12,j)-e2r(2)*D2(16,12,j)                    &
      -D1(16,12)*D1(5,j)-D1(16,j)*D1(5,12))/R
  D2(6,12,j)=(D2(6,12,j)-e2r(3)*D2(16,12,j)                    &
      -D1(16,12)*D1(6,j)-D1(16,j)*D1(6,12))/R
end do ! j

!      do j=1,12
!        d11j=D1(1,j)
!        d12j=D1(2,j)
!        d13j=D1(3,j)
!        d14j=D1(4,j)
!        d15j=D1(5,j)
!        d16j=D1(6,j)
!        d116j=D1(16,j)
!        do i=1,12
!            d116i=D1(16,i)
!            d216ij=D2(16,i,j)
!            D2(1,i,j)=(D2(1,i,j)-e1r(1)*d216ij
!     &                  -d116i*d11j-d116j*D1(1,i))/R
!            D2(2,i,j)=(D2(2,i,j)-e1r(2)*d216ij
!     &                  -d116i*d12j-d116j*D1(2,i))/R
!            D2(3,i,j)=(D2(3,i,j)-e1r(3)*d216ij
!     &                  -d116i*d13j-d116j*D1(3,i))/R
!            D2(4,i,j)=(D2(4,i,j)-e2r(1)*d216ij
!     &                  -d116i*d14j-d116j*D1(4,i))/R
!            D2(5,i,j)=(D2(5,i,j)-e2r(2)*d216ij
!     &                  -d116i*d15j-d116j*D1(5,i))/R
!            D2(6,i,j)=(D2(6,i,j)-e2r(3)*d216ij
!     &                  -d116i*d16j-d116j*D1(6,i))/R
!        end do ! i
!      end do ! j
!  End optimised

END SUBROUTINE pairinfo_tq

END MODULE interact
