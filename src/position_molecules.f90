SUBROUTINE position_molecules

!  Read the position and orientation of one or more molecules
!  [POSITION[S] already read]
!    molecule AT xspec yspec zspec +++
!                 ROTAT{ED|ION} { pname | BY psi ABOUT nx ny nz }
!    molecule ...
!  END
!  where
!  molecule is the name of a molecule
!  xspec, yspec and zspec specify the position coordinates, each in the
!  form of a constant, or in terms of variables:
!  [const] [+/-] vname [ * factor ] [ +/- vname' [ * factor' ] ...
!  where vname is the name of a variable and factor is an optional
!  constant numerical factor multiplying the value of the variable.
!  pname is the name of a set of angle-axis rotation variables.
!  If any position variable or rotation is to be kept constant, provide
!  constant values for x, y, z, or psi, nx, ny and nz as required. 

!  The array element vmap(i,j) is the coefficient of variable j in the
!  expression of coordinate i as a linear combination of the variables.
!  "Variable" 0 is 1, so its coefficient is a constant to be added to
!  this expression. Coordinates base+{1,2,3}, where base=6*(m-1), are
!  the position coordinates of molecule m, and coordinates base+{4,5,6}
!  are the angle-axis rotation coordinates.

USE consts, ONLY: rfact
USE input, ONLY: read_line, item, nitems, reread, readf, reada, upcase, die
USE options, ONLY: map_variables
USE molecules, ONLY: mols, mol
USE rotations, ONLY: read_rotation
USE sites, ONLY: locate
USE variables, ONLY: variable, vlist, vmap, nv

IMPLICIT NONE

TYPE(variable), POINTER :: v=>null()
DOUBLE PRECISION :: p(3), s
INTEGER :: base, i, j, m
CHARACTER(LEN=20) :: ww
CHARACTER(LEN=2) :: coord(6)=["x ","y ","z ","px","py","pz"]
LOGICAL :: constant, eof

map_variables=.true.
if (allocated(vmap)) deallocate(vmap)
allocate(vmap(6*mols,0:nv))
do
  call read_line(eof)
  if (eof) call die("Unexpected end of file",.true.)
  call reada(ww)
  if (upcase(ww) .eq. "END") exit
  m=locate(ww)
  ! print "(2a)", "molecule ", trim(ww)
  do while (item<nitems)
    call reada(ww)
    ! print "(a)", ww
    select case(upcase(ww))
    case("AT")
      ! mol(m)%x=0d0
      base=6*(m-1)
      vmap(base+1:base+6,0:nv)=0d0
      xyz: do i=1,3
        s=1d0
        expr: do
          call reada(ww)
          print "(a)", ww
          constant=.true.
          v=>vlist
          do while (associated(v))
            if (v%name .eq. ww) then
              constant=.false.
              if (v%type .ne. 1) call die ("Variable is not a length",.true.)
              vmap(base+i,v%index)=s
              exit
            end if
            v=>v%next
          end do
          if (constant) then
            call reread(-1)
            call readf(vmap(base+i,0),s*rfact)
            ! print "(f8.4)", vmap(base+i,0)
          end if
          !  Possible factor to multiply the previous variable for the
          !  current coordinate, or another term
          do while (item < nitems)
            call reada(ww)
            select case(ww)
            case ("*")
              if (constant) call die                                      &
                  ("Can't specify a multiplying factor for a constant",.true.)
              call readf(vmap(base+i,v%index),s)
            case("+")
              s=1d0
              cycle expr
            case("-")
              s=-1d0
              cycle expr
            case default
              !  No more of this expression
              call reread(-1)
              exit expr
            end select
          end do
        end do expr
      end do xyz
    case("ROTATED","ROTATION")
      call reada(ww)
      ! print "(a)", ww
      constant=.true.
      v=>vlist
      do while (associated(v))
        if (v%name .eq. ww) then
          constant=.false.
          if (v%type .ne. 3) call die ("Variable is not a rotation",.true.)
          do i=1,3
            vmap(base+3+i,v%index+i-1)=1d0
          end do
          exit
        end if
        v=>v%next
      end do
      if (constant) then
        call reread(-1)
        call read_rotation(p)
        vmap(base+4:base+6,0)=p
        ! print "(3f8.4)", p
      end if
    end select
  end do
  print "(3a)", "Molecule ", trim(mol(m)%name), ":"
  do i=1,6
    print "(a,1x,8f8.4/(3x,8f8.4))", coord(i), vmap(base+i,0:nv)
  end do
end do

do j=1,nv
  if (all(vmap(:,j) == 0d0)) then
    print "(a,i0,a)", "Variable ", j, " has not been used."
    call die("Error in molecule position specification",.true.)
  end if
end do

! print "(a/6f13.7)", "Variables:", x0(1:nv)

END SUBROUTINE position_molecules

