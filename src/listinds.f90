SUBROUTINE listinds(k1,k2,nstart)

!  Find the S functions that are needed for the interaction between
!  induced moments on sites k1 and k2, and list them in numerical order.
!  We only get to this routine if k1 and k2 are both polarizable.

USE indparams
USE consts, ONLY : eslimit
USE induction
USE input, ONLY : die
USE labels
USE molecules
USE sites
USE sizes, ONLY: lq
USE switches
USE types
USE interact
USE binomials
USE Sfns, ONLY : t1s, t2s, js, offset

IMPLICIT NONE


INTEGER, INTENT(IN) :: k1, k2, nstart
INTEGER ix, t1, t2, u1,u2, n, maxjes, pz1, pz2
LOGICAL needed

if (debug(10))                                                     &
    print '(/A,2I3/2A)', 'Sites', k1, k2,                          &
    '    S function       Q1          Q2       binom'

t1 = type(k1)
t2 = type(k2)
if (t1 .gt. 0 .and. t2 .gt. 0) then
  pair=pairindex(t1,t2)
endif
pz1 = pz(k1)
pz2 = pz(k2)
maxjes = min(lq(pz1)+lq(pz2),eslimit-1)
! maxjes = min(pr(k1)+pr(k2),eslimit-1)

!  list(n) is the index number of the S function needed at position n.

!  Identify electrostatic terms for interactions involving induced moments.
n=nstart
ix=1
!  Search electrostatic terms only, and limit electrostatics to
!  R^(-eslimit) terms.
do while (js(ix) .le. maxjes .and. ix .le. offset(1))
  u1=t1s(ix)
  u2=t2s(ix)
  needed=(u1 .le. pz1 .and. u2 .le. pz2)
  if (needed) then
    list(n)=ix
    fac(n)=binom(js(ix),lq(u1))
    q1(n)=Q(u1,k1)
    q2(n)=Q(u2,k2)
    power(n)=js(ix)+1
    if (debug(10)) print '(I3,2X,2A,I2,2F12.6,F7.1,3F11.4)',       &
        ix, label(u1), label(u2), js(ix),                          &
        q1(n), q2(n), fac(n)
    n=n+1
    if (n .gt. PAIRMAX) call die                                &
        ("Not enough space for interaction functions:"//           &
        " increase number of S functions",.false.)
  endif
  ix=ix+1
end do

!  Mark end of list
list(n)=0

return
END SUBROUTINE listinds
