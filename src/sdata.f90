MODULE sdata

!  Not used. Now included in Sfns.f90.

!  S function information
!  MAXSFN is the maximum number of S functions listed altogether. Each
!  S function has an index in the range 1--MAXSFN.
!  SMAP finds the S function S(t,u,j), where t and u are the l\kappa
!  indices for the two sites. If t=l\kappa and u=l'\kappa', then the
!  index of S(t,u,j) is in SMAP(t,u,l+l'-j). (For the electrostatic S
!  functions, l+l'-j=0.) The first S function with a particular value
!  of l+l'-j has the index offset(l+l'-j)+1. maxixes(j) is the index of
!  the last electrostatic function with that value of j.

USE parameters

PARAMETER :: maxsfn=4000
INTEGER :: smap(sq,sq,0:10), offset(0:12), maxixes(-1:5),          &
    t1s(maxsfn), t2s(maxsfn), js(maxsfn)

END MODULE sdata
