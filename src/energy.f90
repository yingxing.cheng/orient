SUBROUTINE energy(es,er,eind,edisp,etotal)

!  This routine is suitable for use in a program that calls Orient to
!  calculate energies. The calling program should call Orient (no
!  arguments) to read the Orient data from the standard
!  input. Subsequent calls to this routine will return the energy of the
!  system. The variables defining the geometry may be changed as
!  required; they are in the array VALUE in module variables, in the order
!  in which they were defined in the "Variables" command in the data.
!  (Note however that the "Variables" section must precede any molecule
!  definitions for this to be true.) A "show variables" command in the
!  Orient data file will print out the variable names, their values, and
!  their positions in the VALUE array. Alternatively the VNAME array in the
!  variables module may be search for the variable name, which is in the
!  position in that array corresponding to the position of the value in
!  the VALUE array.

USE force
USE molecules
USE variables
USE induction, ONLY: maxind

IMPLICIT NONE

DOUBLE PRECISION :: es, er, eind, edisp, etotal
INTEGER :: i, m

do m=1,mols
  call axes(m,.false.)
end do

call forces(0,maxind,es,er,eind,edisp,etotal)

END SUBROUTINE energy
