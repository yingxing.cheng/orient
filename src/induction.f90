MODULE induction

USE indparams

!  Data for polarizable sites. We need a separate list of the polarizable
!  sites, to carry the fields and induced moments and their derivatives.

!  They are crossindexed to the main site list by arrays PS (in /SITES/),
!  with PS(K) pointing to the polarizable site carrying the fields for
!  site K, and PP, where PP(PK) is the main site associated with
!  polarizable site PK.

!  PSITES is the maximum number of polarizable sites
!  NPOL is the actual number of polarizable sites.

!  PR(K) is the rank of polarizabilities (and hence induced moments)
!  on site K. (CHANGED from version 3.1.2, where it was the
!  number of induced moments, i.e., PZ(K) = QSIZE(PR(K)) as now defined.)

!  PS(k) points to the entries in the DQn and Vn arrays holding the
!  induced moments and fields at site k.

!  DQ0(q,ps(k),n) is the qth induced moment for polarizable site k.
!  DQ1(i,m,q,ps(k),n) is its first derivative with respect to global
!  translation-rotation coordinate i of molecule m, and DQ2(i,j,m1,m2,q,ps(k),n)
!  is the second derivative. n=1 is the current iteration, n=0 the previous
!  one. n=-1 gives the induced moments from the first iteration (no
!  derivatives).

!  V0(q,k) is field q at site k, and V1 and V2 are the first and second
!  derivatives with respect to molecular positions and orientations.

!  IXA(K1,K2) is the index of the polarizability matrix for polarizable
!  sites K1 and K2; i.e. the matrix for sites K1 and K2 is at index
!  IXA(K1,K2)). If this is zero there is no polarizability for them.

!  FIRSTP(M) (in module MOLECULES) is the first polarizable site (in
!  linked-list order) for molecule M. It is zero if M has no polarizable sites.
!  NEXTP(PK) is the next polarizable site for the same molecule in
!  linked-list order; zero for the last polarizable site.

!  maxind is the maximum number of induction iterations to be carried out.
!  It can be over-ridden in the call to forces. Zero means no limit.

DOUBLE PRECISION, ALLOCATABLE, SAVE :: dq0(:,:,:)
DOUBLE PRECISION, ALLOCATABLE, SAVE :: dq1(:,:,:,:,:)
DOUBLE PRECISION, ALLOCATABLE, SAVE :: dq2(:,:,:,:,:,:,:)
DOUBLE PRECISION, ALLOCATABLE, SAVE :: v0(:,:)
DOUBLE PRECISION, ALLOCATABLE, SAVE :: v1(:,:,:,:)
DOUBLE PRECISION, ALLOCATABLE, SAVE :: v2(:,:,:,:,:,:)

INTEGER :: first=-1, old, new, maxind=50
DOUBLE PRECISION :: induction_cvg=1D-24 

CONTAINS

SUBROUTINE alloc_vdq(mols,psize,npol,order)

USE input, ONLY : die
IMPLICIT NONE
!  mols   Number of molecules
!  psize  Dimension of polarizability matrices
!  npol   Total number of polarizable sites
!  order  Maximum order of derivatives to allocate
INTEGER, INTENT(IN) :: mols,psize,npol,order
INTEGER :: ok

if (allocated(dq0)) then
  if (psize > size(dq0,1) .or. npol > size(dq0,2)) then
    deallocate(dq0, v0)
  endif
endif
if (.not. allocated(dq0)) then
  ! print "(a, 4i4)", "Allocating fields: ", order, mols, psize, npol
  allocate(dq0(psize,npol,-1:1), v0(psize,npol), stat=ok)
  if (ok > 0) call die                                       &
      ("Couldn't allocate dq0 or v0",.false.)
endif
v0=0d0

if (order .ge. 1) then
  if (allocated(dq1)) then
    if (mols > size(dq1,2) .or. psize > size(dq1,3)        &
        .or. npol > size(dq1,4)) then
      deallocate(dq1, v1)
    endif
  endif
  if (.not. allocated(dq1)) then
    allocate(dq1(6,mols,psize,npol,-1:1), v1(6,mols,psize,npol), stat=ok)
    if (ok > 0) call die                                     &
        ("Couldn't allocate dq1 or v1",.false.)
  endif
  v1=0d0
endif

if (order .ge. 2) then
  if (allocated(dq2)) then
    if (mols > size(dq2,3) .or. psize > size(dq2,5)        &
        .or. npol > size(dq2,6)) then
      deallocate(dq2, v2)
    endif
  endif
  if (.not. allocated(dq2)) then
    allocate(dq2(6,6,mols,mols,psize,npol,-1:1),                   &
        v2(6,6,mols,mols,psize,npol), stat=ok)
    if (ok > 0) call die                                     &
        ("Couldn't allocate dq2 or v2",.false.)
  endif
  v2=0d0
endif

END SUBROUTINE alloc_vdq

END MODULE induction
