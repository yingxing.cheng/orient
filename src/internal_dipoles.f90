SUBROUTINE internal_dipoles(m)

!  Find a description of the specified molecule in which the site
!  dipoles are replaced by a set of reduced dipoles together with
!  local polarizabilities, so that internal polarization recovers
!  the original dipoles.

!  The calculation is carried out on the assumption that all moments
!  are referred to global axes.

USE molecules, ONLY: head
USE sites, ONLY: L, next, sx, name
USE alphas
USE indexes

IMPLICIT NONE

INTEGER, INTENT(IN) :: m

TYPE(molecule) :: mol

CHARACTER, LEN=*, PARAMETER :: this_routine = "internal_dipoles"
DOUBLE PRECISION, ALLOCATABLE :: AT(:,:)
DOUBLE PRECISION :: alpha_a(0:3,0:3), T_ab(0:3,0:3), R_ab(3), R
INTEGER :: info, ix, k, pk, n, pa, pb, np
INTEGER, ALLOCATABLE, ipiv(:)

np = 0
k = head(m)
n = 0
!  Find dimension of problem
do while (k > 0)
  if (l(k) < 0) cycle  !  No moments on this site
  !  We assume that otherwise sites carry charge and dipole only
  !  and that polarizabilities are local
  n = n+4
  k = next(k)
end do
allocate (AT(n,n), qq(n), qu(n), ipiv(n), stat=info)
if (info > 0) call die("Can't allocate arrays in "//this_routine)
AT = 0d0
pa = 0
ka = head(m)
do while (ka > 0)
  if (l(ka) < 0) cycle  !  No moments on this site
  qq(pa+1:pa+4) = Q(1:4,k)
  do p=1,4
    AT(pa+p,pa+p) = 1d0
  end do
  if (ps(ka) > 0) then
    ix=lookup(indexa,ka,ka)
    alpha_a = alpha(0:3,0:3,ix) 
    xa = sx(:,ka)
    pb = 0
    kb = head(m)
    do while (kb > 0)
      if (l(ka) < 0) cycle
      if (kb .ne. ka) then
        xb = sx(:,kb)
        !  Note order of components
        R_ab = [xb(3)-xa(3), xb(1)-xa(1), xb(2)-xa(2)]
        R = sqrt(R_ab(1))**2 + Rab(2)**2 + Rab(3)**2)
        T_ab(0,0) = 1d0/R
        T_ab(0,1:3) = - R_ab(:)/R**2
        T_ab(1:3,0) = R_ab(:)/R**2
        do i = 1,3
          do j = 1,3
            T_ab(i,j) = 3d0 * R_ab(i) * R_ab(j)
          end do
          T_ab(i,i) = T_ab(i,i) - R**2
        end do
        T_ab(1:3,1:3) = dampfn(R) * T_ab(1:3,1:3) / R**5
        AT(pa+1:pa+4,pb+1:pb+4) = matmul(alpha_a,T_ab)
      end if
      pb = pb+4
      kb = next(kb)
    end do
  end if
  pa = pa+4
  ka = next(ka)
end do
if (pa .ne. n) call die("Something's wrong in "//this_routine)

!  Now solve qq(:) = AT(:,:) qu(:) for the 'unpolarized' moments qu.
qu = qq
call dgesv(n,1,TA,n,ipiv,qu,n,info)
select case(info)
case(0)
  print "(a)", "Equations solved successfully"
case(-8:-1):
  print "(a,i0)", "Argument ", -info, " DSEGV was invalid"
  call die("Stopping")
case(1:)
  call die("Singular matrix")
end select

ka = head(m)
pa = 0
do while (ka > 0)
  if (l(ka) < 0) cycle
  print "()", name(ka), qq(pa+1:pa+4)
  print "()", qu(pa+1:pa+4)
  pa = pa+4
  ka = next(ka)
end do


CONTAINS

DOUBLE PRECISION FUNCTION dampfn(R)

!  Dummy at present

DOUBLE PRECISION, INTENT(IN) :: R

if (R > 2.0) then
  dampfn = 1d0
else
  dampfn = 0d0
end if

END FUNCTION dampfn

END SUBROUTINE internal_dipoles
