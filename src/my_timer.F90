SUBROUTINE my_timer(operation,title,debug)
!Based on Wojtek's timing routine.
!operation : One of 
!           'enter' : entering routine/function
!           'exit'  : exiting
!           'report': write it all out nicely
!title     : title of the routine/function you are timing.
!debug     : (OPTIONAL) LOGICAL
!            If present and TRUE, a single line stating the name of the
!            subroutine entered is printed.
!
! USE GLOBAL_DATA, ONLY : G_TIMER_DEBUG
! use precision
IMPLICIT NONE
SAVE
INTEGER, PARAMETER :: sp = kind(1.0)
INTEGER, PARAMETER :: dp = kind(1d0)
CHARACTER(LEN=*), INTENT(IN) :: operation
CHARACTER(LEN=*), INTENT(IN) :: title
LOGICAL, INTENT(IN), OPTIONAL :: debug
!------------
LOGICAL :: iam_init = .FALSE.  !Initial value. Lost after updating
INTEGER :: current_len = 0     !---do---
LOGICAL :: g_timer_debug=.false.
INTEGER :: posn
INTEGER, PARAMETER :: maxlen = 200
CHARACTER(LEN=80), DIMENSION(maxlen) :: titles
INTEGER, DIMENSION(maxlen) :: num_calls
REAL(dp), DIMENSION(maxlen) :: entry_time, accumulated_time
REAL(dp) :: current_time
CHARACTER(LEN=*), PARAMETER :: fmt1='(1x,''Subroutine'',20x,&
& ''Number of Calls'',3x,''Time (minutes)'')'
CHARACTER(LEN=*), PARAMETER :: fmt2='(1x,A30,2x,i10,10x,f12.3)'
!------------
if (present(debug)) then
  if (debug.and.g_timer_debug) print *,'Subroutine ',trim(title),&
    & '  ',trim(operation)
endif
!
if (.not.iam_init) then
  titles = ''
  posn = 0  !Current position in arrays
  !Now update the value of IAM_INIT. Contrary to expectation, the initial
  !value defined in the declaration statement above, will be over-written.
  iam_init = .true.
endif
!
select case(operation)
  case('enter')
    posn = find_title(title) 
    if (posn.eq.-1) then
      !this is a new entry in the table
      current_len = current_len + 1
      if (current_len.GT.maxlen) then
        call error(1)
        !Set current length back one place
        current_len = current_len - 1
        return
      endif
      posn = current_len
      titles(posn) = title
      num_calls(posn) = 0
      accumulated_time(posn) = 0.0_dp
    endif
    num_calls(posn) = num_calls(posn) + 1
    entry_time(posn) = my_cpu_time()
    !
  case('exit')
    posn = find_title(title)
    if (posn.eq.-1) then
      call error(2)
      return
    endif
    current_time = my_cpu_time()
    accumulated_time(posn) = accumulated_time(posn) + &
                            & ( current_time - entry_time(posn) )
    !
  case('report')
    write(*,*)
    write(*,*)' Timing Report '
    write(*,*)'==============='
    write(*,fmt1)
    do posn = 1, current_len
       write(*,fmt2)titles(posn),num_calls(posn),accumulated_time(posn)/60.0
    enddo
    write(*,*)'===================================='
    write(*,*)
    !
  case default
    write(*,*)'MY_TIMER: Wrong operation command given'
    write(*,*)'Received: operation=',operation,' and title =',title
end select
!
return
contains
  function find_title(title)
  !Looks for TITLE in the list of TITLES(). If present, the position of TITLE
  !in the list is returned, else -1 is returned.
  implicit none
  CHARACTER(LEN=*), INTENT(IN) :: title
  !------------
  INTEGER :: find_title
  INTEGER :: found, pos
  !------------
  found = -1
  loop: do pos = 1, current_len
    if (title.eq.titles(pos)) then
      found = pos
      exit loop
    endif
  enddo loop
  !
  find_title = found
  !
  return
  end function find_title

  subroutine error(indx)
  implicit none
  integer indx
  !------------
  if (indx.eq.1) then
    write(*,*)'WARNING: In Timing routine MY_TIMER: MAXLEN exceeded.'
    write(*,*)'Skipping timing routine ',title
  elseif (indx.eq.2) THEN
    write(*,*)'WARNING: In Timing routine MY_TIMER: Cannot find TITLE'
    write(*,*)'Missing title is =',title
  else
    write(*,*)'WARNING: Wrong INDX value passed to SUBROUTINE ERROR'
  endif
  return
  end subroutine error

  function my_cpu_time()
  implicit none
  REAL(dp) :: my_cpu_time
  REAL(sp), DIMENSION(2) :: tarray = (/ 0.0_sp, 0.0_sp /)
  REAL(sp) :: etime = 0.0_sp
  REAL(dp) :: cputime = 0.0_dp
  !------------
#if defined(G77)
  my_cpu_time = second()
#endif
#if defined(DECALPHA) || defined(SUN) || defined(SUNF90)
  my_cpu_time = etime(tarray)
#endif
#if (SGI) || (RS6K) || (IBM32) || (IBM64)
  my_cpu_time = float(mclock())*1.0E-2
#endif
#if (IFC) || (IFORT) || (LAHEY) || (PGF90) || (G95)
  call cpu_time(cputime)
  my_cpu_time = cputime
#endif
    !
  return
  end function my_cpu_time

end subroutine my_timer

