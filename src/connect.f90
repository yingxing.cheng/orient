MODULE connect

USE consts, ONLY : bohrva
USE indexes
USE types, ONLY : covalent_radius, atnum
USE sites, ONLY : name, next, sx, type
USE molecules, ONLY : head
USE input
IMPLICIT NONE

PRIVATE
PUBLIC :: bonds, indexb, bond_matrix
LOGICAL :: auto_bonds=.false., list_bonds=.false.
INTEGER :: nb
DOUBLE PRECISION :: factor=1.2d0
TYPE(index), SAVE :: indexb

CONTAINS

SUBROUTINE bonds(mol)

INTEGER, INTENT(IN) :: mol
CHARACTER(LEN=16) :: ww

call axes(mol,.false.)
! nb=1
auto_bonds=.false.
list_bonds=.false.
do while (item < nitems)
  call readu(ww)
  select case(ww)
  case("RANGE")
    call readi(nb)
    print "(a)", "The RANGE option is obsolete."
  case("AUTO")
    auto_bonds=.true.
  case("LIST")
    list_bonds=.true.
  case("FACTOR")
    !  This factor multiplies the sum of covalent bonding radii used
    !  to test for the presence of a bond. Default 1.2.
    call readf(factor)
  case default
    call reread(-1)
    exit
  end select
end do
call index_bonds(mol)

END SUBROUTINE bonds

SUBROUTINE index_bonds(mol)

INTEGER, INTENT(IN) :: mol
INTEGER :: p, q, j, k, match
CHARACTER(LEN=16) :: ww

if (auto_bonds) then
  k=head(mol)
  do while (k>0)
    if (type(k)>0) then
      j=next(k)
      do while (j>0)
        if (type(j)>0 .and. length(sx(:,k)-sx(:,j))                    &
            < factor*(covalent_radius(atnum(type(k)))+covalent_radius(atnum(type(j)))) &
            /(100d0*bohrvA)) then
          call insert(indexb,k,j,1,"bond index")
          if (list_bonds) print "(3a)", trim(name(k)), " to ", trim(name(j))
        end if
        j=next(j)
      end do
    else if (k .ne. head(mol)) then
      print "(a,i0,3a)", "Auto-bonds warning: no type specified for site ", k,     &
          " (name ", trim(name(k)), ")"
    end if
    k=next(k)
  end do
else
  !  Construct a bond matrix from shorthand input. E.g. two calls with
  !  the input buffer containing first
  !  C1 -> H11 \& H12 \& H13 \& C2 -> H21 \& H22 \& C3
  !  and then
  !  C3 -> H31 \& H32 \& H33
  !  would construct the bonds for propane.
  match=0
  do while (item < nitems)
    p=0
    sublist: do while (item < nitems)
      call reada(ww)
      ! write (6,"(1x,a)",advance="no") trim(ww)
      select case(ww)
      case(";")
        p=0
        exit sublist
      case("&")
        q=p
        cycle sublist
      case("->")
        if (match>1) call die("Ambiguous bond specification",.true.)
        p=q
        cycle sublist
      case default
        if (match>1) call die("Ambiguous bond specification",.true.)
        p=q
      end select
      match=0
      k=head(mol)
      do while (k .ne. 0)
        if (name(k) .eq. ww) then
          match=match+1
          q=k
          if (p .ne. 0) then
            call insert(indexb,p,q,1,"bond index")
            if (list_bonds) print "(3a)", trim(name(p)), " to ", trim(name(q))
          end if
        endif
        k=next(k)
      end do
    end do sublist
    ! print "(1x)"
  end do
end if

!  Not used anywhere
! if (nb>1) call links(mol,nb)

END SUBROUTINE index_bonds

!------------------------------------------------------------------------

SUBROUTINE links(mol,limit)

!  Constructs a sparse matrix for molecule MOL in which the entry for
!  row i, column j is the least number of bonds between i and j,
!  provided that value is not greater than LIMIT. If the number of bonds
!  exceeds the limit, no entry is generated.

USE input
USE output
USE sites, ONLY : next
USE molecules, ONLY : head
IMPLICIT NONE

INTEGER, INTENT(IN) :: mol, limit
INTEGER :: iter, i, j, k, n, m1, m2
LOGICAL :: modify, test=.false.

iter=0
modify=.true.
do while (modify)
  if (test) then
    call putout(1)
    call puttab(6)
    i=head(mol)
    do while (i>0)
      call puti(i,"(i3)",.false.)
      i=next(i)
    end do
    call putout(1)
    i=head(mol)
    do while (i>0)
      call puti(i,"(i3)",.false.)
      call putsp(2)
      j=head(mol)
      do while (j>0)
        n=lookup(indexb,i,j)
        if (n .eq. 0) then
          call putsp(3)
        else
          call puti(n,"(i3)",.false.)
        endif
        j=next(j)
      end do
      call putout(0)
      i=next(i)
    end do
  end if

  modify=.false.
  if (iter .gt. 10) call die ("Too many iterations in CONNECT",.true.)
  i=head(mol)
  do while (i>0)
    j=0
    do
      call entry(indexb,i,j,m1)
      if (j == 0) exit

      !  I --> J. Look for K such that I --> J --> K
      k=0
      do
        !  Find next K such that J --> K
        call entry(indexb,j,k,m2)
        if (k == 0) exit
        n=lookup(indexb,i,k)
        !  n is the bonded distance found so far. Update it if the
        !  new value is shorter.
        if ((n .eq. 0 .or. n .gt. m1+m2)                               &
            .and. m1+m2 .le. limit) then
          call insert(indexb,i,k,m1+m2,"bond index")
          modify=.true.
        end if
      end do

      !  Now look for K such that I --> J and I --> K
      k=j
      do
        call entry(indexb,i,k,m2)
        if (k == 0) exit
        n=lookup(indexb,j,k)
        if ((n .eq. 0 .or. n .gt. m1+m2)                               &
            .and. j .ne. k .and. m1+m2 .le. limit) then
          call insert(indexb,j,k,m1+m2,"bond index")
          modify=.true.
        end if
      end do

      !  Finally look for K such that I --> J and K --> J
      k=head(mol)
      do while (k .ne. j)
        m2=lookup(indexb,k,j)
        if (m2 == 0) exit
        n=lookup(indexb,i,k)
        if ((n .eq. 0 .or. n .gt. m1+m2)                             &
            .and. i .ne. k .and. m1+m2 .le. limit) then
          call insert(indexb,i,k,m1+m2,"bond index")
          modify=.true.
        endif
        k=next(k)
      end do
    end do
    i=next(i)
  end do
  iter=iter+1
end do

if (test) print "(1x,a,i2,a)", "LINKS completed in", iter, " passes"
END SUBROUTINE links

!------------------------------------------------------------------------

DOUBLE PRECISION FUNCTION length(v)
IMPLICIT NONE
DOUBLE PRECISION, INTENT(IN) :: v(3)
length=sqrt(v(1)**2+v(2)**2+v(3)**2)
END FUNCTION length

!------------------------------------------------------------------------

SUBROUTINE bond_matrix(mol,bm,n,nb,map,pam)
USE sites, ONLY : next, ps
USE molecules, ONLY : head
! USE utility, ONLY : matwrt, matwrtv
IMPLICIT NONE
INTEGER, INTENT(IN) :: mol
INTEGER, INTENT(OUT) :: n, nb, map(:), pam(:)
INTEGER, INTENT(OUT), ALLOCATABLE :: bm(:,:)

INTEGER :: i, j, k, m, t, ok ! , p, nrot
LOGICAL nobonds

!  Construct bond connectivity matrix for the polarizable sites of molecule
!  mol

!  Set up mapping between sites and matrix rows. On exit, map(i) contains
!  the site number corresponding to row i of the matrix, and n contains
!  the dimension of the matrix (the number of polarizable sites).
!  pam(k) contains the row of the matrix associated with site k.
!  Only sites marked as polarizable are included.

!  Initially it's assumed that the matrix of bonds for all sites has
!  been set up already (nobonds=.false.). If the bond matrix is empty,
!  index_bonds is called to set it up.
i = 0
map = 0
pam = 0
k = head(mol)
do while (k > 0)
  if (ps(k) > 0) then
    i = i+1
    map(i) = k
    pam(k) = i
  end if
  k = next(k)
end do
n = i
allocate(bm(n,n), stat=ok)
if (ok > 0) call die("Can't allocate bond matrix")

!  Set up bond matrix: off-diagonal element (i,j) = 1 if there is a direct
!  bond between i and j, zero otherwise, while the diagonal element (i,i)
!  is minus the number of bonds to i. Only polarizable sites are counted.
nb = 0
bm = 0
nobonds = .false.
do
  k = head(mol)
  do while (k > 0)
    if (ps(k) > 0) then
      j = 0
      i = pam(k)
      do
        call entry(indexb,k,j,m)
        if (j == 0) then
          exit
        else if (ps(j) > 0) then
          t = pam(j)
          bm(i,t) = 1
          bm(t,i) = 1
          bm(i,i) = bm(i,i)-1
          bm(t,t) = bm(t,t)-1
          nb = nb + 1
        end if
      end do
    end if
    k = next(k)
  end do
  if (nb > 0) exit
  if (nobonds) then
    print "(2a)", "Can't find any bonds in ", trim(name(head(mol)))
    return
  else
    nobonds = .true.
    !  Bonds not defined -- use auto-bonds
    print "(a)", "No bonds defined -- finding them automatically"
    auto_bonds = .true.
    list_bonds = .true.
    call index_bonds(mol)
  end if
end do


END SUBROUTINE bond_matrix

END MODULE connect
