SUBROUTINE relative(lk,k,rr)

USE consts
USE global
USE output
USE rotations, ONLY : quater !, rotmat
USE switches
USE variables
USE sites
USE molecules
IMPLICIT NONE

!  From the matrices SE(i,j,LK) and SE(i,j,K) describing the orientation
!  of site K and its parent LK relative to global axes, construct a
!  rotation matrix R describing the orientation of K relative to LK,
!  convert this into an axis-angle rotation, and store this in the
!  description of the molecule.

!  Similarly, transform the vector SX(i,K) describing the position of
!  site K in global coordinates into local coordinates relative to LK,
!  and store the transformed vector in the molecule description.

INTEGER, INTENT(IN) :: k, lk
DOUBLE PRECISION, INTENT(OUT), OPTIONAL :: rr(3,3)
DOUBLE PRECISION :: aa, theta, a(0:3), x(3), r(3,3)
! DOUBLE PRECISION :: twist, s(3,3), v(3,3)
INTEGER :: i

x=matmul(sx(:,k)-sx(:,lk),se(:,:,sm(lk)))
print "(4a/3f10.5, 1x, a)", "Position of site ", trim(name(k)),         &
    ' relative to site ', trim(name(lk)), x*rfact, runit
r=matmul(transpose(se(:,:,sm(lk))),se(:,:,k))
if (present(rr)) rr=r
print "(4a)", 'Axes for site ', trim(name(k)),                          &
    ' defined relative to site ', trim(name(lk))
print '(a/(a,3f11.6))',                                                 &
    '      Local axes:        x          y          z',                 &
    'Axes of parent: p_x', r(1,:),                                      &
    '                p_y', r(2,:),                                      &
    '                p_z', r(3,:)

!  Now X contains the position of site K relative to LK in the local axis
!  system of site LK. Store it in the molecule description.
do i=1,3
  if (sp(i,k) .eq. 0) sp(i,k)=vslot(.false.)
  value(sp(i,k))=x(i)
  vtype(sp(i,k))=1
end do
!  Set position type to cartesian if necessary
sf(k)=ibclr(sf(k),4)
sf(k)=ibset(sf(k),5)

!  Also R contains the rotation matrix between site K and its parent: R(i,j)
!  is the direction cosine between axis i of the parent and axis j of site K.

! !  Has a twist been specified?
!       if (st(k) .ne. 0) then
!         twist=vvalue(st(k))
! !  The site orientation in R is the product of the twist matrix S(T) with
! !  the basic rotation, which is therefore S(-T).R
!         call rotmat(-twist,x,s)
!         v=matmul(s,r)
! !  Set twist flag
!         sf(k)=ior(sf(k),4)
! !  Convert the rotation matrix first into quaternion parameters ...
!         call quater(v,a)
!       else
call quater(r,a)
!       endif
print '(a,4f10.5)', 'Quaternion parameters:', a(0:3)
!  .. and then into an axis-angle rotation specification.
aa=sqrt(a(1)**2+a(2)**2+a(3)**2)
sf(k)=ibclr(sf(k),0)  !  Not Euler-angle rotation
if (aa .lt. 1d-10) then
  sr(1,k)=0
  sr(2,k)=0
  sr(3,k)=0
  sr(4,k)=0
  sf(k)=ibclr(sf(k),1)  !  Not angle-axis rotation either
else
  do i=2,4
    if (sr(i,k) .eq. 0) sr(i,k)=vslot(.false.)
    value(sr(i,k))=a(i-1)/aa
    vtype(sr(i,k))=0
  end do
  theta=2d0*acos(a(0))
  if (sr(1,k) .eq. 0) sr(1,k)=vslot(.false.)
  value(sr(1,k))=theta
  vtype(sr(1,k))=2
  sf(k)=ibset(sf(k),1)  !  Angle-axis rotation
endif
if (iand(sf(k),3) .eq. 0) then
  print '(a/)', 'Local axes coincide with axes of parent'
else
  call putstr('Rotation through',1)
  call putg(vvalue(sr(1,k))*afact,12,6,.true.)
  call putstr(' about (',1)
  call putg(vvalue(sr(2,k)),12,6,.true.)
  call putstr(',',1)
  call putg(vvalue(sr(3,k)),12,6,.true.)
  call putstr(',',1)
  call putg(vvalue(sr(4,k)),12,6,.true.)
  call putstr(' )',0)
  if (iand(sf(k),4) .eq. 4) then
    call putstr(' Twist',1)
    call putg(vvalue(st(k))*afact,12,6,.true.)
  endif
  call putout(0)
endif

END SUBROUTINE relative
