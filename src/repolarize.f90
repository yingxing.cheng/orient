SUBROUTINE repolarize

!  Given a molecule description in two or more fragments, each defined
!  as a separate molecule and comprising `intrinsic' moments and model
!  polarizabilities on each fragment, regenerate a model involving
!  first-order polarization of each fragment by the others.

USE force
USE input, ONLY: assert, reada
USE induction, ONLY: dq0, new
USE sites, only: ps, next, Q, L, name, locate
USE molecules, ONLY: head, mols
USE switches, ONLY: switch
USE show_data, ONLY: punchm

IMPLICIT NONE

DOUBLE PRECISION :: etotal, es, er, eind, edisp
INTEGER :: k, m, p
LOGICAL :: sw_save(12)
CHARACTER(20) :: label

do m = 1,mols
  call axes(m,.false.)
end do

call reada(label)
sw_save = switch
switch(7) = .true.   ! induce on
switch(8) = .false.  ! iterate off
call forces(0,0,es,er,eind,edisp,etotal)
!  This should leave the first-order induced moments for polarizable
!  site p in dq0(:,p,new)
switch = sw_save

do m = 1,mols
  print "(2a)", "Molecule ", trim(name(head(m))) 
  k = head(m)
  !  head(m) will be 0 for suppressed molecules
  do while (k > 0)
    if (l(k) > 0 .and. ps(k) > 0) then
      ! print "(a6,4f10.6)", name(k), Q(1:4,k)
      p = ps(k)
      Q(1:4,k) = Q(1:4,k) + dq0(1:4,p,new)
      ! print "(a6,4f10.6)", name(k), dq0(1:4,p,new)
      print "(a6,4f10.6)", name(k), Q(1:4,k)
    end if
    k = next(k)
  end do
  call punchm(m,trim(name(head(m)))//".repol"//trim(label))
  print "(4a)", "Repolarized molecule ", trim(name(head(m))),           &
      " written to file ", trim(name(head(m)))//".repol"//trim(label)
end do

END SUBROUTINE repolarize
