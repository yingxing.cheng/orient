MODULE indparams
!  Parameters controlling polarizability and induced-moment arrays
!                  MAXLA  maximum rank of polarizabilities
INTEGER, PARAMETER :: maxla=4
!  Derived value:  SA     size of polarizability arrays
INTEGER, PARAMETER :: sa=(maxla+1)**2
END MODULE
