      MODULE names

      INTEGER LABELS
      PARAMETER (LABELS=81)
!  LABEL is the array of angular momentum indices: 00, 10, 11c, 11s, etc.
      CHARACTER*4 LABEL(LABELS)
      CHARACTER*79 TITLE(2)

      END MODULE names
