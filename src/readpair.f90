SUBROUTINE readpair

!  Called when a PAIR command is encountered in the data.

USE parameters, ONLY : sq
USE consts, ONLY: efact, rfact
USE labels
USE types
USE input
USE sizes
USE interact
USE Sfns, ONLY : t1s, t2s, js, smap, offset
IMPLICIT NONE


CHARACTER(LEN=4) :: w1, w2
CHARACTER(LEN=20) :: key
LOGICAL :: eof  !, missing

INTEGER :: i, j, jd, k1, k2, k, ix, ix12, ix21, ix0, m, n, n12, n21, &
    which(10), t, t1, t2
DOUBLE PRECISION :: b=0, v, alphadef, temp

alphadef=0d0

pr:   do
  call read_line(eof)
  if (eof) call die ('Unexpected end of file',.false.)
  t1=0; t2=0
  call readu(key)
  select case(key)
  case ("END")
    call showparams
    exit pr
  case ("NOTE","!"," ")
    cycle pr
  case ("UNIT","PREFACTOR")
    call readf(BMunit(0,0),efact)
    cycle pr
  case ("DISPERSION")
    call readu(key)
    if (key .ne. "DAMPING") call reread(-1)
    call readdamp(dform(0),dampf(:,0))
    cycle pr
  case ("INDUCTION")
    call readu(key)
    if (key .ne. "DAMPING") call reread(-1)
    call readdamp(indform(0),indampf(:,0))
    cycle pr
!  "Damping" alone sets both for backward compatibility
  case ("DAMPING")
    call readdamp(dform(0),dampf(:,0))
    indform(0)=dform(0)
    indampf(:,0)=dampf(:,0)
    cycle pr
  case ("HARD","HARD-SPHERE")
    if (item < nitems) then
      call readf(alphadef)
    else
      alphadef=25d0
    endif
    call hardsphere
    cycle pr

  case default
    !  In the absence of any of the above keywords, we expect a pair
    !  definition.
    call reread(-1)
    call reada(w1)    ! Read type names (4 characters)
    call reada(w2)
    k1=0
    k2=0

    k1=gettype(w1)
    k2=gettype(w2)
    ! print "(a,2(1x,i0,1x,a))", "Types: ", k1, trim(tname(k1)), k2, trim(tname(k2))

!  Insert table entry for this pair of types, unless there is one already
    n12=pairindex(k1,k2)
    if (n12 == 0) then
      npair=npair+1
      if (npair > maxpairs) call die                            &
          ("Too many pair interactions",.true.)
      pairindex(k1,k2)=npair
      n12=npair
      sptr(n12)=0
    endif
    if (sptr(n12) == 0) then
      BMunit(0,n12)=BMunit(0,0)
      dampf(:,n12)=dampf(:,0)       ! dispersion damping defaults
      dform(n12)=dform(0)
      indampf(:,n12)=indampf(:,0)   ! induction damping defaults
      indform(n12)=indform(0)
      ixptr=ixptr+1
      ixs(ixptr)=1
      sptr(n12)=ixptr
      ix0=ixptr
      nexts(ixptr)=0
      alph(ixptr)=alphadef
      if (radius(k1) > 0.01d0 .and. radius(k2) > 0.01d0) then
        rho(ixptr) = radius(k1)+radius(k2)
        if (indform(n12) == 20) indampf(:,n12) = radius(k1)+radius(k2)
      else
        rho(ixptr) = 0d0
      endif
      do i=6,12
        C(ixptr,i) = 0d0
      end do
    endif
    if (k1 == k2) then
      n21=n12
    else
      n21=pairindex(k2,k1)
      if (n21 == 0) then
        npair=npair+1
        if (npair > maxpairs) call die                          &
            ("Too many pair interactions",.true.)
        pairindex(k2,k1)=npair
        n21=npair
        sptr(n21)=0
      endif
      if (sptr(n21) == 0) then
        BMunit(0,n21)=BMunit(0,0)
        dampf(:,n21)=dampf(:,0)       ! dispersion damping defaults
        dform(n21)=dform(0)
        indampf(:,n21)=indampf(:,0)   ! induction damping defaults
        indform(n21)=indform(0)
        ixptr=ixptr+1
        ixs(ixptr)=1
        sptr(n21)=ixptr
        nexts(ixptr)=0
        alph(ixptr)=alphadef
        if (radius(k1) > 0.01d0 .and. radius(k2) > 0.01d0) then
          rho(ixptr) = radius(k1)+radius(k2)
          if (indform(n21) == 20) indampf(:,n21) = radius(k1)+radius(k2)
        else
          rho(ixptr)=0d0
        endif
        do i=6,12
          C(ixptr,i)=0d0
        end do
      endif
    endif
    ! print "(4(a,i0))", "n12 = ", n12, ", n21 = ", n21

!     print '(A,I3,A,2A4,A,I4)',
!    &    'Pair', n12, '  Types ', tname(k1), tname(k2),
!    &    '  Index pointer', sptr(n12)
!     if (k1 .ne. k2) print '(A,I3,A,2A4,A,I4)',
!    &    'Pair', n21, '  Types ', tname(k2), tname(k1),
!    &    '  Index pointer', sptr(n21)


!  Which parameters are being provided?
    K=0
    do while (item < nitems)
      K=K+1
      call readu(key)
      if (key == "ALPHA") then
        which(K)=1
      else if (key == "RHO") then
        which(K)=2
      else if (key(1:1) == 'C') then
        read (unit=key(2:),fmt='(I2)') which(K)
        if (which(k) < 6 .or. which(k) > 12)                    &
            call die("Invalid dispersion coefficient",.true.)
      else if (key(1:5) == 'GAMMA') then
        read (unit=key(6:),fmt='(I2)') which(K)
        if (which(k) < 6 .or. which(k) > 12)                    &
            call die("Invalid dispersion coefficient",.true.)
        which(K)=-which(K)
      else
        call die ('Unrecognized potential term',.true.)
      endif
    end do
    which(K+1)=0

    ! missing=.false.
    Sfuncs: do
      call read_line(eof)
      if (eof) call die ('Unexpected end of file',.false.)
      call reada(key)
      select case(upcase(key))
      case ("END")
        cycle pr
      case("NOTE","!"," ")
        cycle Sfuncs
      case("UNIT")
        call readf(temp,efact)
        temp = temp/BMunit(0,n12)
        BMunit(:,n12) = BMunit(:,n12) * temp
        BMunit(:,n21) = BMunit(:,n12)
      case("SLATER")
        call readf(b,1d0/rfact)
        BMunit(1,n12) = BMunit(0,n12)*b
        BMunit(2,n12) = BMunit(0,n12)*b**2/3d0
        BMunit(:,n21) = BMunit(:,n12)
      case("PREFACTOR")
        call reada(key)
        select case(upcase(key))
        case("UNIT")
          call readf(BMunit(0,n12),efact)
          BMunit(0,n21)=BMunit(0,n12)
        case default
          call reread(-1)
          i=0
          do while (item<nitems)
            i=i+1
            if (i>2) call die                                          &
                ("Prefactor polynomial is limited to quadratic",.true.)
            call readf(BMunit(i,n12))
            BMunit(i,n12)=BMunit(i,n12)*rfact**i
            BMunit(i,n21)=BMunit(i,n12)
          end do
          prefactor_order=max(prefactor_order,i)
        end select
        cycle Sfuncs
      case ("DISPERSION")
        call readu(key)
        if (key .ne. "DAMPING") call reread(-1)
        call readdamp(dform(n12),dampf(:,n12),                        &
            type21=dform(n21),dampf21=dampf(:,n21))
        cycle Sfuncs
      case ("INDUCTION")
        call readu(key)
        if (key .ne. "DAMPING") call reread(-1)
        call readdamp(indform(n12),indampf(:,n12),                   &
            type21=indform(n21),dampf21=indampf(:,n21))
        cycle Sfuncs
      case ("DAMPING")
        call readdamp(dform(n12),dampf(:,n12),                        &
            type21=dform(n21),dampf21=dampf(:,n21))
        indform(n12)=dform(n12)
        indform(n21)=dform(n12)
        indampf(:,n12)=dampf(:,n12)
        indampf(:,n21)=dampf(:,n12)
        cycle Sfuncs
      case default
        w1 = locase(key(1:4))
        call readl(w2)
        call readi(j)
        t1=0
        t2=0
        do t=1,nlabels
          if (w1 == label(t)) t1=t
          if (w2 == label(t)) t2=t
        end do
        if (t1 == 0) call die('Label '//w1//' not found',.true.)
        if (t2 == 0) call die('Label '//w2//' not found',.true.)
        if (lq(t1)+lq(t2) < j .or. lq(t1)+j < lq(t2)            &
            .or. lq(t2)+j < lq(t1)) call die                    &
            ('Invalid S-function specification ',.true.)
        if (t1 .le. sq .and. t2 .le. sq) then
          jd=lq(t1)+lq(t2)-j
          ix=smap(t1,t2,jd)
        else  ! search special list
          ix=0
          do i=offset(11)+1,offset(12)
            if (t1s(i) == t1 .and. t2s(i) == t2                   &
                .and. js(i) == j) ix=i
          end do
        endif
        ! print "(3a,i2,a,i0)", "S function ", label(t1), label(t2), j, ": index ", ix
        if (ix == 0) then
          print '(/a,a,a,i2,a)',                                      &
              'Sorry, S(', w1, w2, j, ') not implemented'
          ! missing=.true.
        else
          i=sptr(n12)
          do while (ixs(i) < ix .and. i > 0)
            m=i
            i=nexts(i)
          end do
          if (ixs(i) == ix) then
            !  Entry already present
            ix12=i
          else
            !  Insert an index entry for this S function, in index order.
            ixptr=ixptr+1
            if (ixptr > maxix) then
              print "(a)", "Too few parameter-sets allocated."
              call die('Increase value with ALLOCATE command.',.true.)
            end if
            ix12=ixptr
            ixs(ixptr)=ix
            nexts(ixptr)=i
            nexts(m)=ixptr
            alph(ixptr)=0d0
            rho(ixptr)=0d0
            C(ixptr,6:12)=0d0
          endif
        endif

        if (k1 == k2 .and. t1 == t2) then
          ix21=ix12
        else
          !  Entry needed for swapped S function
          if (t1 .le. sq .and. t2 .le. sq) then
            jd=lq(t1)+lq(t2)-j
            ix=smap(t2,t1,jd)
          else  ! search special list
            ix=0
            do i=offset(11)+1,offset(12)
              if (t1s(i) == t2 .and. t2s(i) == t1                 &
                  .and. js(i) == j) ix=i
            end do
          endif
          if (ix == 0) then
            print '(/a,a,a,i2,a)',                                    &
                'Sorry, S(', w1, w2, j, ') not implemented'
            ! missing=.true.
          else
            i=sptr(n21)
            do while (ixs(i) < ix .and. i > 0)
              m=i
              i=nexts(i)
            end do
            if (ixs(i) == ix) then
              !  Entry already present
              ix21=i
            else
              !  Insert an index entry for this S function, in index order.
              ixptr=ixptr+1
              if (ixptr > maxix) then
                print "(a)", "Too few parameter-sets allocated."
                call die('Increase value with ALLOCATE command.',.true.)
              end if
              ix21=ixptr
              ixs(ixptr)=ix
              nexts(ixptr)=i
              nexts(m)=ixptr
              alph(ixptr)=0d0
              rho(ixptr)=0d0
              C(ixptr,6:12)=0d0
            endif
          endif
        endif
        ! print "(2(a,i0))", "ix12 = ", ix12, ", ix21 = ", ix21

        !  Read coefficients for this S function
        do I=1,K
          call readf(v)
          if (which(i) == 1) then
            alph(ix12)=v*rfact
            alph(ix21)=v*rfact
          else if (which(i) == 2) then
            rho(ix12)=v/rfact
            rho(ix21)=v/rfact
          else if (which(i) .ge. 6 .and. which(i) .le. 12) then
            n=which(i)
            C(ix12,n)=v/(efact*rfact**n)
            C(ix21,n)=v/(efact*rfact**n)
          else if (which(i) .le. -6 .and. which(i) .ge. -12) then
            n=-which(i)
            if (ixs(ix12) == 1) then
              C(ix12,n)=v/(efact*rfact**6)
              C(ix21,n)=v/(efact*rfact**6)
            else
              C(ix12,n)=v*C(ix0,n)/(efact*rfact**n)
              C(ix21,n)=v*C(ix0,n)/(efact*rfact**n)
            endif
          endif
        end do
        if (item < nitems)                                         &
            call die('Too many numbers supplied',.true.)
      end select

    end do Sfuncs

  end select
end do pr

CONTAINS

!-----------------------------------------------------------------------

SUBROUTINE hardsphere

!  Hard-sphere repulsion potential for all sites

INTEGER t1, t2

do t1 = 1,ntype
  do t2 = 1,ntype
    if (radius(t1) > 0.01 .and. radius(t2) > 0.01) then
      n12 = pairindex(t1,t2)
      if (n12 == 0) then
        npair = npair+1
        if (npair > maxpairs) call die                    &
            ("Too many pair interactions",.true.)
        pairindex(t1,t2) = npair
        n12 = npair
        sptr(n12) = 0
      endif
      if (sptr(n12) == 0) then
        BMunit(0,n12) = BMunit(0,0)
        ixptr = ixptr+1
        ixs(ixptr) = 1
        sptr(n12) = ixptr
        nexts(ixptr) = 0
        alph(ixptr) = alphadef
        rho(ixptr) = radius(t1)+radius(t2)
        C(ixptr,6:12) = 0d0
      endif
      if (t1 .ne. t2) then
        n21 = pairindex(t2,t1)
        if (n21 == 0) then
          npair = npair+1
          if (npair > maxpairs) call die                  &
              ("Too many pair interactions",.true.)
          pairindex(t2,t1) = npair
          n21 = npair
          sptr(n21) = 0
        endif
        if (sptr(n21) == 0) then
          BMunit(0,n21) = BMunit(0,0)
          ixptr = ixptr+1
          ixs(ixptr) = 1
          sptr(n21) = ixptr
          nexts(ixptr) = 0
          alph(ixptr) = alphadef
          rho(ixptr) = radius(t1)+radius(t2)
          C(ixptr,6:12) = 0d0
        endif
      endif
    endif
  end do
end do

END SUBROUTINE hardsphere

!-----------------------------------------------------------------------

SUBROUTINE readdamp(type12,dampf12,type21,dampf21)

USE consts, ONLY : rfact
USE input
IMPLICIT NONE

INTEGER, INTENT(OUT) :: type12
INTEGER, INTENT(OUT), OPTIONAL ::  type21
DOUBLE PRECISION, INTENT(OUT) :: dampf12(0:)
DOUBLE PRECISION, INTENT(OUT), OPTIONAL :: dampf21(0:)

CHARACTER*20 :: key
INTEGER :: i

dampf12(:) = 0d0
call reada(key)
select case(locase(key))
case ("off","none")
  type12 = 0
case ("factor","tt","tang-toennies")
  if (item < nitems) then
    type12 = 1
    call readf(dampf12(0),1d0/rfact)
  else
    type12 = 2
    dampf12(0) = 1d0
  end if
case("sqrttt")
  call readf(dampf12(0))
  type12 = 16
case("tt-slater","tang-toennies-slater","tt-s")
  if (item < nitems) then
    type12 = 21
    call readf(dampf12(0),1d0/rfact)
  else
    type12 = 22
    dampf12(0) = 1d0
  end if
case("tang-toennies-sca","tt-sca")
  type12 = 24
  dampf12(0) = 1d0
case ("tang-toennies-alpha","tt-alpha")
  type12 = 2
case ("hfd")
  type12 = 3
  call readf(dampf12(0))
case ("kma")
  type12 = 4
  call readf(dampf12(0))
case ("erf")
  type12 = 5
  call readf(dampf12(0))
case ("gaussian","sbcs")
  if (locase(key) == "sbcs") then
    type12 = 6
  else
    type12 = 20
  end if
  if (item < nitems) then
    call readf(dampf12(0),rfact)
  else if (radius(k1) .ne. 0d0 .and. radius(k2) .ne. 0d0) then
    dampf12(0) = radius(k1)+radius(k2)
  else
    call die("Van der Waals radii needed for "//trim(key)//" damping",.true.)
  endif
case ("pade","special","ge","gg")
  select case(key)
  case("pade")
    call readi(i)
    type12 = (i/2)+5
  case ("special")
    call readi(type12)
  case("ge")
    type12 = 7
  case("gg")
    type12 = 17
  end select
  i = 0
  do while (item < nitems)
    call readf(dampf12(i))
    i = i+1
  end do
  if (key == "special" .or. key == "pade")                    &
      dampf12(0) = dampf12(0)/rfact
case default
  call die("Unrecognized damping function "//key,.true.)
end select
if (present(type21)) type21 = type12
if (present(dampf21)) dampf21(:) = dampf12(:)

END SUBROUTINE readdamp

END SUBROUTINE readpair

!-----------------------------------------------------------------------

SUBROUTINE showparams

USE consts
USE global
USE labels
USE types
USE sizes
USE interact
USE Sfns, ONLY : t1s, t2s, js
IMPLICIT NONE

INTEGER i, t1, t2, n12, ix, ix12, last, n
character(120) :: fmt, header
character(3) :: str

print '(/A)', 'Pair potentials'
print "(/A)", "Default values"
call printparams(0)
do t1 = 1,ntype
  do t2 = 1,ntype
    n12 = pairindex(t1,t2)
    if  (n12 .ne. 0) then
      print '(/4A)',                 &
          'Types: ', trim(tname(t1)), " and ", trim(tname(t2))
      call printparams(n12)
      ix12 = sptr(n12)
      !Define the header line:
      !----------------------
      header = '      t1  t2   j      rho        alpha    '
      last = 12
      do while (last .ge. 6 .and. C(ix12,last) == 0d0)
        last = last-1
      end do
      if (last>=6) then
        do n = 6, last
          write(str,'(i3)') n
          header = trim(header)//'         C'//adjustl(str)
        enddo
      endif
      print '(a)',trim(header)
      !----------------------
      do while (ix12 .ne. 0)
        ix = ixs(ix12)
        last = 12
        do while (last .ge. 6 .and. C(ix12,last) == 0d0)
          last = last-1
        end do
        !AJM: Altering the output as this is not suitable for post-processing:
        !print '(I4,2X,2A,I2,2F12.6,1P,3G12.4/40X,4G12.4)',             &
        write (fmt,'(a,i0,a)')' (6X,2A,I2,2F12.6,1P,3G12.4,',           &
            max(last-6+1,1),'(1x,1P,G12.4))'
        print fmt, label(t1s(ix)), label(t2s(ix)), js(ix),              &
            rho(ix12)*rfact, alph(ix12)/rfact,                          &
            (C(ix12,i)*efact*rfact**i, i = 6,last)
        ix12 = nexts(ix12)
      end do
    endif
  end do
end do

CONTAINS

SUBROUTINE printparams(n)

IMPLICIT NONE
INTEGER, INTENT(IN) :: n
INTEGER :: i

if (all(BMunit(1:2,n)==0d0)) then
  print "(a,1p,e12.4)", 'Pre-exponential factor:', BMunit(0,n)
else
  print "(a,1p,3(e12.4,a))", "Pre-exponential factor:", BMunit(0,n),   &
      " * ( 1 + ", BMunit(1,n)/rfact, " * R + ",                       &
      BMunit(2,n)/(rfact**2), " * R^2 )"
end if
if (dform(n) == 0) then
  print "(a)", "No dispersion damping"
else
  i = ubound(dampf,1)
  do while (i > 0 .and. dampf(i,n) == 0d0)
    i = i-1
  end do
  print '(A,I2,A,0P,3G12.5:/(14x,5G12.5))',                              &
      'Dispersion damping type', dform(n),                               &
      '  Parameters', dampf(0:i,n)
end if
if (indform(n) == 0) then
  print "(a)", "No induction damping"
else
  i = ubound(indampf,1)
  do while (i > 0 .and. indampf(i,n) == 0d0)
    i = i-1
  end do
  print '(A,I2,A,0P,3G12.5:/(14x,5G12.5))',                              &
      'Induction damping type ', indform(n),                             &
      '  Parameters', indampf(0:i,n)
end if

END SUBROUTINE printparams

END SUBROUTINE showparams
