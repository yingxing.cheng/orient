SUBROUTINE edit

!  Modify a molecule description
!  A2  Includes the AXES option to define the local axes of specified sites
!  A4  Includes the SHIFT option to move multipoles to another site
!  A5  Redefines axes for polarizabilities as well as moments
!  A5  Bug corrected in lookup for polarizability matrices
!  A6  Minor modifications to permit rank 5 multipoles

!  EDIT [molecule]
!  sub-commands
!  END

!  The subcommands are:

!  MOLECULE name
!    Subsequent subcommands apply to the specified molecule.
!
!  AXES
!    ...
!    site0 {X|Y|Z} [FROM site1] { TO site2 | BETWEEN site2 AND site2' } +++
!        {X|Y|Z} [FROM site3] { TO site4 | BETWEEN site4 AND site4' } +++
!        [TWIST angle]
!    ...
!  END
!    Define local axes for the sites of the current molecule. There should
!    be one line for each site. In each line, site0 is the site for which
!    axes are being defined. The first axis mentioned runs in the direction
!    from site1 either to site2 or in the direction of the line bisecting
!    the 2-0-2' angle. The second axis is initially from site 3 to site 4
!    (or from site 3 in the direction of the line bisecting the 4-0-4' angle)
!    but is then orthogonalized to the first. The third axis forms a right-
!    handed set. 'FROM site1' may be omitted if site1 is the same as site0;
!    similarly 'FROM site3'.

!  DELETE site site ...
!  CLEAR site site ...
!  LIMIT [RANK] j [CLEAR] [FOR] <list>
!    where list is ALL or { TYPE type-name | site-name } ...
!    Limit rank of sites, all or specified by name or type-name. If CLEAR
!    is specified, delete higher moments; otherwise they are kept and may
!    be restored by a later LIMIT with a higher rank.
!  SUPPRESS site site ...
!  RESTORE site site ...
!  NEGATE site site ...
!  TYPE type [FOR] site site ...
!  SCALE factor [ALL] [FOR site site ...]
!  UNSCALE factor [ALL] [FOR site site ...]
!  MOMENTS [FOR] site Qlm [=] <Qlm> ...
!    In the above, the site name need not be unique. The specification
!    is applied to all sites with the name given. DELETE removes a site
!    from the molecule; CLEAR sets its multipoles to zero but leaves it
!    in situ. SUPPRESS suppresses the multipoles for the sites listed
!    (by setting the rank negative) but leaves the values unchanged.
!    RESTORE restores the original rank and hence the multipoles.
!    NEGATE changes the sign of all the moments at the specified sites.
!    SCALE multiplies the moments for the specified sites, or for all
!    sites, by the factor given. UNSCALE does the opposite (divides by the
!    factor, to restore the original values if the factor is the same.)

!  COMBINE name
!    The moments for the named molecule are added to those for the current
!    molecule. (Usually one set or the other will have been negated first,
!    so as to construct the difference.) The site names for the two molecules
!    must match, site by site.

!  SHIFT [RANK n] FROM site1 TO site2
!    In this case the site names should be unique. (If they are not, the
!    last site found with the specified name will be used.) The multipoles
!    for site1 of ranks n and above are transformed to the local axis
!    system of site2 and transferred to that site using the standard
!    formula. If no rank is specified, all ranks are transferred.

!  BONDS [RANGE n] site -> site [&] site ... [; site -> site [&] site ...] ...
!    In this case, the site names must be unique, except that the last
!    name in each sub-list may describe several atoms all bonded to the
!    previous one mentioned. E.g.
!      BONDS  C1 -> H1 ; C1 -> C2 -> H2 ; C2 -> C3 -> H3
!    is taken to mean that:
!      One or more atoms named H1 are bonded to C1;
!      Site C2 is bonded to C1, and one or more atoms named H2 are also
!      bonded to C2; and
!      C3 is bonded to C2 and one or more H3 sites are bonded to C3.
!    So this describes the bonds in propane.
!    Note that the semi-colon separating sub-lists must be preceded and
!    followed by a space.
!    If several site names are separated by ampersands (preceded and
!    followed by spaces) every site in the group is bonded to the site
!    preceding the group. So another way to describe the bonding in
!    propane is
!      BONDS C1 -> H11 & H12 & H13 & C2 -> H21 & H22 & C3 -> H31 & H32 & H33
!    If the RANGE option is given, it specifies the maximum distance
!    between atoms (in terms of the number of bonds) that is recorded in
!    the connectivity table. The default is 1. However this option is no
!    longer supported. (The information wasn't used anywhere.)

!  CHARGE site site ...
!    Evaluate and print the total charge on the sites specified, including
!    every site having one of the names given.


USE parameters
USE indparams
USE alphas
USE connect
USE consts
! USE geom, ONLY : geometry
USE global, ONLY: tracing
USE indexes
USE labels
USE properties
USE switches, print_level => print
USE types
USE variables
USE input
USE rotations, ONLY : wigner, rotmat
USE sites
USE sizes
USE molecules
IMPLICIT NONE

INTERFACE
  SUBROUTINE relative(lk,k,rr)
  INTEGER, INTENT(IN) :: k, lk
  DOUBLE PRECISION, INTENT(OUT), OPTIONAL :: rr(3,3)
  END SUBROUTINE relative
  SUBROUTINE axis(m,k0,r,e,verbose)
  INTEGER, INTENT(IN) :: m, k0
  DOUBLE PRECISION :: r(3,3), e(3,3)
  LOGICAL, INTENT(IN), OPTIONAL :: verbose
  END SUBROUTINE axis
END INTERFACE

INTEGER :: i, j, k, n, p, is, k1, k2, kr, limit, m, m1, m2, maxq, top
CHARACTER(LEN=12) :: w
CHARACTER(LEN=20) :: ww, w1, site_type
CHARACTER(LEN=80) :: www
LOGICAL :: eof, found, flag(sq), all, clearq, verbose=.false.
DOUBLE PRECISION :: charge, qx(sq), r(3,3), x(3), ox(3), nx(3), psi,    &
    d(sq,sq), temp(sa,sa), factor
CHARACTER(LEN=*), PARAMETER :: this_routine="edit"

if (tracing) print "(2a)", "Entering ", this_routine
m=0
call reada(ww)
if (ww .ne. "") m=locate(ww)
do
  call read_line(eof)
  call readu(w)
  select case(w)
  case("","NOTE","!")
    cycle
  case("END")
    exit
  case("COMMENT")
    call reada(www)
    print '(a)', trim(www)
  case("VERBOSE")
    verbose=.true.
  case("QUIET")
    verbose=.false.
  case("MOLECULE")
    call reada(ww)
    m=locate(ww,.true.)
    if (m == 0) call die("Can't find molecule "//trim(ww),.true.)
  case("DELETE")
    call assert(m>0,"No molecule specified",.true.)
    do while (item < nitems)
      call reada(ww)
      found=.false.
      do
        k=head(m)
        if (name(k) .ne. ww) exit
        found=.true.
        n=next(k)
        head(m)=n
        next(k)=free
        free=k
        if (print_level>1) print '(a,i3,1x,a)', 'Deleted: site', k, ww
      end do
      p=k
      k=next(k)
      do while (k .ne. 0)
        n=next(k)
        if (name(k) == ww) then
          found=.true.
          next(k)=free
          free=k
          next(p)=n
          if (print_level>1) print '(a,i3,1x,a)', 'Deleted: site', k, ww
          k=n
        else
          p=k
          k=next(k)
        endif
      end do
      if (.not. found) print '(a,a)', 'Site not found: ',ww
    end do

  case("LIMIT")
    call assert(m>0,"No molecule specified",.true.)
    limit=-2
    clearq=.false.
    all = .false.
    site_type = "ignore"
    do while (item<nitems)
      call reada(ww)
      select case(upcase(ww))
      case ("RANK")
        call readi(limit)
      case("FOR")
        exit
      case("CLEAR")
        clearq=.true.
      case("ALL")
        all = .true.
        exit
      case("TYPE")
        call reread(-1)
        exit
      case default
        call reread(-1)
        if (limit<-1) then
          call readi(limit)
        else
          exit
        endif
      end select
    end do
    do while (item<nitems .or. all .or. site_type .ne. "ignore")
      if (item<nitems) then
        call reada(ww)
        select case(upcase(ww))
        case("ALL")
          all = .true.
          cycle
        case("TYPE")
          call reada(site_type)
          cycle
        end select
      end if
      found=.false.
      k=head(m)
      do while (k > 0)
        if (name(k) == ww .or. tname(type(k)) == site_type .or. all) then
          found=.true.
          l(k)=limit
          if (print_level>1 .or. verbose) print '(a,i3,a,i3,1x,a)',     &
              'Limited to rank', limit, ': ', k, name(k)
          if (clearq) then
            q(qsize(limit)+1:,k)=0d0
            if (print_level>1 .or. verbose) print "(a)",                &
                "Higher moments cleared to zero"
          end if
        end if
        k=next(k)
      end do
      if (all) exit
      site_type = "ignore"
      if (.not. found) print '(a,a)', 'No matching site found: ', ww
    end do

  case("SUPPRESS","RESTORE")
    call assert(m>0,"No molecule specified",.true.)
    if (w == 'suppress') then
      is=-1
      w='Suppressed:'
    else
      is=1
      w='Restored:'
    endif
    do while (item < nitems)
      call reada(ww)
      found=.false.
      k=head(m)
      do while (k .ne. 0)
        if (name(k) == ww) then
          found=.true.
          l(k)=is*(iabs(l(k)+1))-1
          if (print_level>1) print '(a,1x,a)', w, ww
        endif
        k=next(k)
      end do
      if (.not. found) print '(a,a)', 'Site not found: ',ww
    end do

  case("SCALE","UNSCALE")
    call assert(m>0,"No molecule specified",.true.)
    call readf(factor)
    if (w == "UNSCALE") factor=1d0/factor
    do while (item < nitems)
      call reada(ww)
      select case(upcase(ww))
      case("ALL")
        k=head(m)
        do while (k .ne. 0)
          top=(l(k)+1)**2
          do i=1,top
            Q(i,k)=Q(i,k)*factor
          end do
          k=next(k)
        end do
        print "(a,f10.6)", "All moments scaled by ", factor
      case("FOR")
        !  Skip
      case default
        k=head(m)
        do while (k .ne. 0)
          if (name(k) == ww) then
            top=(l(k)+1)**2
            do i=1,top
              Q(i,k)=Q(i,k)*factor
            end do
          end if
          k=next(k)
        end do
        print "(a,f10.6)", "Moments scaled by ", factor, " for sites named ", trim(ww)
      end select
    end do


  case("CLEAR")
    call assert(m>0,"No molecule specified",.true.)
    do while (item < nitems)
      call readu(ww)
      if (ww == "ALL") then
        all=.true.
      else
        call reread(-1)
        call reada(ww)
      end if
      found=.false.
      k=head(m)
      do while (k .ne. 0)
        if (all .or. name(k) == ww) then
          found=.true.
          q(1:sq,k)=0d0
          l(k)=-1
          if (print_level>1) print '(a,i3,1x,a)',                      &
              'Moments cleared to zero:', k,ww
        endif
        k=next(k)
      end do
      if (.not. found) print '(a,a)', 'Site not found: ',ww
    end do
    
  case("NEGATE")
    do while (item < nitems)
      call readu(ww)
      if (ww == "ALL") then
        all=.true.
      else
        call reread(-1)
        call reada(ww)
      end if
      found=.false.
      k=head(m)
      do while (k .ne. 0)
        if (all .or. name(k) == ww) then
          found=.true.
          q(1:sq,k)=-q(1:sq,k)
          if (print_level>1) print '(a,i3,1x,a)',                   &
                         'Signs of moments changed:', k, name(k)
        endif
        k=next(k)
      end do
      if (.not. found) print '(a,a)', 'Site not found: ',ww
    end do

  case("COMBINE")
    call assert(m>0,"No molecule specified",.true.)
    call reada(ww)
    m2=locate(ww)
    k=head(m)
    k2=head(m2)
    do while (k .ne. 0)
      if (name(k2) .ne. name(k2)) call die("Names don't match",.true.)
      q(1:sq,k)=q(1:sq,k)+q(1:sq,k2)
      if (print_level>1) print "(a,i0,a,i0)",                          &
          "Moments for site ", k2, " added to moments for site ", k
      k=next(k)
      k2=next(k2)
    end do

  case("SHIFT")
    call assert(m>0,"No molecule specified",.true.)
    k1=0
    k2=0
    n=0
    do while (item < nitems)
      call readu(ww)
      select case(ww)
      case("FROM")
        call read_site(k1,m)
        if (k1 == 0) call die ('Site not found',.true.)
        w1=name(k1)
      case("TO")
        call read_site(k2,m)
        if (k2 == 0) call die ('Site not found',.true.)
      case("RANK")
        call readi(n)
      end select
    end do
    if (k1 == 0) call die ('Source site not specified',.true.)
    if (k2 == 0) call die ('Target site not specified',.true.)
    print "(a,i0,2a)", "Transferring moments of rank ", n,             &
        " and above to site ", trim(name(k2))

    k1=head(m)
    do while (k1 .ne. 0)
      if (name(k1) == w1) then
        print "(3a,i0,a)", "From site ", trim(name(k1)), " (", k1, ")"

        if (l(k1) > l(k2))                                          &
            print '(a,a)',                                             &
            'Warning: rank of source site exceeds rank of target -- ', &
            'information will be lost'
        call axes(m,.false.)
        m1=sm(k1)
        m2=sm(k2)
        maxq=(l(k1)+1)**2
        if (m1 == m2) then
          qx(1:maxq)=q(1:maxq,k1)
        else
          do i=1,3
            do j=1,3
              r(i,j)=0d0
              do p=1,3
                r(i,j)=r(i,j)+se(p,i,m1)*se(p,j,m2)
              end do
            end do
          end do
!  Now R(j,i) is the direction cosine between the j axis of site K1 and the
!  i axis of site K2
          call wigner(r,d,5)
          do i=1,maxq
            qx(i)=0d0
            do j=1,maxq
              qx(i)=qx(i)+q(j,k1)*d(j,i)
            end do
          end do
        endif
!  Now QX contains the moments of site K1, rotated if necessary into the
!  axis frame of site K2
        do i=1,3
          x(i)=0d0
          do j=1,3
            x(i)=x(i)+(sx(j,k1)-sx(j,k2))*se(j,i,m2)
          end do
        end do
!  X now contains the position of site K1 relative to that of K2 in the
!  coordinate frame of K2.

        call shiftq(qx,n,l(k1), x(1),x(2),x(3), q(:,k2),l(k2))
!  Zero the moments of K1 that were transferred. All moments of a particular
!  rank are treated the same way, so the transformation of axes is irrelevant
!  here.
        q(n**2+1:maxq,k1)=0d0
        l(k1)=n-1
      endif

      k1=next(k1)
    end do

  case("MOMENTS","MULTIPOLES")
    call readu(w)
    if (w .ne. 'FOR') call reread(-1)
    call reada(ww)
    flag(1:sq)=.false.
    n=0
    do while (item < nitems)
      call reada(w)
      j=0
      do i=1,sq
        if (w == 'Q'//label(i)) j=i
      end do
      if (j == 0)                                                    &
          call die ('Multipole name not recognised',.true.)
      flag(j)=.true.
      n=max(n,int(sqrt(real(j)-0.5)))
      call reada(w)
      if (w .ne. '=') call reread(-1)
      call readf(qx(j))
    end do
    call assert(m>0,"No molecule specified",.true.)
    k=head(m)
    do while (k .ne. 0)
      if (name(k) == ww) then
        do i=1,sq
          if (flag(i)) q(i,k)=qx(i)
        end do
        l(k)=max(l(k),n)
      endif
      ! print "(a)", name(k)
      ! call printq(q(1:,k),l(k),"(a,9f9.3)")
      k=next(k)
    end do

  case("TYPE")
    call assert(m>0,"No molecule specified",.true.)
    call reada(www)
    j=0
    do i=1,ntype
      if (tname(i) == www) j=i
    end do
    if (j == 0) then
      ntype=ntype+1
      j=ntype
      tname(j)=www
    endif
    call readu(w)
    if (w .ne. 'FOR' .and. w .ne. 'SITE' .and. w .ne. 'SITES')       &
                                                   call reread(-1)
    do while (item < nitems)
      call reada(ww)
      found=.false.
      k=head(m)
      do while (k .ne. 0)
        if (name(k) == ww) then
          type(k)=j
          found=.true.
        endif
        k=next(k)
      end do
      if (.not. found) print '(a,a)', 'Site not found: ',ww
    end do

  case("AXES")
    call assert(m>0,"No molecule specified",.true.)
    !  Find the rotation matrix for each site. se(I,i,k) is the direction
    !  cosine between global axis I and local axis i.
    call axes(m,.false.)
    !  If a site k uses the same axis system as some other site kr (i.e.
    !  sm(k) = kr .ne. k) we have to detach it so that its axes can be
    !  redefined independently.
    k=head(m)
    do while (k .ne. 0)
      kr=sm(k)
      if (kr .ne. k) se(:,:,k)=se(:,:,kr)
      sm(k)=k
      k=next(k)
    end do

    do
      call read_line(eof)
      call readu(ww)
      if (ww == "END") exit
      if (ww == "" .or. ww == "!") cycle
      call reread(-1)
      call reada(ww)
      found=.false.
      k=head(m)
      do while (k .ne. 0)
        if (name(k) == ww) then
          found=.true.
          exit
        endif
        k=next(k)
      end do
      if (.not. found) call die ('Site not found: '//ww,.true.)
      !  Read the axis specification and convert it into direction cosines:
      !  se(I,i,k) is the direction cosine between global axis I and local
      !  axis i.
      !  R is the transformation matrix between the old axes and the new; in R(i,j),
      !  i refers to the old axis and j to the new.
      call axis(m,k,r,se(:,:,k),verbose)
      if (verbose) then
        print '(/a,a)', 'Axes redefined for site ', trim(ww)
        print '(a/(a,3f10.5))',                                        &
            '        Local axes:   x         y         z',             &
            'Global axes:    X', se(1,:,k),                            &
            '                Y', se(2,:,k),                            &
            '                Z', se(3,:,k)
      end if
      !  Convert the rank 1 rotation matrix into Wigner rotation matrices
      !  for ranks up to 5.
      call wigner(r,d,5)
      call rotate_site(k,d)

    end do
    print '(1x)'
    k=next(head(m))
    !  Convert positions into Cartesian vectors relative to parent site,
    !  and orientations into angle-axis rotations relative to parent site.
    do while (k .ne. 0)
      call relative(link(k),k)
      k=next(k)
    end do

  case("ROTATE")
    !  Rotate entire molecule, assumed to be specified in global axes,
    !  ROTATE BY psi {ABOUT|AXIS} nx ny nz THROUGH px py pz
    !  The rotation axis need not be normalized
    call assert(m>0,"No molecule specified",.true.)
    ox = 0d0
    nx = 0d0
    psi = 0d0
    do while (item < nitems)
      call reada(ww)
      select case(upcase(ww))
      case("BY")
        call readf(psi,afact)
      case("ABOUT","AXIS")
        call readf(nx)
      case("THROUGH")
        call readf(ox)
      end select
    end do
    call rotmat(psi,nx,r)
    !  Here we are rotating the multipoles along with the sites, still
    !  referring them to global axes. (In AXES above, we are describing the
    !  unrotated multipoles with respect to new axes.)
    call wigner(transpose(r),d,5)
    k = head(m)
    do while (k > 0)
      !  Rotate position
      if (verbose) print "(a6,3f10.5)", name(k), sx(:,k) - sx(:,link(k))
      sx(:,k) = ox + matmul(r,sx(:,k)-ox) - sx(:,link(k))
      if (verbose) print "(a6,3f10.5)", name(k), sx(:,k) 
      do i=1,3
        call setval(sx(i,k),sp(i,k),0)
      end do
      !  Rotate multipoles and polarizabilities
      call rotate_site(k,d)
      k = next(k)
    end do

  case("BONDS")
    call bonds(m)

  case("CHARGE")
    call assert(m>0,"No molecule specified",.true.)
    charge=0d0
    do while (item < nitems)
      call reada(ww)
      found=.false.
      k=head(m)
      do while (k .ne. 0)
        if (name(k) == ww) then
          found=.true.
          charge=charge+q(1,k)
          write (*,"(a,1x)",advance="no") trim(ww)
        endif
        k=next(k)
      end do
      if (.not. found) then
        write(*,"(3a)",advance="no") "(", trim(ww), " not found) "
      endif
    end do
    write (*,"(a,g14.7)") ":  Total charge = ", charge

  case default
    call die('EDIT: keyword not recognised',.true.)

  end select

end do

if (tracing) print "(2a)", "Leaving ", this_routine

CONTAINS

SUBROUTINE rotate_site(k, d)

INTEGER, INTENT(IN) :: k
DOUBLE PRECISION, INTENT(IN) :: d(:,:)

!  Rotate multipoles and polarizabilities for site k
!  using Wigner matrix d

!  Transform the multipole moments from the original axes into these
!  new local axes. We transform all the moments, not just the moments
!  up to rank l(k), in case l(k) has been reduced by the LIMIT command.
q(:,k)=matmul(q(:,k),d)
n=(l(k)+1)**2
if (verbose) then
  print '(a,a)',                                                 &
      'Transformed (local) moments for site ', trim(name(k))
  if (n .ge. 1) print '(a,f10.5)',                               &
      'Charge        ', q(1,k)
  if (n .ge. 4) print '(a,3f10.5)',                              &
      'Dipole        ', q(2:4,k)
  if (n .ge. 9) print '(a,5f10.5)',                              &
      'Quadrupole    ', q(5:9,k)
  if (n .ge. 16) print '(a,5f10.5/(24x,4f10.5))',                &
      'Octopole      ', q(10:16,k)
  if (n .ge. 25) print '(a,5f10.5/(24x,4f10.5))',                &
      'Hexadecapole  ', q(17:25,k)
  if (n .ge. 36) print '(a,5f10.5/(24x,4f10.5))',                &
      'Rank 5        ', q(26:36,k)
end if
!  Transform polarizabilities. Again we transform the full matrix, even
!  if the rank is less than the maximum.
j=0
call entry(indexa,k,j,p)
do while (j > 0)
  if (p > 0) then
    !         ll=qsize(la(p))
    !                 KJ     T       KJ
    !  Transform alpha   := D * alpha
    call dgemm('t','n', sa,sa,sa,                                &
        1d0, d,sq, alpha(1,1,p),sa, 0d0,temp,sa)
    alpha(:,:,p)=temp
    if (k .ne. j) then
      if (verbose) print "(5a)", "Polarizability matrix alpha(", &
          trim(name(k)), ",", trim(name(j)), ") transformed"
      p=lookup(indexa,j,k)
    endif
    !                 JK         JK
    !  Transform alpha   := alpha   * D
    call dgemm('n','n', sa,sa,sa,                                &
        1d0, alpha(1,1,p),sa, d,sq, 0d0,temp,sa)
    alpha(:,:,p)=temp
    if (verbose) print "(5a)", "Polarizability matrix alpha(", &
        trim(name(j)), ",", trim(name(k)), ") transformed"
  endif
  call entry(indexa,k,j,p)
end do

END SUBROUTINE rotate_site

END SUBROUTINE edit
