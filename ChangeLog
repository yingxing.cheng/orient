2018-01-18  Anthony Stone  <ajs1@cam.ac.uk>

	* src/f03gl_gl.f90: Change BOZ constants as below.
	* remove redundant variable declarations and assignments from many
	routines.

2018-01-12  Anthony Stone  <ajs1@cam.ac.uk>

	* src/f03gl_glut.f90 (f03gl_glut): Change BOZ constants z'nnnn' to
	int(z'nnnn', kind=GLenum). Change CHARACTER to CHARACTER(C_CHAR).
	* show_data.f90: insert missing "upcase".

2017-01-20  Anthony Stone  <ajs1@cam.ac.uk>  revision 23132

	* Makefile: Simplified to remove the need for subsidiary
	Makefiles and to make the same Makefile suitable for the
	distributed source version.
	* src/alphas.f90: Various corrections and bug fixes.
	* src/damping.f90: bug fix
	* doc/Manual.tex: updated.
	* Various redundant directories and files removed. Support for
	x86-32 architecture, XP operating system and g95 compiler has
	been absent for some time and is now officially removed.

2016-11-16  Anthony Stone  <ajs1@cam.ac.uk>

	* src/alphas.f90 (write_alphas): Fixed bug in reading site list
	* bin/orient_display.py: Clarifications to documentation

2016-10-27  Anthony Stone  <ajs1@cam.ac.uk>

	* src/molecules.f90 (activate): Fix bug where activate
	deleted molecule head pointers for unsuppressed molecules.

2016-10-20  Anthony Stone  <ajs1@cam.ac.uk>

	* src/display.F90: Corrections to describe_map, make_map and
	scalar_map.

2016-10-19  Anthony Stone  <ajs1@cam.ac.uk>

	* src/alphas.f90 (read_alphas): Further corrections to new format
	input.
	* src/display.F90: Minor changes to export routine.
	* doc/Manual.tex updated.

2016-10-07  Anthony Stone  <ajs1@cam.ac.uk>

	* src/alphas.f90 (read_site_pairs): New routine to read CamCASP
	polarizability matrices in the new format. (Still accepts old
	format.)
	* src/alphas.f90 (write_alphas): New routine, moved out of the
	main pdata parser routine. Writes polarizabilities out in the new
	format.
	* LIMIT and PRINT also moved out of pdata. LIMIT, PRINT and WRITE
	now accept "ALL" to apply to all sites, without the need for a
	site list.
	* src/alphas.f90 (print_polarizabilities): Use of old output
	routines replaced by standard write using a buffer. RANK a [to] b
	prints only ranks from a to b. LIMIT b is equivalent to RANK 0 to
	b.
	* src/input.F90: getv routine added to read a 1D array, reading
	new records as necessary.

2016-09-13  Anthony Stone  <ajs1@cam.ac.uk>

	* src/display.F90 (display_parser): Modified ALLOCATE syntax. Now
	ALLOCATE [MAPS | POINTS | VERTICES] n

2016-08-31  Anthony Stone  <ajs1@cam.ac.uk>

	* src/display.F90 (import_map): Corrections and improvements.
	* doc/Manual.tex: Corrections.

2016-08-30  Anthony Stone  <ajs1@cam.ac.uk>

	* src/display.F90 (import_grid): Added missing code for headers.
	Minor corrections.
	* readpair.f90: Added more explanation to error message.
	* tabulate.F90: Minor corrections.

2016-07-19  Anthony Stone  <ajs1@cam.ac.uk>
	* Tagged as 4.9.03. Revision 23070.

2016-07-18  Anthony Stone  <ajs1@cam.ac.uk>

	* distrib/Makefile: Updated and corrected distribution files.
	* tests/formamide: updated.
	* doc/Manual: further corrections to display module.

2016-07-14  Anthony Stone  <ajs1@cam.ac.uk>

	* doc/Manual.tex: Further corrections and additions.
	* src/display.F90: Further corrections to command syntax.

2016-07-13  Anthony Stone  <ajs1@cam.ac.uk>  revision 23064

	* doc/Manual.tex: A5 version removed. Environments syntax and
	example defined within the latex source instead of being
	pre-processed by perl. Manual.tex can now be processed by pdflatex
	directly. The manual has been updated to describe the changes to
	the display module.

2016-07-11  Anthony Stone  <ajs1@cam.ac.uk>

	* src/display.F90 (field_map): Further changes to get the field
	display working and to have it displayed together with the
	potential display.
	* src/gl_module.F90: '?' key produces a printout of the keyboard
	commands (on stdout, so only visible if that's to the screen).

2016-07-07  Anthony Stone  <ajs1@cam.ac.uk>

	* src/display.F90 (import_grid): Further changes, to implement
	import_grid. Now at version 4.9.01.

2016-07-05  Anthony Stone  <ajs1@cam.ac.uk>  Revision 23059.

	* src/display.F90 (display): Display module extensively rewritten.
	It now has surface_grid and surface_map defined types. The
	surface_grid holds the lists of points and triangles defining the
	grid, along with some other details, while the surface_map holds
	the function values at each point for the display. There can be
	only one grid, and while there can be many surface_maps, only one
	map can be displayed on each run of the program. However several
	maps can be defined, and a difference of maps defined as another
	map for display. The grid can be exported, together with one of
	the maps if required, or a map can be exported with just the
	values and a link to the appropriate grid. It is also possible to
	export just the points, for input to CamCASP.

	* Makefile simplified. It no longer checks whether module files
	are unchanged, since compilation is much faster now.

2016-01-18  Anthony Stone  <ajs1@cam.ac.uk>

	* distrib/Orient-current/bin/compile.sh: New module show_data
	wasn't listed for compilation. Also bugs in Flags file.

2016-01-15  Anthony Stone  <ajs1@cam.ac.uk>

	* bin/compile.sh: Still listed files that have been moved into
	modules.

2015-12-02  Anthony Stone  <ajs1@cam.ac.uk>

	* src/display.F90 (display_potential): sum was not initialized.
	np(:) was not deallocated before re-allocation.
	* src/monolayer.F90: Private mathlib versions of DGAGNC and
	DLGAMA added

2015-10-05  Anthony Stone  <ajs1@cam.ac.uk>

	* src/force.F90 (sitepair): ENERGY DETAILS now prints out the
	electrostatic interaction contributions by rank.
	* Tidied up some declarations of unused variables.
	* distrib/prefix_src.py allows comment in source-files
	* Minor bugs corrected.

2015-10-02  Anthony Stone  <ajs1@cam.ac.uk>   revision 23003

	* VERSION 4.8.29   tagged as 4.8.29
	* src/damping.f90 (Tang_Toennies): Now includes modified TT
	damping for E_er = K R^((7/b)-1) exp(-bR).
	The modified TT only sets b = alpha if the dispersion is defined
	for site pairs that have an associated repulsion. Otherwise the
	alpha (b) parameter must be specified explicitly.

2015-10-01  Anthony Stone  <ajs1@cam.ac.uk>

	* src/display.F90 (display_potential): The display data may
	include
	  centre "<x> <y> <z>"
	to centre the specified position in the molecule in the display
	* src/damping.f90: Further changes to implement modified
	Tang-Toennies damping, for E_er = K (br^2 + 3br +3) exp(-br),
	br=b*R.

2015-09-30  Anthony Stone  <ajs1@cam.ac.uk>  revision 23000

	* src/damping.f90: modified Tang-Toennies damping now available.
	Consequential minor change to src/readpair.f90.

2015-08-19  Anthony Stone  <ajs1@cam.ac.uk>

	* src/alphas.f90 (drude_alpha): Added option under
	POLARIZABILITIES to read a Drude oscillator specification and
	convert it into distributed polarizabilities.

2015-08-18  Anthony Stone  <ajs1@cam.ac.uk>  revision 22991

	* src/readpair.f90 (readpair): SLATER option provided in the PAIRS
	section.

2015-08-04  Anthony Stone  <ajs1@cam.ac.uk>

	* src/force.F90: Subroutines dcheck, dcheck2, scheck and sitepair
	moved into module force. No changes other than standard
	indentation. src/Object-list updated.

2015-08-03  Anthony Stone  <ajs1@cam.ac.uk>  revision 22989

	* src/tabulate.F90 (get_config): Error trap for failure to define
	Energy Table variables.

	* src/alphas.f90 (pdata): Bug in limit subcommand: Default was
	'LIMIT TYPES' instead of 'LIMIT SITES'.

2015-07-21  Anthony Stone  <ajs1@cam.ac.uk>

	* src/gl_module.F90 (surface): Added U command to reverse inside
	and outside of surface display.

2015-07-07  Anthony Stone  <ajs1@cam.ac.uk>

	* src/types.f90: Replaced Bragg-Slater covalent radii with values
	from Cordero et al. (CCDC).
	* src/connect.f90: Changed accordingly.

2015-05-12  Anthony Stone  <ajs1@cam.ac.uk>

	* src/copy.f90: moved into mdata.F90.
	* src/mdata.f90: Added option to assemble molecule from fragments.
	* src/damping.f90: added Tkatchenko-DiStasio damping.
	* src/edit.f90: Moved site rotation code into subroutine. Added
	  option to rotate entire molecule.
	* Added depolarize.f90 and repolarize.f90 routines to extract or
	  replace effects of internal polarization.
	* fit.F90: changes to correct handling of linked parameters.
	* src/show.f90 renamed as src/show_data.f90
	* Various cosmetic changes.

2015-04-21  Anthony Stone  <ajs1@cam.ac.uk>

	* src/fit.F90: maxlink parameter introduced: maximum number of
	linked pair-function components, i.e. with the same parameter name.

2015-03-16  Anthony Stone  <ajs1@cam.ac.uk>

	* src/analyse.F90 (analyse): Long-standing bug corrected: total of
	many-body terms was incorrect.

	* src/alphas.f90 (read_alphas): Added option to read polarizabilities
	from a file. For this option they should be given separately for
	each polarizable site pair.

2015-03-02  Anthony Stone  <ajs1@cam.ac.uk>

	* tests/run_tests.py: New script to run tests of the Orient
	program. tests/{Makefile,run.test,run2} are now redundant, but the
	perl scripts for checking test results are still used.

2015-02-23  Anthony Stone  <ajs1@cam.ac.uk>

	* The Lapack ilaenv.f and xerbla.f routines are provided in the
	source directory so that they are compiled along with the rest of
	the program. Otherwise the versions from ATLAS are used, and those
	require gfortran routines from the gcc library.

	* Various minor changes here and there for consistency with
	earlier changes.

2015-02-20  Anthony Stone  <ajs1@cam.ac.uk>

	* distrib/Orient-current/INSTALL: Installation instructions and
	Flags files changed to remove the option of compiling the Lapack
	routines if the Lapack library isn't available. Keeping the
	collection of Lapack routines up to date is not a realistic
	practice.

2015-02-17  Anthony Stone  <ajs1@cam.ac.uk>

	* distrib/Orient-current/INSTALL: Numerous changes to source
	package and installation procedures.

2015-02-13  Anthony Stone  <ajs1@cam.ac.uk>

	* Tagged as 4.8.21
	* Source distribution uses a list of source files instead of
	specifying them by symbolic links in Orient-current/src.
	* src/output.f90 (putout): Cosmetic changes
	* src/show/f90: Option added: SHOW [MOLECULE] mol WRITE file
	writes a complete molecule definition to the file in Orient
	input format. Also cosmetic changes.

2015-02-03  Anthony Stone  <ajs1@cam.ac.uk>

	* distribution files: index.html changed to index.php, without a
	separate download.php. README in distrib and gdma-current linked
	to README in base directory. Both index.php and README are updated
	to new versions by update_readme.py.

2015-02-02  Anthony Stone  <ajs1@cam.ac.uk>

	* src/gl_module.F90 (colour_strip): added code in gl_module and
	display to add ticks to the colour strip. Requires a line in the
	display data of the form
	ticks t1 t2 ...
	where t1, t2, .. are the tick positions.

2015-01-07  Anthony Stone  <ajs1@cam.ac.uk>

	* src/orient.F90 (orient): Added top-level command:
	FIND [MOLECULE] name  [CM] [MI]
	CM option prints out the centre-of-mass position. MI option
	calculates and prints out the moments of inertia and inertial
	axes.

2015-01-03  Anthony Stone  <ajs1@cam.ac.uk>

	* src/edit.f90 (edit): Errors in LIMIT subcommand corrected.

2014-09-23  Anthony Stone  <ajs1@cam.ac.uk>

	* Gamma-function module replaced by more accurate numerical
	recipes versions.
	* Minor corrections.

2014-07-18  Anthony Stone  <ajs1@cam.ac.uk>

	* src/compare.f90 (compare): COMPARE command compares an Orient model
	for a dimer with a CamCASP energy scan.

	* src/extra.f90 (extra): Handles COMPARE command.

	* src/edit.f90 (edit): LIMIT option now permits ALL instead of a
	site list.

2014-07-16  Anthony Stone  <ajs1@cam.ac.uk>

	* src/display.F90 (display_potential): Changes to scalar display
	output: mean and s.d. of difference from reference now printed.

2014-07-11  Anthony Stone  <ajs1@cam.ac.uk>

	* src/display.F90 (read_data): Further changes to allow data at
	start of grid files.

2014-07-01  Anthony Stone  <ajs1@cam.ac.uk>

	* src/fit.F90 (adjust): ECHO [ON|OFF] can occur in the list of
	configurations and energies, on a line by itself, and switches on
	or off the echoing of data points to the output. Default is ON.

	* The ADJUST command can be repeated with new specifications for
	the parameters to be fitted. Parameters can be added or removed
	from the fit. Values assigned to parameters in a PAIRS command or
	fitted by a previous ADJUST are retained as starting values for
	the new fit or as fixed values if not included in the fit.

2014-06-11  Anthony Stone  <ajs1@cam.ac.uk>

	* src/types.f90 (types): List of element names moved here from
	thermofns.f90.

	* src/fit.F90: parameter name length increased to 12. Error
	messages clarified and some warnings replaced by errors.
	More information printed on quality of fit. xyz frames printed for
	extreme residuals.

2014-06-05  Anthony Stone  <ajs1@cam.ac.uk>

	* tagged as 4.8.16

2014-06-04  Anthony Stone  <ajs1@cam.ac.uk>

	* src/fit.F90 (adjust): Default weight of 0.1 kJ/mol provided for
	anchors.

2014-06-03  Anthony Stone  <ajs1@cam.ac.uk>

	* src/fit.F90 (readfit): Error message improved.
	* (use_orses): Description of fit quality improved.

2014-05-29  Anthony Stone  <ajs1@cam.ac.uk>

	* Changelog renamed as ChangeLog.

	* VERSION (PATCHLEVEL): tagged as 4.8.15.

	* src/readpair.f90 (showparams): S-function index no longer
	printed, so output can be pasted directly into Orient input.

	* Minor cosmetic changes to other source files.

2014-04-17  AJS  22746

	Tagged as 4.8.14.

2014-04-17  AJS
	src/consts.f90: Default eslimit reset to 5.
	src/display.F90: find_normals routine corrected.
	src/gl_module.F90: uses gl_FrontFace(GL_CW). This should be CCW,
	but that gives the wrong result.

14 Apr 2014  AJS
	Fixed bug in find_site function.

04 Apr 2014  AJS
	Various changes in private version transferred to trunk. Some
	persistent problems in display.F90 and gl_module.F90 corrected.
	Lighting still doesn't work.

03 Mar 2014  AJS
	gl_module.F90: bug corrected; it was displaing the inside of the
	surface.

04 Sep 2013  AJS  22554
	src/display.F90: attempted to simplify syntax for difference
	displays. Reference grid values imported into vref array.
	Minor changes to header in exported grid.
	nag & gfortran Flags: corrected references to OpenGL and X11
	libraries.
	src/consts: new function efactor(name) returns the energy
	conversion factor for unit name.

11 Jan 2013 AJM
	* axes.f90: Corrected format statements. Numbers were merging with
	each other and the text. Fixed this.

6 Dec 2012  AJS 22431
	Basinhop: "steps -> policy [last|minimum|new]" sub-option. Take
	next step from last test point, always from minimum, last unless
	new minimum.

05 December 2012 AJM 22428
	tabulate.F90: Increased format length in header in subroutine get_variable_list
	readpair.f90: Changes to subroutine showparams to print parameters without line
	   breaks. Also more information in headers.

13 Nov 2012  AJS  22404
	analyse.F90: modified to allow clusters of up to 30 molecules (in
	principle). New test water_decamer to illustrate it. Minor
	consequential changes.

27 Sept 2012  AJS  22355
	alphas.f90: prune variable wasn't declared
	distrib/Makefile corrected

21 Sept 2012  AJS  svn 22345
	Manual updated.

14 Sept 2012  AJS
	Interface for hdiag added to axis.f90
	Supposed refinement of grid positions in display.F90 suppressed.

17 Aug 2012   AJS  svn 22331
	New test added for minimization of naphthalene array

17 Aug 2012   AJS  svn 22330
	Extended the syntax in position_molecules. A position coordinate
	can now take the form
	v1 [* f1] [+/- v2 [* f2]] [+/- ...]
	where vn are defined variables and fn are numerical factors.

15 Aug 2012   AJS  svn 22325
	Further change: maxind=0 now means no iterations, while maxind<0
	means no limit.

13 Aug 2012   AJS  svn 22324
	Control of induction iterations: maxind in induction module
	provides default=50. Settable in options module. Zero means no
	limit, not no iterations. The value of maxind is used by most
	energy calculations.

11 Aug 2012   AJS  svn 22320
	induce.f90 didn't handle maxit=0 correctly.

10 Aug 2012  AJS
	axes.f90, mdata.f90: bugs corrected.
	md.F90: Now permits specification of seed for random number
	generator.

7 Aug 2012  AJS  svn 22307
	options.f90: vmap array now set up using new DEFINE and
	POSITION commands. vmap and x0 arrays and nv variable moved to
	variables module.
	mdata.f90: Molecule position information (AT keyword) is now
	optional; it can be given later in the POSITION command.
	variables.f90: New derived type "variable". New define_variables
	routine to handle variable definitions.
	New position_molecules subroutine to read molecule positions and
	orientations.
	rotations.f90: New eulermat and eulerquat routines to convert
	euler angles to matrix or quaternion. New read_rotation routine.

17 July 2012  AJS  svn 22305
	strip_recipes.pl (formerly get_recipes.pl) and patch_recipes.pl
	updated and applied.

13 July 2012  AJS  svn 22304
	gl_module: Contour colours implemented.

10 July 2012  AJS  svn 22302
	Units command now pushes both energy and length units onto the
	stack, and "Units pop" command pops both off. Consequential
	changes to basinhop.F90, and display.F90.
	Triple-dipole command prints the value of C9 used.
	Minor correction to gl_module.F90

4 July 2012  AJS  svn 22297
	display.F90 and gl_module.F90 modified to implement contours on
	scalar maps. Also modified to implement lighting, but it doesn't
	work. Switched off by default.
	New routine push_unit added to consts.f90.
	Formamide test modified to include contours.
	Basinhop test changed and MC test removed from standard set.

29 Jun 2012  AJS  svn 22295
	Modifications to deal with unrotated molecules.
	dump.F90 becomes a new module dumper. Consequent changes in options,
	  mc, optimize, mdmc, orient, md, Module-list, Object-list,
	  Module_dependencies
	Redundant variable declarations removed from many routines.
	Some explicit ONLY lists added in induce, alphas,
	Partial changes to show.f90 to print data from molecule type.
	Triple.f90 changed to print triangle angles instead of cosines.
	Adjustments to some formats.
	basinhop test improved.
	Implementation notes: major changes.

4 Jan 2012  AJS  svn 22255
	Symbolic links in distrib/Orient-current changed from absolute to relative.

30 Dec 2011 AJS  svn 22242
	* Subroutine plotinput renamed as plot_options.
	* optimize.F90: verbose option added in bfgs
	* options.f90: option parameters tidied up.
	* tabulate.F90: reorganised to provide module routines
	get_variable_list and get_config.
	* consts.f90: modified to read "angstrom" and "bohr" from e.g.
	"angstrom^2" or "bohr^2".
	* alphas.f90: minor changes to output.
	* display.F90: trivial errors corrected.
	* interact.f90: more detailed output when sites coincide.
	* basinhop.F90: extensive changes to correct implementation of
	basin-hopping algorithm. Additional features also included.
	Reorient option still doesn't work correctly.
	* Manual updated for basin-hopping.

12 Sept 2011 AJS
	* display.F90: Code added to calculate vertex normals. Viewpoint can be
	specified (in molecule axes); also up direction. Import command
	has extra factor keyword to adjust sign or value of imported
	values.
	* gl_module: Code added to permit generation of video frames
	(single rotation about the vertical axis).
	* input.F90: readf can take a double-precision vector as first
	argument, and reads the number of values to fill it (all from one
	line of input).
	* basinhop.f90: Code added to permit blocks of rotations only or
	translations only (or both). Rotations are about molecular axes,
	and linear molecules are rotated about axes perpendicular to z.
	Trial configurations are not now tested with a Metropolis
	criterion, so any trial configuration is accepted unless its
	energy falls below the rejection criterion.

11 Aug 2011 AJS
	* basinhop.f90: failed when one molecule was fixed -- trial config
	not initialized properly.

8 Aug 2011 AJS
	* gl_module: display window closed by ctrl-Q as well as Q.
	* show.f90: translate then rotate moments in total moment
	calculation. 
	* mdim() moved to mol()%mdim everywhere.
	* mdata.f90: Atom declaration was not handled correctly.
	* optimize.F90: test for plot corrected in potential subroutine.
	* connect.f90: adjustable factor introduced for bonded test.
	* properties.f90: dgemv replaced by matmul.
	* input.F90: explicit colour names red, green and blue recognized.
	* mdmc.F90: private version of calclinks restored.
	* display.F90: limit on number of bonds increased. Minor changes
	to output.
	* H2O..H2O and HF..HF tests updated.
	* Manual updated.

27 Jul 2011 AJS
	* properties.f90: minor corrections to deal with system moment of
	inertia. 
	* Bug in interact.f90 corrected at last.
	* basinhop.F90: added option to specify the minimum difference in
	energy and moments of inertia for acceptance as a new minimum. USE
	statements qualified by ONLY lists.

25 Jul 2011 AJS  
	* rotations.f90: improved accuracy of conversions.
	* input.F90: added warn(string) subroutine.
	* fit.F90: added sanity checks: no more than np penalties, no more
	than one anchor for any parameter.
	* axis,f90: treatment of "global" specification corrected.
	* display.F90: improved syntax check in EXPORT.
	* basinhop.F90: added moment-of-inertia check as well as energy
	check when testing for new minima.
	* consts.f90: moved code into new (private) set_unit and (public)
	pop_unit routines.
	* x86-64/nag/Flags: removed -lXaw.
	
20 Jun 2011 AJS  svn 22137
	* Branch orient4.7
	* basinhop.F90: Many changes, mostly to step-adjustment algorithm.
	* optimize.F90: Changes to LMBFGS optimization procedure.
	* interact.f90: Minor changes in attempt to find bug.
	* Other routines: many minor changes. ONLY lists added to some USE
	statements.
	* deriv_check.F90: configuration saved and recalled rather than
	reinstated by calculation. Other minor changes.

5 Apr 2011 AJS  svn 22136
	* basinhop.F90: Configuration defined in terms of mol(m)%cm
	and mol(m)%p. Various other minor changes.
	* optimize.F90: BFGS minimization uses analytic derivatives.
	Currently doesn't work correctly.
	* deriv_check.F90: Code to check energy derivatives w.r.t.
	angle-axis variables.
	* gammafns.f90, orses_module.F90: Removed "implicit double
	precision" and inserted explicit declarations.
	* properties.f90: removed transform routine and replaced it with
	matrix intrinsics.
	* Minor changes to some other routines.	

29 Mar 2011 AJS  svn 22134
	* Extensive changes to introduce angle-axis treatment of
	orientation derivatives.
	* Pairinfo moved into module interact as pairinfo_tq. New routine
	pairinfo_aa to construct derivatives in terms of angle-axis
	variables. (First-order only at present.) Calls to pairinfo select
	between pairinfo_aa and pairinfo_tq according to setting of
	variable angle_axis_derivs.
	* Subroutine minimum (BFGS version) in module optimize now uses
	angle-axis variables.
	* Test example H2O..H2O changed to use BFGS minimization.
	* Derived type molecule introduced. At present it contains only
	the molecular (centre-of-mass) position and orientation (inertial
	axes relative to global axes) as angle-axis variables and rotation
	matrix, and the derivatives of the rotation matrix w.r.t. the
	angle-axis variables.
	* Deriv_check has an option to check the derivatives of the basic
	direction cosines w.r.t. position and angle-axis orientation.
	* Basin-hopping routine works but does not yet treat orientation
	sensibly.

24 Feb 2011 AJS
	* Further changes to basinhop.F90 to permit restarts.

11 Feb 2011 AJS
	* basinhop.F90: Further changes. Redundant code removed.
	* lmbfgs.f90 removed. Replaced with David Wales's mylbfgs routine,
	in a new module gmin.F90.
	* optimize.F90: minimum subroutine substantially modified to use
	mylbfgs. Additional routines provided, including a potential
	routine to evaluate energy and gradients. The coordinates used are
	position and angle-axis vector, and derivatives with respect to
	angle-axis vector components are currently evaluated numerically.
	Attempts to convert torque to angle-axis derivatives are currently
	unsuccessful.
	* deriv_check.F90: code added to check angle-axis derivatives.
	* options.f90: code added to handle options for the mylbfgs
	routine.
	readpair.f90: Absence of damping recorded more clearly in output.
	force.F90: derivative arrays are allocated on entry if necessary.
	rotations.f90: in rotmat, an attempt to rotate about a zero vector
	is not faulted if the rotation angle is close to zero.

31 Jan 2011 AJS
	* basinhop.F90: Numerous changes. Now believed to be working
	correctly.
	* properties.f90: IMPLICIT NONE introduced
	* dump.F90: Provision for dump to "stdout".
	* rotations.f90: angleaxis routine refined to handle special cases
	correctly. Check for genuine rotation matrix separated into new
	subroutine.

25 Jan 2011 AJS
	* options.f90: default was to plot at every iteration. Now
	suppressed.
	* mc.F90: Comment lines now accepted.
	* optimize.F90: optional "converged" output flag. Optional
	"reject" argument: abort if energy goes below this value.
	Incomplete attempt to correct errors in minimize routine.
	mdmc.F90: Changes to output and other minor changes.
	orses_module.F90: IMPLICIT NONE for routine efol. Needed elsewhere
	too. Messages about progress of optimization suppressed (reinstate
	by setting verbose flag).
	histograms.f90: minenergy and maxenergy public variables.
	dump.F90: Optional "full" flag: minimal information output if
	full=.false.
	plot.F90: xyz output to named file instead of anonymous unit 24.
	rotations.f90: Additional routines for manipulating rotations.

18 Aug 2010 AJS
	* LW localization procedure was incorrectly implemented. Now
	corrected. The diagonalization of the bond matrix has been moved
	from the bondev routine in connect.f90 (which is now renamed
	bond_matrix and just returns the bond matrix).
	* f03gl_glut.f90 is now derived from freeglut.h rather than
	GLUT.h.
	* Typing "Q" in a display should now call glutLeaveMainLoop, but
	this doesn't seem to work -- the program stops as before.
	* ENERGY TABLE has more options. See manual.
	
6 July 2010 AJS
	* Array size parameters maxg and maxfit are now adjustable in the
	initial ALLOCATE command.

29 April 2010 AJS
	* output.f90: putstr didn't handle null strings correctly.
	* mdata.f90: provided interface to axis
	* redundant code and declarations removed from several routines.

7 April 2010 AJS
	* alphas.f90: Total keyword in polarizabilities only worked for
	polarizabilities in global axes. Should now work for local or
	non-local polarizabilities referred to local axes.

31 March 2010 AJS
	* axis.f90: Local axes can be defined parallel or antiparallel to
	global axes.

30 March 2010 AJS
	* Changes to input.F90: upcase and locase are now functions.
	nerror changed to input_error_flag. Input record buffer size
	increased to 255. 

2 March 2010 AJS
	* mod.makefile: inclusion of display routines now controlled by
	OPENGL flag.

25 February 2010 AJS
	* distrib/Makefile: x86-64/gfortran version added.

29 January 2010 AJS
	* Makefiles modified so that "make STATIC=true" causes a build
	with the display procedures omitted.

27 November 2009 AJS
	* rotations.f90 and putquat.f90
	The MD and MC modules work in the passive convention when they
	obtain a rotation matrix from a quaternion, while the rest of
	Orient uses the active convention. The quatmat routine in the
	rotations.f90 module was therefore inconsistent with most of the
	program, and in particular with the quater routine, which obtains
	a rotation matrix from a quaternion. MD and MC use quatmat via
	putquat, and quatmat.f90 has been absorbed into the MDMC module,
	along with the passive version of quatmat, renamed quatmatrix. The
	version of quatmat in the rotations module has been changed to the
	active convention. (It isn't now used by Orient, but is needed
	when the rotations module is used elsewhere.)
	* structure.f90
	This routine has been taken out of the rotations module in order
	to make the rotations module more self-contained and portable.

20 October 2009 AJS
	* display.F90: If displayed scalar values extend through zero, the
	default display range is symmetrical about zero.
	* axis.f90: Changes to cope better with linear molecules.

14 March 2009
	* axis.f90: A local axis may be specified as "global" to coincide
	with the global axis direction.

2 March 2009  AJS
	* mdata.f90: ROTATED BY x ABOUT l m n didn't work in the molecule
	definition line of a linear molecule.

24 February 2009  AJS
	* variables.f90: size of w variable increased to avoid irrelevant
	string truncation errors.

19 February 2009  AJS
	* display.F90: use -DNAGF95 to activate flush statements.

14 February 2009  AJS
	* input.F90: use -DARGS in FFLAGS if the getargs routine is
	required. Not all compilers can deal with it.

16 October 2008  AJS
	* src/connect.f90: references to unused links routine removed.
	* src/edit.f90: change in comment only.
	* src/alphas.f90: only-chargeflow implemented for LW localization.
	* doc/Manual.tex: Updated accordingly.
	* display.F90: minor bug fix.
	* distrib/sub.makefile: install target removed.
	* distrib/Orient-current/Makefile: install target changed to
	handle ARCH. rm 
	* distrib/Makefile: XP download tgz instead of zip.
	* nag/Flags: minor corrections.
	* INSTALL: corrected.
	* compile.sh: modified.
	* bin/makemod: minor corrections.

11 April 2008  AJS
	* colours in a colour map may be specified in terms of either hue,
	  luminance and saturation or hue, saturation and value.

25 March 2008  AJS
	* indexes: remove routine added, to remove an index entry.
	* connect: bondev only considers polarizable sites.
	           index_bonds routine separated from main bonds routine,
	* alphas: LW localize routine is the default. Now checks that all
	           non-local terms are less than the test value. Various
	           other minor changes.

