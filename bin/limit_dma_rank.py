#!/usr/bin/python
#  -*-  coding  iso-8859-1  -*-

"""Reduce rank of a distributed-multipole description.
"""

import argparse
import re
# import os.path
# import string
# import subprocess

parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Reduce rank of a distributed-multipole description.
""",epilog="""
Given a file containing distributed multipoles for a molecule in standard
Orient format (as output by the GDMA program), produce a copy of the file
with multipoles above the specified rank suppressed.
E.g.
limit_dma_rank <file>
""")


parser.add_argument("file", help="File containing multipole description")
parser.add_argument("--rank", help="Maximum rank to retain (default 1)",
                    type=int, default=1)

args = parser.parse_args()

#  Number of lines to print
np = [1,2,3,5,7]

printing = True
with open(args.file) as IN:
  while True:
    line = IN.readline()
    if line == "":
      break
    m = re.match(r' *([A-Z][a-z]*\d+) +(-?\d+\.\d+) +(-?\d+\.\d+) +(-?\d+\.\d+) +(Type|Rank)',line)
    if m:
      line = re.sub(r'Rank +\d', 'Rank '+"{:1d}".format(args.rank), line, flags=re.I)
      print line,
      for n in range(np[args.rank]):
        line = IN.readline()
        print line,
      #  Skip until a blank line is read
      while True:
        line = IN.readline()
        if re.match(r' *$', line):
          break
    else:
      print line,

