(* Content-type: application/mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 7.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       145,          7]
NotebookDataLength[      2765,         98]
NotebookOptionsPosition[      2337,         78]
NotebookOutlinePosition[      2675,         93]
CellTagsIndexPosition[      2632,         90]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"e", "=", 
  RowBox[{"0.001", 
   RowBox[{"(", 
    RowBox[{"1", "+", 
     RowBox[{"p1", "*", "R"}], "+", 
     RowBox[{"p2", "*", 
      RowBox[{"R", "^", "2"}]}]}], ")"}], " ", 
   RowBox[{"Exp", "[", 
    RowBox[{"alpha", 
     RowBox[{"(", 
      RowBox[{"rho", "-", "R"}], ")"}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.448177818620773*^9, 3.448177820513192*^9}, {
  3.448177859431576*^9, 3.448177901904078*^9}, {3.4481782812599*^9, 
  3.448178298206807*^9}}],

Cell[BoxData[
 RowBox[{"0.001`", " ", 
  SuperscriptBox["\[ExponentialE]", 
   RowBox[{"alpha", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "R"}], "+", "rho"}], ")"}]}]], " ", 
  RowBox[{"(", 
   RowBox[{"1", "+", 
    RowBox[{"p1", " ", "R"}], "+", 
    RowBox[{"p2", " ", 
     SuperscriptBox["R", "2"]}]}], ")"}]}]], "Output",
 CellChangeTimes->{{3.448178284398752*^9, 3.448178299055703*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"D", "[", 
    RowBox[{"e", ",", "p2", ",", "alpha"}], "]"}], "/.", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"alpha", "\[Rule]", "1.8"}], ",", 
     RowBox[{"rho", "\[Rule]", "7.0"}], ",", 
     RowBox[{"p1", "\[Rule]", "0.1"}], ",", 
     RowBox[{"p2", "\[Rule]", "0.1"}]}], "}"}]}], "/.", 
  RowBox[{"{", 
   RowBox[{"R", "->", "6.62913432"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.448177910512088*^9, 3.448178051729385*^9}, {
  3.44817823356549*^9, 3.448178237552807*^9}, {3.44817832727788*^9, 
  3.448178327605069*^9}, {3.448178365445187*^9, 3.448178381977039*^9}}],

Cell[BoxData["0.031772188912282805`"], "Output",
 CellChangeTimes->{
  3.448178241164708*^9, {3.448178313758148*^9, 3.448178329319909*^9}, {
   3.448178370602493*^9, 3.448178382823199*^9}}]
}, Open  ]]
},
WindowSize->{640, 750},
WindowMargins->{{145, Automatic}, {Automatic, 30}},
FrontEndVersion->"7.0 for Linux x86 (32-bit) (November 10, 2008)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[567, 22, 493, 14, 31, "Input"],
Cell[1063, 38, 408, 12, 33, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1508, 55, 621, 15, 52, "Input"],
Cell[2132, 72, 189, 3, 31, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

