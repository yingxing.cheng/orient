Clear[rax,ray,raz,rbx,rby,rbz,cxx,cxy,cxz,cyx,cyy,cyz,czx,czy,czz]
array[S[x_,y_,u_,v_],{0,6},{0,6},{0,6}]
S[0,0,0]=1
S[1,0,1]=gaz[x,y,u,v]
S[0,1,1]=gbz[x,y,u,v]
S[1,1,0]=gzz[x,y,u,v]
S[1,1,2]=Simplify[(S[1,0,1]*S[0,1,1]+S[1,1,0]/3)*3/2]
S[1,2,1]=Simplify[(S[1,1,0]*S[0,1,1]+S[1,0,1]/3)*3/2]
S[2,1,1]=Simplify[(S[1,1,0]*S[1,0,1]+S[0,1,1]/3)*3/2]
S[0,2,2]=Simplify[(S[0,1,1]^2-S[0,0,0]/3)*3/2]
S[2,2,0]=Simplify[(S[1,1,0]^2-S[0,0,0]/3)*3/2]
S[2,0,2]=Simplify[(S[1,0,1]^2-S[0,0,0]/3)*3/2]
S[1,2,3]=Simplify[(S[1,1,2]*S[0,1,1]-S[1,0,1]/3+S[1,2,1]/15)*5/3]
S[2,1,3]=Simplify[(S[1,1,2]*S[1,0,1]-S[0,1,1]/3+S[2,1,1]/15)*5/3]
S[3,1,2]=Simplify[(S[2,1,1]*S[1,0,1]-S[1,1,0]/3+S[1,1,2]/15)*5/3]
S[1,3,2]=Simplify[(S[1,2,1]*S[0,1,1]-S[1,1,0]/3+S[1,1,2]/15)*5/3]
S[2,3,1]=Simplify[(S[1,2,1]*S[1,1,0]-S[0,1,1]/3+S[2,1,1]/15)*5/3]
S[3,2,1]=Simplify[(S[2,1,1]*S[1,1,0]-S[1,0,1]/3+S[1,2,1]/15)*5/3]
S[2,2,2]=Simplify[(S[1,1,0]*S[1,1,2]+S[0,2,2]/3+S[2,0,2]/3)*3]
S[3,0,3]=Simplify[(S[2,0,2]*S[1,0,1]-S[1,0,1]*2/5)*5/3]
S[0,3,3]=Simplify[(S[0,2,2]*S[0,1,1]-S[0,1,1]*2/5)*5/3]
S[3,3,0]=Simplify[(S[2,2,0]*S[1,1,0]-S[1,1,0]*2/5)*5/3]
S[4,0,4]=Simplify[(S[2,0,2]^2-S[0,0,0]/5-S[2,0,2]*2/7)*35/18]
S[0,4,4]=Simplify[(S[0,2,2]^2-S[0,0,0]/5-S[0,2,2]*2/7)*35/18]
S[4,4,0]=Simplify[(S[2,2,0]^2-S[0,0,0]/5-S[2,2,0]*2/7)*35/18]
S[2,2,4]=Simplify[(S[1,1,2]^2-S[0,0,0]/6-S[0,2,2]/6-S[2,0,2]/6-S[2,2,0]/30-S[2,2,2]/21)*35/18]
S[2,4,2]=Simplify[(S[1,2,1]^2-S[0,0,0]/6-S[0,2,2]/6-S[2,0,2]/30-S[2,2,0]/6-S[2,2,2]/21)*35/18]
S[4,2,2]=Simplify[(S[2,1,1]^2-S[0,0,0]/6-S[0,2,2]/30-S[2,0,2]/6-S[2,2,0]/6-S[2,2,2]/21)*35/18]
S[1,3,4]=Simplify[(S[1,0,1]*S[0,3,3]+S[1,3,2]*3/7)*7/4]
S[3,1,4]=Simplify[(S[0,1,1]*S[3,0,3]+S[3,1,2]*3/7)*7/4]
S[1,4,3]=Simplify[(S[1,1,0]*S[0,3,3]+S[1,2,3]*3/7)*7/4]
S[4,1,3]=Simplify[(S[3,0,3]*S[1,1,0]+S[2,1,3]*3/7)*7/4]
S[3,4,1]=Simplify[(S[0,1,1]*S[3,3,0]+S[3,2,1]*3/7)*7/4]
S[4,3,1]=Simplify[(S[3,3,0]*S[1,0,1]+S[2,3,1]*3/7)*7/4]
S[2,3,3]=Simplify[(S[2,1,1]S[0,2,2]-S[2,1,1]/25+S[2,3,1]*9/25+S[2,1,3]*9/25)*25/6]
S[3,2,3]=Simplify[(S[1,2,1]S[2,0,2]-S[1,2,1]/25+S[3,2,1]*9/25+S[1,2,3]*9/25)*25/6]
S[3,3,2]=Simplify[(S[1,1,2]S[2,2,0]-S[1,1,2]/25+S[3,1,2]*9/25+S[1,3,2]*9/25)*25/6]
S[0,5,5]=Simplify[(S[0,3,3]S[0,2,2]-S[0,1,1]*9/35-S[0,3,3]*4/15)*21/10]
S[5,0,5]=Simplify[(S[2,0,2]S[3,0,3]-S[1,0,1]*9/35-S[3,0,3]*4/15)*21/10]
S[5,5,0]=Simplify[(S[2,2,0]S[3,3,0]-S[1,1,0]*9/35-S[3,3,0]*4/15)*21/10]
S[1,4,5]=Simplify[(S[1,0,1]S[0,4,4]+S[1,4,3]*9/4)*9/5]
S[1,5,4]=Simplify[(S[1,1,0]S[0,4,4]+S[1,3,4]*9/4)*9/5]
S[4,1,5]=Simplify[(S[0,1,1]S[4,0,4]+S[4,1,3]*9/4)*9/5]
S[5,1,4]=Simplify[(S[1,1,0]S[4,0,4]+S[3,1,4]*9/4)*9/5]
S[4,5,1]=Simplify[(S[0,1,1]S[4,4,0]+S[4,3,1]*9/4)*9/5]
S[5,4,1]=Simplify[(S[1,0,1]S[4,4,0]+S[3,4,1]*9/4)*9/5]
S[2,3,5]=Simplify[(S[2,0,2]S[0,3,3]-S[2,3,1]*9/35+S[2,3,3]*4/15)*21/10]
S[3,2,5]=Simplify[(S[0,2,2]S[3,0,3]-S[3,2,1]*9/35+S[3,2,3]*4/15)*21/10]
S[5,2,3]=Simplify[(S[2,2,0]S[3,0,3]-S[1,2,3]*9/35+S[3,2,3]*4/15)*21/10]
S[2,5,3]=Simplify[(S[2,2,0]S[0,3,3]-S[2,1,3]*9/35+S[2,3,3]*4/15)*21/10]
S[3,5,2]=Simplify[(S[0,2,2]S[3,3,0]-S[3,1,2]*9/35+S[3,3,2]*4/15)*21/10]
S[5,3,2]=Simplify[(S[2,0,2]S[3,3,0]-S[1,3,2]*9/35+S[3,3,2]*4/15)*21/10]
Derivative[1,0,0,0][gaz][x_,y_,u_,v_]=I gay[x,y,u,v]
Derivative[0,1,0,0][gaz][x_,y_,u_,v_]=-I gax[x,y,u,v]
Derivative[0,0,1,0][gaz][x_,y_,u_,v_]=0
Derivative[0,0,0,1][gaz][x_,y_,u_,v_]=0
Derivative[1,0,0,0][gax][x_,y_,u_,v_]=0
Derivative[0,1,0,0][gax][x_,y_,u_,v_]=I gaz[x,y,u,v]
Derivative[0,0,1,0][gax][x_,y_,u_,v_]=0
Derivative[0,0,0,1][gax][x_,y_,u_,v_]=0
Derivative[1,0,0,0][gay][x_,y_,u_,v_]=-I gaz[x,y,u,v]
Derivative[0,1,0,0][gay][x_,y_,u_,v_]=0
Derivative[0,0,1,0][gay][x_,y_,u_,v_]=0
Derivative[0,0,0,1][gay][x_,y_,u_,v_]=0
Derivative[0,0,1,0][gbz][x_,y_,u_,v_]=I gby[x,y,u,v]
Derivative[0,0,0,1][gbz][x_,y_,u_,v_]=-I gbx[x,y,u,v]
Derivative[1,0,0,0][gbz][x_,y_,u_,v_]=0
Derivative[0,1,0,0][gbz][x_,y_,u_,v_]=0
Derivative[0,0,1,0][gbx][x_,y_,u_,v_]=0
Derivative[0,0,0,1][gbx][x_,y_,u_,v_]=I gbz[x,y,u,v]
Derivative[1,0,0,0][gbx][x_,y_,u_,v_]=0
Derivative[0,1,0,0][gbx][x_,y_,u_,v_]=0
Derivative[0,0,1,0][gby][x_,y_,u_,v_]=-I gbz[x,y,u,v]
Derivative[0,0,0,1][gby][x_,y_,u_,v_]=0
Derivative[1,0,0,0][gby][x_,y_,u_,v_]=0
Derivative[0,1,0,0][gby][x_,y_,u_,v_]=0
Derivative[1,0,0,0][gzz][x_,y_,u_,v_]=I gyz[x,y,u,v]
Derivative[0,1,0,0][gzz][x_,y_,u_,v_]=-I gxz[x,y,u,v]
Derivative[1,0,0,0][gxz][x_,y_,u_,v_]=0
Derivative[0,1,0,0][gxz][x_,y_,u_,v_]=I gzz[x,y,u,v]
Derivative[1,0,0,0][gyz][x_,y_,u_,v_]=-I gzz[x,y,u,v]
Derivative[0,1,0,0][gyz][x_,y_,u_,v_]=0
Derivative[1,0,0,0][gzx][x_,y_,u_,v_]=I gyx[x,y,u,v]
Derivative[0,1,0,0][gzx][x_,y_,u_,v_]=-I gxx[x,y,u,v]
Derivative[1,0,0,0][gxx][x_,y_,u_,v_]=0
Derivative[0,1,0,0][gxx][x_,y_,u_,v_]=I gzx[x,y,u,v]
Derivative[1,0,0,0][gyx][x_,y_,u_,v_]=-I gzx[x,y,u,v]
Derivative[0,1,0,0][gyx][x_,y_,u_,v_]=0
Derivative[1,0,0,0][gzy][x_,y_,u_,v_]=I gyy[x,y,u,v]
Derivative[0,1,0,0][gzy][x_,y_,u_,v_]=-I gxy[x,y,u,v]
Derivative[1,0,0,0][gxy][x_,y_,u_,v_]=0
Derivative[0,1,0,0][gxy][x_,y_,u_,v_]=I gzy[x,y,u,v]
Derivative[1,0,0,0][gyy][x_,y_,u_,v_]=-I gzy[x,y,u,v]
Derivative[0,1,0,0][gyy][x_,y_,u_,v_]=0
Derivative[0,0,1,0][gzz][x_,y_,u_,v_]=I gzy[x,y,u,v]
Derivative[0,0,0,1][gzz][x_,y_,u_,v_]=-I gzx[x,y,u,v]
Derivative[0,0,1,0][gzx][x_,y_,u_,v_]=0
Derivative[0,0,0,1][gzx][x_,y_,u_,v_]=I gzz[x,y,u,v]
Derivative[0,0,1,0][gzy][x_,y_,u_,v_]=-I gzz[x,y,u,v]
Derivative[0,0,0,1][gzy][x_,y_,u_,v_]=0
Derivative[0,0,1,0][gxz][x_,y_,u_,v_]=I gxy[x,y,u,v]
Derivative[0,0,0,1][gxz][x_,y_,u_,v_]=-I gxx[x,y,u,v]
Derivative[0,0,1,0][gxx][x_,y_,u_,v_]=0
Derivative[0,0,0,1][gxx][x_,y_,u_,v_]=I gxz[x,y,u,v]
Derivative[0,0,1,0][gxy][x_,y_,u_,v_]=-I gxz[x,y,u,v]
Derivative[0,0,0,1][gxy][x_,y_,u_,v_]=0
Derivative[0,0,1,0][gyz][x_,y_,u_,v_]=I gyy[x,y,u,v]
Derivative[0,0,0,1][gyz][x_,y_,u_,v_]=-I gyx[x,y,u,v]
Derivative[0,0,1,0][gyx][x_,y_,u_,v_]=0
Derivative[0,0,0,1][gyx][x_,y_,u_,v_]=I gyz[x,y,u,v]
Derivative[0,0,1,0][gyy][x_,y_,u_,v_]=-I gyz[x,y,u,v]
Derivative[0,0,0,1][gyy][x_,y_,u_,v_]=0
xx is the matrix X that converts between complex and real spherical harmonics.
jmax=5;
Array[xx,{2jmax+1,2jmax+1},{0,-jmax}]
For[i=0,i<=2jmax+1,i++,
For[j=-jmax,j<=jmax,j++,
xx[i,j]=0]]
xx[0,0]=1
For[m=1,m<=jmax,m++,
xx[2m-1,m]=(-1)^m*Sqrt[1/2];
xx[2m-1,-m]=Sqrt[1/2];
xx[2m,m]=(-1)^m*I*Sqrt[1/2];
xx[2m,-m]=-I*Sqrt[1/2]]
(* Clear[out] *)
(* out=OpenWrite["Sfunctions.f90"] *)
For[l1=0,l1<6,l1++,For[l2=0,l2<6,l2++,For[j=Abs[l1-l2],j<=Min[l1+l2,5],j+=2,
(*Clear[rax,ray,raz,rbx,rby,rbz,cxx,cxy,cxz,cyx,cyy,cyz,czx,czy,czz];*)
ClearAll[ss,sfn];
Array[ss,{2l1+1,2l2+1},{-l1,-l2}];
ss[0,0][x_,y_,u_,v_]=S[l1,l2,j][x,y,u,v];
For[m2=0,m2<l2,m2++,
ss[0,m2+1][x_,y_,u_,v_]=FullSimplify[(Derivative[0,0,1,0][ss[0,m2]]-I Derivative[0,0,0,1][ss[0,m2]])/Sqrt[l2(l2+1)-m2(m2+1)]];
ss[0,-m2-1][x_,y_,u_,v_]=FullSimplify[(Derivative[0,0,1,0][ss[0,-m2]]+I Derivative[0,0,0,1][ss[0,-m2]])/Sqrt[l2(l2+1)-m2(m2+1)]]];

For[m2=-l2,m2<=l2,m2++,
For[m1=0,m1<l1,m1++,
ss[m1+1,m2][x_,y_,u_,v_]=FullSimplify[(Derivative[1,0,0,0][ss[m1,m2]]-I Derivative[0,1,0,0][ss[m1,m2]])/Sqrt[l1(l1+1)-m1(m1+1)]];
ss[-m1-1,m2][x_,y_,u_,v_]=FullSimplify[(Derivative[1,0,0,0][ss[-m1,m2]]+I Derivative[0,1,0,0][ss[-m1,m2]])/Sqrt[l1(l1+1)-m1(m1+1)]]]];
Array[sfn,{2l1+1,2l2+1},{0,0}];
For[k1=0,k1<=2l1,k1++,
For[k2=0,k2<=2l2,k2++,
sfn[k1,k2]=Sum[ss[i,j]xx[k1,i]xx[k2,j],{i,-l1,l1},
{j,-l2,l2}];
Print["S",l1,l2,j,"[",k1,",",k2,"]=",FullSimplify[sfn[k1,k2]]]]
]]]]
(* Close[out] *)
