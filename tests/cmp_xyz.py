#!/usr/bin/python
#  -*-  coding:  utf-8  -*-

"""Compare xyz files with reference.
"""

from functions import *
import argparse
import re
# import os.path
# import string
# import subprocess

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Compare xyz files with reference.
""",epilog="""
Usage: {} file1 [file2 [file3]]
Compare .xyz files. Only the last frame of each file is examined.
If one file argument is given, file2 is taken to be check/file1.
If 3 file arguments are given, file1 is compared with file2, and if
that comparison fails, it is compared with file3.
""".format(this))

parser.add_argument("file", help="Files to compare", nargs="+")

args = parser.parse_args()



file1 = args.file[0]
f1 = ofile(file1)
if len(args.file) > 1:
  file2 = args.file[1]
else:
  file2 = "check/"+file1
f2 = ofile(file2)
if len(args.file) > 2:
  file3 = args.file[2]
  print "\nChecking {} against {} or {}".format(file1,file2,file3)
else:
  file3 = ""
  print "\nChecking {} against {}".format(file1,file2)

if cmp_xyz(f1,f2):
  print "File {} checked against {} -- OK.".format(file1,file2)
  exit(0)
else:
  if file3:
    f3 = ofile(file3)
    if cmp_xyz(f1,f3):
      print "File {} checked against {} -- OK.".format(file1,file3)
      exit(0)
    else:
      print "File {} differs from both {} and {}.".format(file1,file2,file3)
      exit(1)
  else:
    print "Files {} and {} differ.".format(file1,file2)
    exit(1)
