#! /bin/bash
if [[ ! -a out ]]; then
  echo "No output file"
  exit 1
fi

perl ../cplines.pl out "Many-body" "Total triple-dipole"
