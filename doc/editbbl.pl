#!/usr/bin/perl -wi.bak
while (<>) {
  if (/harvarditem/) {
    print "\\setlength\\itemsep{0pt}\n";
  }
  elsif ( /ajs1-cam/ ) {
    s/ajs1-/ajs1@/;
  }
  print;
}
